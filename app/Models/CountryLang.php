<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryLang extends Model
{

    protected $table = 'countries_languages';
    protected $fillable = [
        'id', 'title', 'country_id', 'lang_id',
    ];

    public function language()
    {
        return $this->hasOne('App\Models\Language', 'id', 'lang_id');
    }

    public static function createCountry($data)
    {

        $country = Country::create([
            'iso' => $data['isoName'],
            'flag' => $data['flagName'],
        ]);

        $new_country = CountryLang::create([
            'country_id' => $country->id,
            'title' =>  $data['title'],
            'lang_id' => $data['lang_id'],
        ]);

        $langs = Language::where('id', '!=', $data['lang_id'])->get();
        foreach ($langs as $key => $item) {
			

            CountryLang::create([
                'lang_id' => $item->id,
                'country_id' => $country->id,
            ]);
        }

        return $country;
    }

    public static function deleteCountry($id)
    {
        $country = Country::find($id);
        $countryLanguages = $country->getLangAll();
        foreach ($countryLanguages as $item) {
            $item->delete();
        }
        $country->delete();
    }
}
