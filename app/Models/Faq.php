<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Faq extends Model
{

    protected $table = 'faqs';
    protected $fillable = [
        'id', 'created_ad', 'updated_at',
    ];
    /**
     * Get the phone record associated with the user.
     */
    public function translate($lang = null)
    {
        $lang_id = Language::nowID();

        return $this->hasOne('App\Models\FaqLang', 'faq_id', 'id')
            ->where([
                ['lang_id', $lang_id],
                ['title', '!=', null],
            ])
            ->first();
    }

    public function getLangAll()
    {
        return $this->hasOne('App\Models\FaqLang', 'faq_id', 'id')->get();
    }

    public function translateAdmin()
    {
        $lang_id = Language::nowID();

        $result = $this->hasOne('App\Models\FaqLang', 'faq_id', 'id')
            ->where([
                ['lang_id', $lang_id],
                ['title', '!=', null],
            ])
            ->first();

        if (!$result) {
            return $this->hasOne('App\Models\FaqLang', 'faq_id', 'id')
                ->where('title', '!=', null)
                ->first();
        } else {
            return $result;
        }
	}
	
	public function language()
    {
        return $this->hasOne('App\Models\FaqLang', 'faq_id', 'id');
    }
}
