<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class HistoryLike extends Model
{
    protected $table = 'histories_likes';

    public static function is_like($historyId)
    {
        $user = Auth::user();
        if ($user) {

			$is_like = $user->likes
				->where('history_id', $historyId)
				->first();

            if ($is_like) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
