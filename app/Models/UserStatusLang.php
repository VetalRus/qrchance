<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserStatusLang extends Model
{
    protected $table = 'user_status_languages';
    protected $fillable = [
        'status_id', 'title', 'lang_id',
    ];

    public function language()
    {
        return $this->hasOne('App\Models\Language', 'id', 'lang_id');
    }

    public static function createStatus($data)
    {
        $statusToCreate = UserStatus::create();
        $newStatus = UserStatusLang::create([
            'lang_id' => $data['lang_id'],
            'status_id' => $statusToCreate->id,
            'title' =>  $data['title'],
        ]);
        $langs = Language::where('id', '!=', $data['lang_id'])->get();

        foreach ($langs as $key => $item) {
            UserStatusLang::create([
                'lang_id' => $item->id,
                'status_id' => $statusToCreate->id,
            ]);
        }

        return $statusToCreate;
    }

    public static function updateStatus($request, $id)
    {
        $status = UserStatus::find($id);
        $lang_id = $request->get('lang_id');
        $statusLanguage = $status->getLangAll()->where('lang_id', $lang_id)->first();

        $statusLanguage->title = $request->get('title');
        $statusLanguage->save();

        return $status;
    }

    public static function searchStatus($request)
    {
        $dataToValidate = Validator::make($request->all(), [
            'search_name' => 'required|string',
            'search_text' => 'required|string',
        ]);

        $foundStatuses = UserStatus::whereHas('language', function (Builder $query ) use($request){
            $query->where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%');
        })->paginate(5);

        return $foundStatuses;
    }

    public static function deleteStatus($id)
    {
        $status = UserStatus::find($id);
        $statusLanguages = $status->getLangAll();
        foreach ($statusLanguages as $item) {
            $item->delete();
        }
        $status->delete();
    }
}
