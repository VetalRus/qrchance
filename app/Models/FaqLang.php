<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqLang extends Model
{

    protected $table = 'faqs_languages';

    protected $fillable = [
        'faq_id', 'title', 'content', 'lang_id',
	];
    
    public function language()
    {
        return $this->hasOne('App\Models\Language', 'id', 'lang_id');
	}

	public static function createFaq($data)
	{
		$faq = Faq::create();
		$new_faq = FaqLang::create([
			'lang_id' => $data['lang_id'],
			'faq_id' => $faq->id,
			'title' =>  $data['title'],
			'content' =>  $data['content'],
			
		]);
		$langs = Language::where('id', '!=', $data['lang_id'])->get();

		foreach ($langs as $key => $item) {
			FaqLang::create([
				'lang_id' => $item->id,
				'faq_id' => $faq->id,
			]);
		}

		return $faq;
	}

	public static function deleteFaq($id)
	{
		$faq = Faq::find($id);
		$faqLanguages = $faq->getLangAll();
		foreach ($faqLanguages as $item) {
			$item->delete();
		}
        $faq->delete();
	}
}
