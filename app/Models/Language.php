<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Language extends Model
{
    public function histories() {
        return $this->hasMany('App\Models\History');
	}

    public static function flag() {
	   $lang = static::query()->where('iso', App::getLocale())->first();

	   return $lang->flag;
	}

    public static function nowID() {
	   $lang = static::query()->where('iso', App::getLocale())->first();

	   return $lang->id;
	}


}

