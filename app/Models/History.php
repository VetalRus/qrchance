<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class History extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'user_id',
        'lang_id',
        'image',
    ];

    public static function getByLimit($limit = 5, $columns = ['*'])
    {
        $lang_id = Language::nowID();

        return static::query()->where('lang_id', $lang_id)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get(is_array($columns) ? $columns : func_get_args());
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\HistoryLike');
    }

    public static function uploadPhotos($images)
    {

        foreach ($images as $key => $image) {
            $destinationPath = 'img/history/';
            $fileName = 'uid_' . mt_rand() . time() . '.jpg';
            $image->move($destinationPath, $fileName);

            $new_images[] = $destinationPath . $fileName;
        }

        return $new_images;

    }
    public static function deletePhoto($history_id, $image_id)
    {
        $history = static::query()->find($history_id);
        $images = json_decode($history->images, true);

		if ( isset($images[$image_id]) ) {
            unset($images[$image_id]);
            $images = json_encode(array_values($images));
            $history->images = $images;
            $history->save();
		}
		return $history;
    }

    public function language()
    {
        return $this->hasOne('App\Models\Language', 'id', 'lang_id');
    }

    public static function deleteHistory($id)
    {
        $history = History::find($id);
        $history->delete();
    }

}
