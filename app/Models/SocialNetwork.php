<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $table = 'social_networks';
    protected $fillable = [
        'title', 'url', 'icon', 'order',
    ];

    public static function createNetwork($data)
    {

        $networkToCreate = SocialNetwork::create([
            'title' => $data['title'],
            'url' => $data['url'],
            'icon' => $data['icon'],
            'order' => $data['order'],
        ]);

        return $networkToCreate;
    }

    public static function updateNetwork($data, $id)
    {
        $networkToUpdate = SocialNetwork::find($id);

        $networkToUpdate->title = $data->get('title');
        $networkToUpdate->url = $data->get('url');
        $networkToUpdate->icon = $data->get('icon');
        $networkToUpdate->order = $data->get('order');
        $networkToUpdate->save();

        return $networkToUpdate;
    }

    public static function urlPrompt ()
    {
        $urlFromDB = SocialNetwork::first();
        $urlString = substr($urlFromDB->url, 0, 8);

        return $urlString;
    }

    public static function deleteNetwork($id)
    {
        $network = SocialNetwork::find($id);
        $network->delete();
    }
}
