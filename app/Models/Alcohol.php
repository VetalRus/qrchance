<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alcohol extends Model
{
    protected $table = 'alcohols';
    protected $fillable = [
        'title', 'key_translation', 'created_ad',
    ];

    public static function createKey($data)
    {

        $alcoholKey = Alcohol::create([
            'title' => $data['title'],
            'key_translation' => $data['alcoholKey'],
        ]);

        return $alcoholKey;
    }

    public static function updateKey($data, $id)
    {
        $alcoholToUpdate = Alcohol::find($id);

        $alcoholToUpdate->title = $data->get('title');
        $alcoholToUpdate->key_translation = $data->get('alcoholKey');
        $alcoholToUpdate->save();

        return $alcoholToUpdate;
    }

    public static function keyPrompt ()
    {
        $keyFromDB = Alcohol::first();
        $keyString = substr($keyFromDB->key_translation, 0, 35);

        return $keyString;
    }

    public static function deleteKey($id)
    {
        $alcohol = Alcohol::find($id);
        $alcohol->delete();
    }
}
