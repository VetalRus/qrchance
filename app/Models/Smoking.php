<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Smoking extends Model
{
    protected $table = 'smokings';
    protected $fillable = [
        'title', 'key_translation', 'created_ad',
    ];
    public static function createKey($data)
    {

        $smokingKey = Smoking::create([
            'title' => $data['title'],
            'key_translation' => $data['smokingKey'],
        ]);

        return $smokingKey;
    }

    public static function updateKey($data, $id)
    {
        $smokingToUpdate = Smoking::find($id);

        $smokingToUpdate->title = $data->get('title');
        $smokingToUpdate->key_translation = $data->get('smokingKey');
        $smokingToUpdate->save();

        return $smokingToUpdate;
    }

    public static function keyPrompt ()
    {
        $keyFromDB = Smoking::first();
        $keyString = substr($keyFromDB->key_translation, 0, 35);

        return $keyString;
    }

    public static function deleteKey($id)
    {
        $smoking = Smoking::find($id);
        $smoking->delete();
    }
}
