<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $table = 'user_status';
    protected $fillable = [
        'id', 'created_ad', 'updated_at',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function translate()
    {
        $lang_id = Language::nowID();

        $result = $this->hasOne('App\Models\UserStatusLang', 'status_id', 'id')
            ->where([
                ['lang_id', $lang_id],
                ['title', '!=', null],
            ])
            ->first();

        if(!$result) {
            return $this->hasOne('App\Models\UserStatusLang', 'status_id', 'id')
                ->where('title', '!=', null)
                ->first();
        } else {
            return $result;
        }
    }

    public function getLangAll()
    {
        return $this->hasOne('App\Models\UserStatusLang', 'status_id', 'id')->get();
    }

    public function language()
    {
        return $this->hasOne('App\Models\UserStatusLang', 'status_id', 'id');
    }
}
