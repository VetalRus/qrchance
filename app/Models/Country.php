<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Country extends Model
{

    protected $table = 'countries';
    protected $fillable = [
        'id', 'iso', 'flag', 'created_at', 'updated_at',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function translate()
    {

		$lang_id = Language::nowID();

		$result = $this->hasOne('App\Models\CountryLang', 'country_id', 'id')
			->where('lang_id', $lang_id)
			->first();

        /**
         * @todo Нужно будет сделать переводы для всех языков и удалить эти проверки
        */
        if(!$result) {
            return $this->hasOne('App\Models\CountryLang', 'country_id', 'id')
                ->where('lang_id', 1)
                ->first();
        } else {
            return $result;
        }
    }

    public function translateAdmin()
    {
        $lang_id = Language::nowID();

        $result = $this->hasOne('App\Models\CountryLang', 'country_id', 'id')
            ->where([
                ['lang_id', $lang_id],
                ['title', '!=', null],
            ])
            ->first();

        if (!$result) {
            return $this->hasOne('App\Models\CountryLang', 'country_id', 'id')
                ->where('title', '!=', null)
                ->first();
        } else {
            return $result;
        }
    }

    public function language()
    {
     return $this->hasOne('App\Models\CountryLang', 'country_id', 'id');

    }

    public function country()
    {
        return $this->hasOne('App\Models\CountryLang', 'country_id', 'id');
    }

    public function getLangAll()
    {
        return $this->hasOne('App\Models\CountryLang', 'country_id', 'id')->get();
	}
	
	public static function getLangByID($id) {
		$result = static::query()->where('id', $id)->first();

		return $result;
	}
}
