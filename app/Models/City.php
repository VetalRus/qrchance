<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class City extends Model
{

    protected $table = 'cities';
    protected $fillable = [
        'country_id', 'created_ad', 'updated_at',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function translate()
    {
        $lang_id = Language::nowID();

        $result = $this->hasOne('App\Models\CityLang', 'city_id', 'id')
            ->where('lang_id', $lang_id)
            ->first();

        /**
         * @todo Нужно будет сделать переводы для всех языков и удалить эти проверки
        */
        if(!$result) {
            return $this->hasOne('App\Models\CityLang', 'city_id', 'id')
                ->where('lang_id', 1)
                ->first();
        } else {
            return $result;
        }
    }

    public function translateAdmin()
    {
        $lang_id = Language::nowID();

        $result = $this->hasOne('App\Models\CityLang', 'city_id', 'id')
            ->where([
                ['lang_id', $lang_id],
                ['title', '!=', null],
            ])
            ->first();

        if (!$result) {
            return $this->hasOne('App\Models\CityLang', 'city_id', 'id')
                ->where('title', '!=', null)
                ->first();
        } else {
            return $result;
        }
    }

    public function getLangAll()
    {
        return $this->hasOne('App\Models\CityLang', 'city_id', 'id')->get();
    }

    public function language()
    {
        return $this->hasOne('App\Models\CityLang', 'city_id', 'id');
    }

    public function country_translate()
    {
        return $this->hasOne('App\Models\CountryLang', 'id', 'country_id');
	}
	
    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\CityLang', 'city_id', 'id');
	}
    public static function getCountryID($country_id)
	{
		return static::query()->where('country_id', '=', $country_id)->get();
	}
}
