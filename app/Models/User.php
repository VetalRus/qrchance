<?php

namespace App\Models;

use App\Http\Controllers\Admin\HistoryController;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
	const STATUS_ACTIVE = 10;

	// User Status 
	const STATUS_IN_SEATCH_OF = 1;
	const STATUS_IN_RELATIONSHIP = 2;
	const STATUS_IN_NOT_ACTIVE = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'sex',
        'second_name',
        'birthday',
        'status_id',
        'country_id',
        'city_id',
        'children',
        'smoking_id',
        'alcohol_id',
        'profession',
        'phone',
        'instagram',
        'vk',
        'facebook',
		'twitter',
		'youtube',
		'verify_token',
		'verify_status',
		'referral_token',
		'referral_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'email_verified_at' => 'datetime',
		'isAdmin' => 'boolean',
        'isReadyGame' => 'boolean',
//        'children' => 'boolean',
        'sex' => 'boolean'
    ];

    public function histories()
    {
        return $this->hasMany('App\Models\History');
	}

	public function qrCode()
    {
		return $this->hasOne('App\Models\UserQrcodes')
			->where('deleted', 0)
			->first();
	}

	public function country()
    {
		return $this->hasOne('App\Models\Country', 'id', 'country_id');
	}

	public function status()
    {
		return $this->hasOne('App\Models\UserStatus', 'id', 'status_id');
	}

	public function city()
    {
		return $this->hasOne('App\Models\City', 'id', 'city_id');
	}

	public function unreadMessages()
    {
		return $this->hasMany('App\Models\Message', 'user_id', 'id')
            ->where('read', 0)
			->get();

    }

    public function messages()
    {	
        $result = $this->hasMany('App\Models\Message', 'user_id', 'id');
			
		if( !$this->isSearch() ){
			$result->where('read', 1);
		}
		$result = $result->get();

		return $result;
    }

    public static function isAdmin()
    {
        $user = Auth::user();

        if($user->isAdmin) {
            return true;
        } else{
            return false;
        }
	}

    public static function isVerify()
    {
        $user = Auth::user();

        if($user->verify_status === User::STATUS_ACTIVE) {
            return true;
        } else{
            return false;
        }
	}

    public function isRelationship()
    {
		$user = $this;

        if($user->status_id == User::STATUS_IN_RELATIONSHIP) {
            return true;
        } else{
            return false;
        }
	}

    public function isSearch()
    {
		$user = $this;

        if($user->status_id == User::STATUS_IN_SEATCH_OF) {
            return true;
        } else{
            return false;
        }
	}

    public function isNoActive()
    {
		$user = $this;

        if($user->status_id == User::STATUS_IN_NOT_ACTIVE) {
            return true;
        } else{
            return false;
        }
	}

    public static function isReadyGame()
    {
        $user = Auth::user();

        if($user->isReadyGame) {
            return true;
        } else{
            return false;
        }
	}
	
	public function likes()
    {
        return $this->hasMany('App\Models\HistoryLike');
	}
	
	public function alcohol()
    {
		return $this->hasOne('App\Models\Alcohol', 'id', 'alcohol_id');
	}

	public function smoking()
    {
		return $this->hasOne('App\Models\Smoking', 'id', 'smoking_id');
	}

	public static function get_userID_by_referral_token( $referral_token = null ) {

		$referral_id = null;

		if(isset($referral_token)){
			$referral_user = static::query()->where('referral_token', $referral_token)->first();
			$referral_id = (isset($referral_user)) ? $referral_user->id : null;
		}
		

		return $referral_id;
	}

	public static function updateStatus($data)
	{
		$user = Auth::user();
		$user->status_id = $data['status_id'];
		$user->save();

		return $user;
	}

	
	public static function updateUser($data, $id)
    {
        $userToUpdate = User::find($id);
        $dateToUpdate = User::dateOfBirth($data);
        $request = $data->all();
        $request['birthday'] = $dateToUpdate;
        $request['photo'] = "";
        $userToUpdate->update($request);

        return $userToUpdate;
    }

    public static function createUser($data)
    {
            $dateToUpdate = User::dateOfBirth($data);
            $hashedPass = User::passToHash($data['password']);

            $request = $data->all();
//            $request['photo'] = $userPhoto;
            $request['birthday'] = $dateToUpdate;
            $request['password'] = $hashedPass;

            $userToCreate = User::create($request);

            return $userToCreate;
//        }
    }

    public static function uploadPhotos($image)
    {
//        dd($images);

        $destinationPath = 'img/avatars/';
        $fileName = 'uid_' . mt_rand() . time() . '.jpg';
        $image->move($destinationPath, $fileName);
        $imagePath = $destinationPath . $fileName;

        return $imagePath;

    }

    // функция для преобразования полученных из формы разрозненных чисел в дату
    public static function dateOfBirth($data)
    {
        $temp = $data->date_birthday.'.'.$data->month_birthday.'.'.$data->year_birthday;
        $dateToCarbon = Carbon::make($temp);
        return $dateToCarbon;
    }

    public static function passToHash($pass)
    {
        $hashedPass = Hash::make($pass);
        return $hashedPass;
    }

    public static function searchUser($request)
    {
//        dd($request);
        if($request->get('search_name') == 'id') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric|',
            ])->validate();
            $foundUsers = User::where( $request->get('search_name') , '=', $request->get('search_text'))->paginate(5);

        } elseif ($request->get('search_name') == 'name') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|string',
            ]);
            $foundUsers = User::where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%')
                ->orWhere('second_name' , 'like', '%'. $request->get('search_text') .'%')
                ->paginate(5);

        } elseif ($request->get('search_name') == 'sex') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric',
            ]);
            $foundUsers = User::where( $request->get('search_name') , '=', $request->get('search_text'))->paginate(5);
        }
        return $foundUsers;
    }

    public static function userValidation($data)
    {
        $dataToValidate = Validator::make($data->all(), [
            'name' => ['required', 'string', 'max:255'],
            'second_name' => ['nullable', 'string', 'max:255'],
            'isAdmin' => ['required', 'numeric'],
            'isReadyGame' => ['required', 'numeric'],
            'password' => ['required', 'string', 'min:10'],
            'email' => ['required', 'string', 'unique', 'max:255'],
            'date_birthday' => ['nullable', 'numeric'],
            'month_birthday' => ['nullable', 'numeric'],
            'year_birthday' => ['nullable', 'numeric'],
            'country_id' => ['nullable', 'numeric', 'exists:countries, id'],
            'city_id' => ['nullable', 'numeric', 'exists:cities, id'],
            'children' => ['nullable', 'numeric'],
            'alcohol_id' => ['nullable', 'numeric', 'exists:alcohols, id'],
            'smoking_id' => ['nullable', 'numeric', 'exists:smokings, id'],
            'phone' => ['nullable', 'string'],
            'profession' => ['nullable', 'string'],
            'youtube' => ['nullable', 'string', 'url'],
            'instagram' => ['nullable', 'string', 'url'],
            'vk' => ['nullable', 'string', 'url'],
            'facebook' => ['nullable', 'string', 'url'],
            'twitter' => ['nullable', 'string', 'url'],
            'referral_id' => ['nullable', 'numeric'],
        ])->validate();
        if($dataToValidate) {
            return true;
        } else {
            return false;
        }
    }

    public static function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
    }




}
