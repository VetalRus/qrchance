<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class PrintoutPlace extends Model
{
    protected $table = "printout_places";

    protected $fillable = [
        'country_id',
        'city_id',
        'address',
        'email',
        'phone',
        'verified',
    ];

    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public static function updatePlace($data, $id)
    {
        $placeToUpdate = PrintoutPlace::find($id);

        $placeToUpdate->country_id = $data['country_id'];
        $placeToUpdate->city_id = $data['city_id'];
        $placeToUpdate->address = $data['address'];
        $placeToUpdate->email = $data['email'];
        $placeToUpdate->phone = $data['phone'];
        $placeToUpdate->verified = $data['verified'];
        $placeToUpdate->save();

        return $placeToUpdate;
    }

    public static function createPlace($data)
    {

        $placeToCreate = PrintoutPlace::create([
            'country_id' => $data['country_id'],
            'city_id' => $data['city_id'],
            'address' => $data['address'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'verified' => $data['verified'],
        ]);

        return $placeToCreate;
    }

    public static function searchPlace($request)
    {
//        dd($request);
        if($request->get('search_name') == 'id') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric|',
            ])->validate();
            $foundPlaces = PrintoutPlace::where( $request->get('search_name') , '=', $request->get('search_text'))->paginate(5);

        } elseif ($request->get('search_name') == 'country_id') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric',
            ]);
            $foundPlaces = PrintoutPlace::where( $request->get('search_name') , '=', $request->get('search_text'))->paginate(5);

        } elseif ($request->get('search_name') == 'verified') {
            $dataToValidate = Validator::make($request->all(), [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric',
            ]);
            $foundPlaces = PrintoutPlace::where( $request->get('search_name') , '=', $request->get('search_text'))->paginate(5);
        }

        return $foundPlaces;
    }

    public static function deletePlace($id)
    {
        $place = PrintoutPlace::find($id);
        $place->delete();
    }
}
