<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class UserQrcodes extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

	public static function getByUid($uid)
	{
		$result = static::query()
			->where([
				['code', $uid],
				['deleted', 0]
			])->first();

		if(isset($result) && !$result->user->isSearch()) {
			$result = static::query()
			->where([
				['code', $uid],
				['deleted', 0],
				['user_id', "!=" , $result->user->id],
			])->first();
		}

		return $result;
	}

    public static function createQrcode($data)
    {
        $user = Auth::user();

		$oldQrcode = $user->qrCode(); 

		if($oldQrcode){
			$oldQrcode->deleted = true;
			$oldQrcode->save();
		}

        $qrcode = new UserQrcodes();
		
		$uid = ($oldQrcode) ? $oldQrcode->code : $qrcode->randomUid();
        $url = 'img/qrcode/' . $uid . '.png';
        $img = \QRCode::text(route('main.qrcode.show', $uid))->setOutfile(public_path($url))->png();

        $qrcode->description = $data['description'];
        $qrcode->code = $uid;
        $qrcode->img = $url;
        $qrcode->user_id = $user->id;
        $qrcode->save();
	}
	
	/* @todo Дописать генирацию uid */
	protected function randomUid($length = 15)
	{	
		$uid = "uid";
		
		for ($i=0; $i < $length; $i++) { 
			$uid .= random_int(0,9);
		}

		return $uid;
	}
}
