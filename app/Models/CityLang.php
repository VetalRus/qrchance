<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityLang extends Model
{

    protected $table = 'cities_languages';
    protected $fillable = [
        'lang_id', 'city_id', 'title', 'created_at', 'updated_at',
    ];

    public function language()
    {
        return $this->hasOne('App\Models\Language', 'id', 'lang_id');
    }

    public static function createCity($data)
    {
//        dd($data);
        $city = City::create([
            'country_id' =>  $data['country_id'],
        ]);
        $new_city = CityLang::create([
            'lang_id' => $data['lang_id'],
            'city_id' => $city->id,
            'title' =>  $data['title'],
        ]);
        $langs = Language::where('id', '!=', $data['lang_id'])->get();

        foreach ($langs as $key => $item) {
            CityLang::create([
                'lang_id' => $item->id,
                'city_id' => $city->id,
            ]);
        }

        return $city;
    }

    public static function deleteCity($id)
    {
        $city = City::find($id);
        $cityLanguages = $city->getLangAll();
        foreach ($cityLanguages as $item) {
            $item->delete();
        }
        $city->delete();
    }
}
