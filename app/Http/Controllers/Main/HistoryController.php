<?php

namespace App\Http\Controllers\Main;

use App;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\History;
use App\Models\HistoryLike;
use App\Models\Language;
use App\Models\SocialNetwork;
use Auth;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $languages = Language::all();
        $social_networks = SocialNetwork::all();
        $history = History::find($id);
        $is_like = HistoryLike::is_like($id);

        return view('main.history.show', [
            'languages' => $languages,
            'social_networks' => $social_networks,
            'history' => $history,
            'is_like' => $is_like,
        ]);
    }
    public function create()
    {
        $user = Auth::user();
        $languages = Language::all();
        $social_networks = SocialNetwork::all();
        $faqs = Faq::all();

        return view('main.history.create', [
            'languages' => $languages,
            'social_networks' => $social_networks,
            "user" => $user,
        ]);
    }

    public function edit($id)
    {
        $languages = Language::all();
        $user = Auth::user();
        $history = History::find($id);
        $social_networks = SocialNetwork::all();

        if ($user->id == $history->user_id) {
            return view('main.history.edit', [
                'social_networks' => $social_networks,
                "user" => $user,
                'languages' => $languages,
                'history' => $history,
            ]);
        } else {
            return redirect()->route('main.history.show', $id);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|string',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $history = new History();

        if ($request->hasFile('images')) {
            $images = History::uploadPhotos($request->file('images'));

            $history->images = json_encode($images);
        }

        $lang_id = Language::nowID();

        $history->content = $request->get('content');
        $history->user_id = Auth::id();
        $history->lang_id = $lang_id;
        $history->save();

        return redirect()->route('main.history.show', $history->id);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required|string',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $history = History::find($id);

        if ($history->user_id == Auth::id()) {

			$delete_image_id = $request->get('delete_image_id');

			if(isset($delete_image_id)) {
				$history = History::deletePhoto($id, $delete_image_id);
				
			}
			$old_images = json_decode($history->images, true);

			if ($request->hasFile('images')) {
                $new_images = History::uploadPhotos($request->file('images'));
                $new_images = array_merge($new_images, $old_images);

                $history->images = json_encode($new_images);
            }

            $lang_id = Language::nowID();

            $history->content = $request->get('content');
            $history->lang_id = $lang_id;
			$history->save();
			
        }

        return redirect()->route('main.history.show', $history->id);
    }

    public function createLikes($id, Request $request)
    {
        $user = Auth::user();

        $newLike = new HistoryLike();
        if (!$newLike->is_like($id)) {
            $newLike->user_id = $user->id;
            $newLike->history_id = $id;
            $newLike->save();
        }
        return back();
    }

}
