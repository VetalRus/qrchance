<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Faq;
use App\Models\Language;
use App\Models\SocialNetwork;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //   $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languages = Language::all();
        $user = Auth::user();
        $countries = Country::all();
        $cities = City::all();

        $social_networks = SocialNetwork::all();
        $faqs = Faq::all();

        return view('main.account.index', [
            'languages' => $languages,
            'user' => $user,
            'countries' => $countries,
            'cities' => $cities,
            'faqs' => $faqs,
            'social_networks' => $social_networks
        ]);
    }


}
