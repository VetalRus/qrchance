<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\User;
use App\Models\FaqLang;
use App\Models\History;
use App\Models\Language;
use App\Models\SocialNetwork;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $languages = Language::all();
        $social_networks = SocialNetwork::all();

		return view('main.home.index', ['languages' => $languages, 'social_networks' => $social_networks]);
    }

    public function aboutProject()
    {	
		$languages = Language::all();
        $social_networks = SocialNetwork::all();
		$faqs = Faq::take(5)->offset(0)->get();
		$offset = 5;
		
		$is_show = true;
		
		return view('main.home.about_project', [
			'languages' => $languages, 
			'social_networks' => $social_networks, 
			'faqs' => $faqs,
            'is_show' => $is_show,
            'offset' => $offset
		]);

    }
}
