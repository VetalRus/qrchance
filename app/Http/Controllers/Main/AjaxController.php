<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\History;
use App\Models\Language;
use App\Models\City;
use Illuminate\Http\Request;
use Auth;
use Widget;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function faq(Request $request)
    {
        $offset = $request->get('offset');

        $faqs = Faq::take(2)
            ->offset($offset)
            ->get();

        $offset += 2;

        $faqsCount = Faq::count();
        $end = $faqsCount > $offset ? 'no' : 'yes';

        return response()->json([
            'html' => view('widgets.faq_loop', [
                'faqs' => $faqs,
            ])->render(),
            'status' => 200,
            'offset' => $offset,
            'end' => $end,
        ]);
    }

    public function history(Request $request)
    {	
		$col_number = $request->get('col_number') ?? 4;
		$offset = $request->get('offset');
		$take = $request->get('take') ?? 6;
        $not_show_history_id = $request->get('not_show_history_id');
        $lang_id = Language::nowID();
		
        $histories = History::take($take)
            ->offset($offset)
            ->where([
                ['id', '!=', $not_show_history_id],
                ['lang_id', $lang_id],
            ])
            ->orderBy('created_at', 'desc')
            ->get();

        $offset += $take;

        $historiesCount = History::count();
        $end = $historiesCount > $offset ? 'no' : 'yes';

        return response()->json([
            'html' => view('widgets.history_loop', [
				'histories' => $histories,
				'col_number' => $col_number,
            ])->render(),
            'status' => 200,
            'offset' => $offset,
            'end' => $end,
        ]);
	}
	
    public function deletePhotoHistory(Request $request, $id)
    {
        $delete_image_id = $request->get('delete_image_id');
		
		$history = History::deletePhoto($id, $delete_image_id);

        return response()->json([
			'html' => view('widgets.history_edit_slider', [
                'history' => $history,
            ])->render(),
			'status' => 200,
        ]);
	}
	
	public function getCityList(Request $request)
    {
        $country_id = $request->get('current_country_id');
		   $cities = City::where('country_id', '=', $country_id)->get();
		   $user = Auth::user();
        
        return response()->json([
			'html' => view('widgets.city_loop', [
				'cities' => $cities,
				'user' => $user,
            ])->render(),
			'status' => 200,
			'message' => 'get',
		]);
	}
	
	public function inputPhone(Request $request)
    {	
		$country_id = $request->get('current_country_id');
		$user = Auth::user();
        $phone_mask = \App\Models\PhoneMask::where('country_id', $country_id)->first();
		$placeholder_phone = ($phone_mask) ? $phone_mask->phone : __('main.account.qr.phone_placeholder') ;
		$config['phone'] = $user->phone;

		return response()->json([
			'html' => view('widgets.account.input_phone', [
				'config' => $config,
				'placeholder_phone' => $placeholder_phone,
			])->render(),
			'status' => 200,
			'message' => 'succses',
		]);
	}
	

}
