<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Alcohol;
use App\Models\City;
use App\Models\Country;
use App\Models\Faq;
use App\Models\User;
use App\Models\History;
use App\Models\Language;
use App\Models\Message;
use App\Models\PrintoutPlace;
use App\Models\Smoking;
use App\Models\UserStatus;
use App\Models\SocialNetwork;
use App\Models\UserQrcodes;
use App\Models\PhoneMask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PDF;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languages = Language::all();
        $user = Auth::user();
        $countries = Country::all();
	
        $alcohols = Alcohol::all();
        $smokings = Smoking::all();
		
		$statuses = UserStatus::where('id', '!=', $user->status_id)->get();
        $social_networks = SocialNetwork::all();
        $faqs = Faq::all();

        return view('main.account.index', [
            'languages' => $languages,
            'user' => $user,
            'countries' => $countries,
            'smokings' => $smokings,
            'alcohols' => $alcohols,
           
            'faqs' => $faqs,
			'social_networks' => $social_networks,
			'statuses' => $statuses,
        ]);
    }

    public function businessCard()
    {
        $languages = Language::all();
        $user = Auth::user();
        $messages = Message::where('user_id', $user->id)->get();
        $printoutPlaces = PrintoutPlace::where('city_id', $user->city_id)->get();
        $cities = City::where('country_id', $user->country_id)->get();

		$statuses = UserStatus::where('id', '!=', $user->status_id)->get();
        $social_networks = SocialNetwork::all();

        return view('main.account.business-card', [
            'languages' => $languages,
            'user' => $user,
            'messages' => $messages,
            'social_networks' => $social_networks,
            'printoutPlaces' => $printoutPlaces,
			'cities' => $cities,
			'statuses' => $statuses,
			
        ]);
    }

    public function messages()
    {	
        $languages = Language::all();
        $user = Auth::user();
        $messages = $user->messages();

		$statuses = UserStatus::where('id', '!=', $user->status_id)->get();
        $social_networks = SocialNetwork::all();

        return view('main.account.message', [
            'user' => $user,
            'messages' => $messages,
			'social_networks' => $social_networks,
			'statuses' => $statuses,
			'languages' => $languages,
        ]);
    }

    public function histories()
    {
        $languages = Language::all();
        $user = Auth::user();
        $histories = History::getByLimit(3);
        $countries = Country::all();
        $cities = City::all();
        $social_networks = SocialNetwork::all();
        $faqs = Faq::all();
		$statuses = UserStatus::where('id', '!=', $user->status_id)->get();

        return view('main.account.histories', [
            'languages' => $languages,
            'user' => $user,
            'faqs' => $faqs,
            'histories' => $histories,
            'countries' => $countries,
            'cities' => $cities,
			'social_networks' => $social_networks,
			'statuses' => $statuses,
			
        ]);
    }

    public function security()
    {
        $languages = Language::all();
        $user = Auth::user();

        $social_networks = SocialNetwork::all();
		$statuses = UserStatus::where('id', '!=', $user->status_id)->get();

        return view('main.account.security', [
            'languages' => $languages,
            'user' => $user,
			'social_networks' => $social_networks,
			'statuses' => $statuses,
			
        ]);
    }

    public function messageDelete($id)
    {
        $user = Auth::user();
        $message = Message::where('id', $id)->where('user_id', $user->id)->delete();

        if ($message) {
            return response()->json(['status' => 200, 'message' => 'deleted', 'count' => Message::where('user_id', $user->id)->count()]);
        } else {
            return response()->json(['status' => 400, 'message' => 'error']);
        }
    }

    public function messageRead($id)
    {
        $user = Auth::user();
        $message = Message::where('id', $id)->where('user_id', $user->id)->first();

        if ($message) {
            $message->read = 1;
            $message->save();
            return response()->json(['status' => 200, 'id' => $id, 'message' => 'read', 'count' => Message::where('user_id', $user->id)->where('read', 0)->count()]);
        } else {
            return response()->json(['status' => 400, 'message' => 'error']);
        }
    }

    public function uploadAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $user = Auth::user();

        if ($request->hasFile('avatar')) {

            $destinationPath = 'img/avatars/';
            $fileName = 'uid_' . $user->id . time() . '.jpg';

            $request->file('avatar')->move($destinationPath, $fileName);

            $user->photo = $destinationPath . $fileName;
            $user->save();

            $response = [
                'status' => 200,
                'message' => 'success',
                'photo_url' => $user->photo,
            ];
            return response()->json($response);
        }
    }

    public function updateInfo(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'second_name' => ['required', 'string', 'max:255'],
            'date_birthday' => ['required', 'numeric'],
            'month_birthday' => ['required', 'numeric'],
            'year_birthday' => ['required', 'numeric'],
            'country_id' => ['required', 'numeric'],
            'city_id' => ['required', 'numeric'],
            'children' => ['required', 'numeric'],
            'alcohol_id' => ['required', 'numeric'],
            'smoking_id' => ['required', 'numeric'],
            'phone' => ['nullable', 'string'],
            'profession' => ['required', 'string'],
            'youtube' => ['nullable', 'string', 'url'],
            'instagram' => ['nullable', 'string', 'url'],
            'vk' => ['nullable', 'string', 'url'],
            'facebook' => ['nullable', 'string', 'url'],
            'twitter' => ['nullable', 'string', 'url'],

        ])->validate();
		
		$user = Auth::user();
		if( $user->photo == '/img/avatars/user.svg' ) {
			return redirect()->route('main.account.index')
				->withErrors(['error_modal' => 'main.error.upload_avatar' ])
				->withInput();
		}
        $user->name = $request->get('name');
        $user->second_name = $request->get('second_name');
        $user->birthday = date('Y-m-d',
            strtotime($request->get('date_birthday')
                . '.' . $request->get('month_birthday')
                . '.' . $request->get('year_birthday')));
        $user->country_id = $request->get('country_id');
        $user->city_id = $request->get('city_id');
        $user->children = $request->get('children');
        $user->alcohol_id = $request->get('alcohol_id');
        $user->smoking_id = $request->get('smoking_id');
        $user->phone = $request->get('phone');
        $user->profession = $request->get('profession');
        $user->phone = $request->get('phone');
        $user->youtube = $request->get('youtube');
        $user->instagram = $request->get('instagram');
        $user->vk = $request->get('vk');
        $user->facebook = $request->get('facebook');
        $user->twitter = $request->get('twitter');
        $user->isReadyGame = 1;
        $user->save();

		return redirect()->route('main.account.index')
			->with('success_modal', 'main.account.succses.update-info');
    }

    public function createQrCode(Request $request)
    {
        $this->validate($request, [
            'description' => ['required', 'string'],
        ]);

		UserQrcodes::createQrcode( $request->only(['description']) );
		$user = Auth::user();
		$printoutPlaces = PrintoutPlace::where('city_id', $user->city_id)->get();
		$cities = City::where('country_id', $user->country_id)->get();
		
        return response()->json([
			'status' => 'success',
			'message_key' =>  __("main.succses.business_card"),
			'html' => view('widgets.account.business_card_download_pdf', [
				'user' => $user,
				'printoutPlaces' => $printoutPlaces,
				'cities' => $cities,
			])->render(),
		]);

    }

    public function updateSecurity(Request $request)
    {
        $this->validate($request, [
            'old_password' => ['required', 'string', 'min:6'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $user = Auth::user();
        $old_password = $request->get('old_password');

        if (Hash::check($old_password, $user->password)) {

            $user->password = Hash::make($request->get('password'));

            $user->save();
        }

        return redirect()->route('main.account.security');

    }

    public function createReferral(Request $request)
    {
        $user = Auth::user();

        if (!isset($user->referral_token)) {

            $user->referral_token = Str::random();

            $user->save();
        }

        return redirect()->route('main.account.security');

    }

    public function addPrintoutPlace(Request $request)
    {
        $country = Country::where('id', $request->get('city_id'))->first();
        $entity = new PrintoutPlace();
        $entity->country_id = $country->id;

        $entity->city_id = $request->get('city_id');
        $entity->address = $request->get('address');
        $entity->phone = $request->get('phone');
        $entity->email = $request->get('email');
        $entity->save();

        return response()->json(['status' => 200, 'message' => 'success']);
    }

    public function downloadBusinessCard(Request $request)
    {
        $user = Auth::user();
        $qrcode = $user->qrCode();

        if (isset($qrcode)) {
            view()->share('qrcode_img', $qrcode->img);

            if ($request->has('download')) {
                $pdf = PDF::loadView('pdf.business-card');
                return $pdf->download('business-card.pdf');
                // return $pdf->stream();
            }
		} else {
			return redirect()->route('main.account.business-card')->withErrors(['error_modal' => 'main.error.no_exist_qrcode']);

		}
		
	}
	
	public function updateStatus(Request $request)
	{
		$user = User::updateStatus($request);

		if( $user->isRelationship() ){
			return redirect()
				->route('main.account.histories')
				->withErrors(['error_modal' => 'main.account.warning.create_history']);

		} else if( $user->isSearch() ){
			return redirect()
				->route('main.account.messages')
				->withErrors(['error_modal' => 'main.account.warning.new_view_messages']);
		}
		return redirect()->route('main.account.index')->withErrors(['error_modal' => 'main.account.warning.update_status']);
	}


}
