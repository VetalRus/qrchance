<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Language;
use App\Models\SocialNetwork;
use App\Models\UserQrcodes;
use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\User;


class QRCodeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //   $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function index($uid)
//    {
//        $languages = Language::all();
//        $user = User::find($uid);
//        $social_networks = SocialNetwork::all();
//        $faqs = Faq::all();
//
//        return view('main.qrcode.index', [
//            'languages' => $languages,
//            'user' => $user,
//            'faqs' => $faqs,
//            'social_networks' => $social_networks
//        ]);
//    }

    public function sendMessage($uid, Request $request)
    {
        $user = User::find($uid);
        $message = new Message();
        $message->user_id = $user->id;
        $message->name = $request->get('name');
        $message->phone = $request->get('phone');
        $message->deleted = 0;
        $message->message = 'Привет. Ты мне очень понравилась. Поужинаем вместе?';
        $message->save();

        return redirect()->route('main.home.index');
    }


    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($uid)
    {
        $languages = Language::all();
        $social_networks = SocialNetwork::all();
		
        $qr_code = UserQrcodes::getByUid($uid);

		if(isset($qr_code)){
			return view('main.qrcode.show', [
				'languages' => $languages,
				'social_networks' => $social_networks,
				'qr_code' => $qr_code,
			]);
		}

		return redirect()->route('main.home.index')->withErrors(['error_modal' => 'main.warning.user_stopped_searching']);
    }

}
