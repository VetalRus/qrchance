<?php

namespace App\Http\Controllers\Main;

use App\Mail\FeedbackMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send_form(Request $request)
    {
        
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $text = $request->get('text');
        
        Mail::to('qrchanceproject@gmail.com')->send(new FeedbackMail($name, $email, $phone, $text));
        return redirect('aboutproject')->with('message','Ваше сообщение отправлено');
    }
}
    
