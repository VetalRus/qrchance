<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\CountryLang;
use App\Models\Language;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CountryLangController extends Controller
{
    public function index()
    {
        $countries = Country::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.country_lang.index', [
            'countries' => $countries,
        ]);
    }

    public function create()
    {
        $languages = Language::all();

        return view('admin.country_lang.create', [
            'languages' => $languages,
        ]);
    }

    public function store(Request $request)
    {
        $country = CountryLang::createCountry($request);

        return redirect()->route('admin.country_lang.edit', $country->id);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $languages = Language::all();
        $country = Country::find($id);

        return view('admin.country_lang.edit', [
            'languages' => $languages,
            'country' => $country,
        ]);
    }

	public function update(Request $request, $id)
    {
//        dd($request);
        $country = Country::find($id);
        $country->iso = $request->get('isoName');
        $country->flag = $request->get('flagName');
        $lang_id = $request->get('lang_id');
        $countryLanguage = $country->getLangAll()->where('lang_id', $lang_id)->first();

        $countryLanguage->title = $request->get('title');
//        $faqLanguage->content = $request->get('content');
        $country->save();
        $countryLanguage->save();

        return redirect()->route('admin.country_lang.edit', $country->id);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search_name' => 'required|string',
            'search_text' => 'required|string',
        ]);

        $countries = Country::whereHas('language', function (Builder $query ) use($request){
            $query->where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%');
        })->paginate(5);

        return view('admin.country_lang.index', [
            'countries' => $countries,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function destroy($id)
    {
        Countrylang::deleteCountry($id);

        return redirect()->route('admin.country_lang.index');
    }
}
