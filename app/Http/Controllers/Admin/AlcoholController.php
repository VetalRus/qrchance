<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alcohol;
use Illuminate\Http\Request;

class AlcoholController extends Controller
{
    public function index()
    {
        $attitudes = Alcohol::all();

        return view('admin.alcohol.index', [
            'attitudes' => $attitudes,
        ]);
    }

    public function create()
    {
        $key = Alcohol::keyPrompt();

        return view('admin.alcohol.create', [
            'key' => $key,
        ]);
    }

    public function store(Request $request)
    {
        $alcohol = Alcohol::createKey($request->only('title', 'alcoholKey'));

        return redirect()->route('admin.alcohol.edit', $alcohol->id);
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $alcohol = Alcohol::find($id);

        return view('admin.alcohol.edit', [
            'alcohol' => $alcohol,
        ]);
    }

    public function update(Request $request, $id)
    {
        $alcohol = Alcohol::updateKey($request, $id);

        return redirect()->route('admin.alcohol.edit', $alcohol->id);
    }

    public function destroy($id)
    {
        Alcohol::deleteKey($id);

        return redirect()->route('admin.alcohol.index');
    }
}
