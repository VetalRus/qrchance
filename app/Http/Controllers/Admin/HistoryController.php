<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\History;
use App\Models\HistoryLang;
use App\Models\Language;
use App\Models\SocialNetwork;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {
        $histories = History::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.history.index', [
            'histories' => $histories,
        ]);
    }

    public function create()
    {
        $users = User::all();
        $languages = Language::all();
        $social_networks = SocialNetwork::all();


        return view('admin.history.create', [
            'languages' => $languages,
            'social_networks' => $social_networks,
            "users" => $users,
        ]);
    }

    public function edit($id)
    {
        $languages = Language::all();

        $history = History::find($id);

        return view('admin.history.edit', [
                'languages' => $languages,
                'history' => $history,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|string',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $history = new History();

        if ($request->hasFile('images')) {
            $images = History::uploadPhotos($request->file('images'));

            $history->images = json_encode($images);
        }

        $lang_id = $request->get('lang_id');
        $history->content = $request->get('content');
        $history->user_id = $request->get('user_id');
        $history->lang_id = $lang_id;
        $history->save();

        return redirect()->route('admin.history.index', $history->id);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required|string',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $history = History::find($id);
        $lang_id = $request->get('lang_id');

        $delete_image_id = $request->get('delete_image_id');

        if(isset($delete_image_id)) {
            $history = History::deletePhoto($id, $delete_image_id);

        }
        $old_images = json_decode($history->images, true);

        if ($request->hasFile('images')) {
            $new_images = History::uploadPhotos($request->file('images'));
            $new_images = array_merge($new_images, $old_images);

            $history->images = json_encode($new_images);
        }


        $history->content = $request->get('content');
        $history->lang_id = $lang_id;
        $history->save();

        return redirect()->route('admin.history.index', $history->id);
    }

    public function search(Request $request)
    {
//        dd($request->get('search_name'));
        if($request->get('search_name') == 'user_id') {
            $this->validate($request, [
                'search_name' => 'required|string',
                'search_text' => 'required|numeric|exists:App\Models\User,id',
            ]);
            $histories = History::whereHas('user', function (Builder $query ) use($request){
                $query->where( $request->get('search_name') , '=', $request->get('search_text'));
            })->paginate(5);
        } elseif ($request->get('search_name') == 'name') {
            $this->validate($request, [
                'search_name' => 'required|string',
                'search_text' => 'required|string',
            ]);
            $histories = History::whereHas('user', function (Builder $query ) use($request){
                $query->where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%');
            })->paginate(5);
        } else {
            $this->validate($request, [
                'search_name' => 'required|string',
                'search_text' => 'required|string',
            ]);
            $histories = History::whereHas('language', function (Builder $query ) use($request){
                $query->where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%');
            })->paginate(5);
        }

        return view('admin.history.index', [
            'histories' => $histories,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function show($id)
    {

    }

    public function destroy($id)
    {
        History::deleteHistory($id);

        return redirect()->route('admin.history.index');
    }
}
