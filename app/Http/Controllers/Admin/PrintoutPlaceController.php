<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\PrintoutPlace;
use Illuminate\Http\Request;

class PrintoutPlaceController extends Controller
{
    public function index()
    {
        $places = PrintoutPlace::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.printout_place.index', [
            'places' => $places,
        ]);
    }

    public function create()
    {
        $place = PrintoutPlace::all();
        $countries = Country::all();
        $cities = City::all();

        return view('admin.printout_place.create', [
            'place' => $place,
            'countries' => $countries,
            'cities' => $cities,
        ]);
    }

    public function store(Request $request)
    {
        $place = PrintoutPlace::createPlace($request);

        return redirect()->route('admin.printout_place.edit', $place->id);
    }

    public function search(Request $request)
    {
        $places = PrintoutPlace::searchPlace($request);

        return view('admin.printout_place.index', [
            'places' => $places,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function edit($id)
    {
        $place = PrintoutPlace::find($id);
        $countries = Country::all();
        $cities = City::all();

        return view('admin.printout_place.edit', [
            'place' => $place,
            'countries' => $countries,
            'cities' => $cities,
        ]);
    }

    public function update(Request $request, $id)
    {
        $place = PrintoutPlace::updatePlace($request, $id);

        return redirect()->route('admin.printout_place.edit', $place->id);
    }

    public function destroy($id)
    {
        PrintoutPlace::deletePlace($id);

        return redirect()->route('admin.printout_place.index');
    }
}
