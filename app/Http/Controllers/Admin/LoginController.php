<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = 'admin/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware( 'guest' )->except( 'admin.logout' );
    }

    public function index()
    {
        if ( Auth::check() ) {
            return redirect()->route( 'admin.main.dashboard' );
        }

        return view( 'admin.login' );
    }

    /**
     * Обработка попытки аутентификации.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */

    public function authenticate( Request $request )
    {

        $request->validate( [
            'email' => 'required|email|exists:users',
            'password' => 'required',
        ] );

        $credentials = $request->only( 'email', 'password' );

        if ( Auth::attempt( $credentials ) ) {

            if ( !Auth::user()->isAdmin() ) {
                Auth::logout();
				return back()->withErrors( 'You are denied access!' );
				
            } else if (!Auth::user()->isVerify()) {
				Auth::logout();
				return back()->withErrors( 'You are denied access!' );
			}

            // Аутентификация успешна...
            return redirect()->route( 'admin.main.dashboard' );

        } else{
            // Пароль не верен...
            return back()
                ->withErrors(['password' => 'The password is incorrect.'])
                ->withInput();
        }

        return back();
    }

    protected function redirectTo()
    {
        return route( 'admin.main.dashboard' );
    }

    public function logout( Request $request )
    {
        Auth::logout();
        return redirect()->route( 'admin.login' );

    }
}
