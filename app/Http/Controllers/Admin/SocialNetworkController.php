<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SocialNetwork;
use Illuminate\Http\Request;

class SocialNetworkController extends Controller
{
    public function index()
    {
        $networks = SocialNetwork::all();

        return view('admin.social_network.index', [
            'networks' => $networks,
        ]);
    }

    public function create()
    {
        $url = SocialNetwork::urlPrompt();

        return view('admin.social_network.create', [
            'url' => $url,
        ]);
    }

    public function store(Request $request)
    {
        $network = SocialNetwork::createNetwork($request->only('title', 'url', 'icon', 'order'));

        return redirect()->route('admin.social_network.edit', $network->id);
    }

    public function edit($id)
    {
        $network = SocialNetwork::find($id);

        return view('admin.social_network.edit', [
            'network' => $network,
        ]);
    }

    public function update(Request $request, $id)
    {

        $network = SocialNetwork::updateNetwork($request, $id);

        return redirect()->route('admin.social_network.edit', $network->id);
    }

    public function destroy($id)
    {
        SocialNetwork::deleteNetwork($id);

        return redirect()->route('admin.social_network.index');
    }
}
