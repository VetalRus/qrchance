<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alcohol;
use App\Models\City;
use App\Models\Country;
use App\Models\Smoking;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
       $users = User::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.user.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        $user = User::all();
        $countries = Country::all();
        $cities = City::all();
        $smokings = Smoking::all();
        $alcohols = Alcohol::all();
        return view('admin.user.create', [
            'user' => $user,
            'countries' => $countries,
            'cities' => $cities,
            'smokings' => $smokings,
            'alcohols' => $alcohols,
        ]);
    }

    public function store(Request $request)
    {
        $user = User::createUser($request);

        $this->validate($request, [
            'name' => 'required|string',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('images')) {
            $image = User::uploadPhotos($request->file('images'));

            $user->photo = $image;
        }


        $user->save();
        return redirect()->route('admin.user.edit', $user->id);
    }

    public function search(Request $request)
    {
        $users = User::searchUser($request);

        return view('admin.user.index', [
            'users' => $users,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $countries = Country::all();
        $cities = City::all();
        $smokings = Smoking::all();
        $alcohols = Alcohol::all();
        return view('admin.user.edit', [
            'user' => $user,
            'countries' => $countries,
            'cities' => $cities,
            'smokings' => $smokings,
            'alcohols' => $alcohols,
        ]);
    }


    public function update(Request $request, $id)
    {
        $user = User::updateUser($request, $id);

        $this->validate($request, [
            'name' => 'required|string',
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('images')) {
            $image = User::uploadPhotos($request->file('images'));

            $user->photo = $image;
            $user->save();
        }



        return redirect()->route('admin.user.edit', $user->id);
    }

    public function destroy($id)
    {
        User::deleteUser($id);

        return redirect()->route('admin.user.index');
    }
}
