<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Smoking;
use Illuminate\Http\Request;

class SmokingController extends Controller
{
    public function index()
    {
        $attitudes = Smoking::all();

        return view('admin.smoking.index', [
            'attitudes' => $attitudes,
        ]);
    }

    public function create()
    {
        $key = Smoking::keyPrompt();

        return view('admin.smoking.create', [
            'key' => $key,
        ]);
    }

    public function store(Request $request)
    {
        $smoking = Smoking::createKey($request->only('title', 'smokingKey'));

        return redirect()->route('admin.smoking.edit', $smoking->id);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $smoking = Smoking::find($id);

        return view('admin.smoking.edit', [
            'smoking' => $smoking,
        ]);
    }

    public function update(Request $request, $id)
    {
        $smoking = Smoking::updateKey($request, $id);

        return redirect()->route('admin.smoking.edit', $smoking->id);
    }

    public function destroy($id)
    {
        Smoking::deleteKey($id);

        return redirect()->route('admin.smoking.index');
    }
}
