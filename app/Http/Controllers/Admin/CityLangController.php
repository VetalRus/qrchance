<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\CityLang;
use App\Models\Country;
use App\Models\Language;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CityLangController extends Controller
{
    public function index($country_id)
    {	
		$country = Country::find($country_id);
        $cities = City::where('country_id', $country_id)->orderBy('created_at', 'desc')->paginate(5);
        return view('admin.city_lang.index', [
            'cities' => $cities,
            'country' => $country,
        ]);
    }

    public function create($country_id)
    {
        $languages = Language::all();
        $countries = Country::all();
		$country = Country::find($country_id);

        return view('admin.city_lang.create', [
            'languages' => $languages,
            'countries' => $countries,
            'country' => $country,
        ]);
    }

    public function store(Request $request)
    {
        $city = CityLang::createCity($request->only('lang_id', 'title', 'city_id', 'country_id'));
        return redirect()->route('admin.city_lang.edit', $city->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();
        $countries = Country::all();
        $city = City::find($id);


        return view('admin.city_lang.edit', [
            'languages' => $languages,
            'countries' => $countries,
            'city' => $city,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::find($id);
        $city->country_id = $request->get('country_id');
        $lang_id = $request->get('lang_id');
        $cityLanguage = $city->getLangAll()->where('lang_id', $lang_id)->first();

        $cityLanguage->title = $request->get('title');
//        $faqLanguage->content = $request->get('content');

        $cityLanguage->save();

        return redirect()->route('admin.city_lang.edit', $city->id);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search_name' => 'required|string',
            'search_text' => 'required|string',
		]);
		
        if($request->get('search_name') == 'city') {
            $cities = City::whereHas('language', function (Builder $query) use ($request) {
                $query->where('title', 'like', '%' . $request->get('search_text') . '%');
            })->paginate(5);

        } 

        return view('admin.city_lang.index', [
            'cities' => $cities,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function destroy($id)
    {
        CityLang::deleteCity($id);

        return redirect()->route('admin.city_lang.index');
    }
}
