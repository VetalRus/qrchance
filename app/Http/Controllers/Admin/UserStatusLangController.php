<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\UserStatus;
use App\Models\UserStatusLang;
use Illuminate\Http\Request;

class UserStatusLangController extends Controller
{
    public function index()
    {
        $statuses = UserStatus::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.user_status_lang.index', [
            'statuses' => $statuses,
        ]);
    }

    public function create()
    {
        $languages = Language::all();

        return view('admin.user_status_lang.create', [
            'languages' => $languages,
        ]);
    }

    public function store(Request $request)
    {
        $status = UserStatusLang::createStatus($request->only('lang_id', 'title'));

        return redirect()->route('admin.user_status_lang.edit', $status->id);
    }

    public function search(Request $request)
    {
        $statuses = UserStatusLang::searchStatus($request);

        return view('admin.user_status_lang.index', [
            'statuses' => $statuses,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function edit($id)
    {
        $languages = Language::all();
        $status = UserStatus::find($id);

        return view('admin.user_status_lang.edit', [
            'languages' => $languages,
            'status' => $status,
        ]);
    }

    public function update(Request $request, $id)
    {
        $status = UserStatusLang::updateStatus($request, $id);

        return redirect()->route('admin.user_status_lang.edit', $status->id);
    }

    public function destroy($id)
    {
        UserStatuslang::deleteStatus($id);

        return redirect()->route('admin.user_status_lang.index');
    }
}
