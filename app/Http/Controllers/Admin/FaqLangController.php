<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\FaqLang;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class FaqLangController extends Controller
{
    public function index()
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.faq_lang.index', [
            'faqs' => $faqs,
        ]);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'search_name' => 'required|string',
            'search_text' => 'required|string',
        ]);

		$faqs = Faq::whereHas('language', function (Builder $query ) use($request){
			$query->where( $request->get('search_name') , 'like', '%'. $request->get('search_text') .'%');
		})->paginate(5);
		
        return view('admin.faq_lang.index', [
			'faqs' => $faqs,
        ])->with($request->only('search_name', 'search_text'));
    }

    public function create()
    {
        $languages = Language::all();

        return view('admin.faq_lang.create', [
            'languages' => $languages,
        ]);
    }

    public function store(Request $request)
    {
        $faq = Faqlang::createFaq($request->only('lang_id', 'title', 'content'));

        return redirect()->route('admin.faq_lang.edit', $faq->id);

    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $languages = Language::all();
        $faq = Faq::find($id);

        return view('admin.faq_lang.edit', [
            'languages' => $languages,
            'faq' => $faq,
        ]);
    }

    public function update($id, Request $request)
    {
        $faq = Faq::find($id);
        $lang_id = $request->get('lang_id');
        $faqLanguage = $faq->getLangAll()->where('lang_id', $lang_id)->first();

        $faqLanguage->title = $request->get('title');
        $faqLanguage->content = $request->get('content');
        $faqLanguage->save();

        return redirect()->route('admin.faq_lang.edit', $faq->id);
    }
    public function destroy($id)
    {
        Faqlang::deleteFaq($id);

        return redirect()->route('admin.faq_lang.index');
    }
}
