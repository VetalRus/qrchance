<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FaqLang;
use App\Models\Language;
use Illuminate\Http\Request;

class MainController extends Controller
{

    /**
     * Show
     *
     * @return View
     */
    public function show()
    {
        return view('admin.main.show');
	}
	
    public function dashboard()
    {
        return view('admin.main.dashboard');
    }

    public function update($id, Request $request)
    {
        $langs = Language::all();

        foreach ($langs as $item) {
            $faq_lang = FaqLang::where('faq_id', $id)->where('lang_id', $item->id)->first();

            if($faq_lang == null) {
                $faq_lang = new FaqLang();
            }

            $faq_lang->title = $request->get('title_' . $item->iso);
            $faq_lang->content = $request->get('title_' . $item->iso);

            $faq_lang->save();
        }
    }

}
