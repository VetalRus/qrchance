<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Authenticate
{

    public function handle($request, Closure $next)
    {	
		if (!Auth::user()) {
            return redirect()->route('main.home.index')->withErrors(['error_modal' => 'main.error.authenticate' ]);
		}
		
        return $next($request);
    }
}
