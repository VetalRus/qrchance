<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;


class isReadyGame
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {	
		if (!User::isReadyGame()) {
            return redirect()->route('main.account.index')->withErrors(['error_modal' => 'main.error.fill_inputs' ]);
		}
		
        return $next($request);
    }
}
