<?php

namespace App\Http\Middleware\Admin;

use Closure;
use App\Models\User;

class IsAdmin
{

    public function handle($request, Closure $next)
    {
        if (!User::isAdmin()) {
            return redirect()->route('admin.login')->withErrors(['error_modal' => 'main.error.admin.is-admin' ]);
        }

        return $next($request);
    }
}
