<?php

namespace App\Http\Middleware\Admin;

use Auth;
use Closure;

class Authenticate
{

    public function handle($request, Closure $next)
    {	
		if (!Auth::user()) {
            return redirect()->route('admin.login')->withErrors(['error_modal' => 'main.error.admin.authenticate' ]);
		}
		
        return $next($request);
    }
}
