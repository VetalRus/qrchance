<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;


class isRelationship
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {	
		if (!User::isRelationship()) {
            return redirect()->route('main.account.index')->withErrors(['error_modal' => 'main.error.change-status-relationship' ]);
		}
		
        return $next($request);
    }
}
