<?php

namespace App\Widgets;

use App\Models\Language;
use Arrilot\Widgets\AbstractWidget;

class HeaderLanguages extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $languages = Language::all();

        return view('widgets.header_languages', [
            'config' => $this->config,
            'languages' => $languages
        ]);
    }
}
