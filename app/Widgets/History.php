<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Language;
use Auth;

class History extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
		"count_colum" => 3
	];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {	
		$config = $this->config;
		$count_colum = $this->config['count_colum'];
		$offset = ($count_colum * 2);
		$offset = (Auth::user() && Auth::user()->isRelationship()) ? $offset - 1 : $offset;
		$col_number = 12 / $count_colum;

		$lang_id = Language::nowID();
        $histories = \App\Models\History::take($offset)
            ->offset(0)
			->orderBy('created_at', 'desc')
			->where('lang_id', $lang_id)
			->get();
		

        return view('widgets.history', [
            'config' => $config,
			'histories' => $histories,
			'col_number' => $col_number,
            'offset' => $offset,
            'take' => ++$offset,
        ]);
    }

}
