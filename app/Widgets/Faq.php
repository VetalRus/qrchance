<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;

class Faq extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $faqs = \App\Models\Faq::take(5)->offset(0)->get();
		$offset = 5;
		
		$is_show = true;

        return view('widgets.faq', [
            'config' => $this->config,
            'faqs' => $faqs,
            'is_show' => $is_show,
            'offset' => $offset
        ]);
    }

}
