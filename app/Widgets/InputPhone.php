<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;
use Auth;

class InputPhone extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
		'phone' => '',
	];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$user = Auth::user();
        $phone_mask = \App\Models\PhoneMask::where('country_id', $user->country_id)->first();
		$placeholder_phone = ($phone_mask) ? $phone_mask->phone : __('main.account.qr.phone_placeholder') ;

		return view('widgets.account.input_phone', [
            'config' => $this->config,
            'placeholder_phone' => $placeholder_phone,
        ]);
    }

}
