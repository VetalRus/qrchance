<?php

namespace App\Widgets;

use App\Models\Language;
use Arrilot\Widgets\AbstractWidget;
use Carbon\Carbon;
use App\Models\FreePeriod;

class HeaderTime extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$period = FreePeriod::where('deleted', 0)->first();
        $created = new Carbon($period->end_date);
		$now = Carbon::now();
		$difference = $created->diff($now)->format('%d');

		return view('widgets.header_time', [
            'config' => $this->config,
            'difference' => $difference
        ]);
    }
}
