<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $name;
    protected $email;
    protected $tel;
    protected $text;

    /**
     * Create a new text instance.
     *
     * @return void
     */
    public function __construct($name, $email, $tel, $text)
    {
        $this->name = $name;
        $this->email = $email;
        $this->tel = $tel;
        $this->text = $text;
    }

    /**
     * Build the text.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.feedback')
            ->with([
                'name'=>$this->name,
                'email'=>$this->email,
                'tel'=>$this->tel,
                'text'=>$this->text,
            ])
            ->subject('QRchance');
    }
}
