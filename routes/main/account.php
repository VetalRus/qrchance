<?php

use Illuminate\Support\Facades\Route;

Route::get('/account', 'Main\AccountController@index')->name('main.account.index');


Route::post('/account/update/info', 'Main\AccountController@updateInfo')->name('main.account.update.info');

Route::post('/account/avatar-upload', 'Main\AccountController@uploadAvatar')->name('main.account.upload.avatar');


Route::group(
    [
        'middleware' => ['isReadyGame'],
    ],
    function () {
		
		Route::get('/account/business-card', 'Main\AccountController@businessCard')->name('main.account.business-card');

        Route::get('/account/messages', 'Main\AccountController@messages')->name('main.account.messages');

        Route::get('/account/histories', 'Main\AccountController@histories')->name('main.account.histories');

		Route::get('/account/security', 'Main\AccountController@security')->name('main.account.security');
		
		Route::post('/download/business-card', 'Main\AccountController@downloadBusinessCard')->name('pdf.business-card');

		Route::post('/account/update/security', 'Main\AccountController@updateSecurity')->name('main.account.update.security');
		
		Route::post('/account/update/status', 'Main\AccountController@updateStatus')->name('main.account.update.status');
		
        Route::post('/account/create/referral', 'Main\AccountController@createReferral')->name('main.account.create.referral');

        Route::post('/account/messages/delete/{id}', 'Main\AccountController@messageDelete')->name('main.account.message-delete');

        Route::post('/account/messages/read/{id}', 'Main\AccountController@messageRead')->name('main.account.message-read');

        Route::post('/account/create/business-card', 'Main\AccountController@createQrCode')->name('main.account.create.business-card');

        Route::post('/account/printout-place/add', 'Main\AccountController@addPrintoutPlace')->name('main.account.add-printout-place');


    }
);

