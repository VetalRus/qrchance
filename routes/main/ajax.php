<?php

use Illuminate\Support\Facades\Route;

Route::post('/ajax/widget/faq','Main\AjaxController@faq')->name('ajax.widget.faq');

Route::post('/ajax/widget/history','Main\AjaxController@history')->name('ajax.widget.history');

Route::post('/ajax/widget/account/inputPhone','Main\AjaxController@inputPhone')->name('ajax.widget.account.inputPhone');

Route::post('/account/cities/get', 'Main\AjaxController@getCityList')->name('main.account.cities.get');

Route::post('/ajax/history/{id}/delete-photo-history','Main\AjaxController@deletePhotoHistory')->name('ajax.main.history.deletePhotoHistory');
