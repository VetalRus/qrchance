<?php

use Illuminate\Support\Facades\Route;

Route::get('/history/show/{id}', 'Main\HistoryController@show')->name('main.history.show');

Route::get('/history/{id}/edit', 'Main\HistoryController@edit')->name('main.history.edit');

Route::post('/history/{id}/update', 'Main\HistoryController@update')->name('main.history.update');

Route::post('/history/{id}/create-likes', 'Main\HistoryController@createLikes')->name('main.history.create-likes');

Route::group(['middleware' => ['auth', 'isReadyGame', 'isRelationship']], function(){

	Route::get('/history/create', 'Main\HistoryController@create')->name('main.history.create');

	Route::post('/history/store', 'Main\HistoryController@store')->name('main.history.store');
});

