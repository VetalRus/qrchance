<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'Main\HomeController@index')->name('main.home.index');

Route::get('/aboutproject','Main\HomeController@aboutProject')->name('main.home.about_project');