<?php

use Illuminate\Support\Facades\Route;

Route::post('/qrcode/{uid}/send-message', 'Main\QRCodeController@sendMessage')->name('main.qrcode.send-message');

Route::get('/qrcode/{uid}','Main\QRCodeController@show')->name('main.qrcode.show');
