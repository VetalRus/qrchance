<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(
    [
        'prefix' => LocalizationService::locale(),
        'middleware' => 'setLocale',
    ],
    function () {
        require_once "main/home.php";

        require_once "main/account.php";

        require_once "main/history.php";

        require_once "main/qr_code.php";

        require_once "main/ajax.php";

		require_once "admin/auth.php";
		
    }
);

Route::post('/mails/feedback','Main\MailController@send_form')->name('mails.feedback');

Route::group(
    [
        'middleware' => ['admin.auth', 'isAdmin'],
        'prefix' => 'admin',
    ],
    function () {
        require_once "admin/dashboard.php";

		require_once "admin/faq_lang.php";
		
		require_once "admin/history.php";
		
		require_once "admin/country_lang.php";

        require_once "admin/city_lang.php";

		require_once "admin/alcohol.php";

		require_once "admin/smoking.php";
		
		require_once "admin/user.php";
		
		require_once "admin/printout_place.php";
		
		require_once "admin/social_network.php";
		
        require_once "admin/user_status_lang.php";

    }
);

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

Auth::routes();
