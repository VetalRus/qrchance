<?php

use Illuminate\Support\Facades\Route;

$methods = ['edit', 'update',  'store', 'destroy' ];

Route::resource('/city-lang', 'Admin\CityLangController')
->only($methods)
->names('admin.city_lang');

Route::get('/city-lang/{country_id}', 'Admin\CityLangController@index')->name('admin.city_lang.index');

Route::get('/city-lang/create/{country_id}', 'Admin\CityLangController@create')->name('admin.city_lang.create');

Route::post('/city-lang/search', 'Admin\CityLangController@search')->name('admin.city_lang.search');
