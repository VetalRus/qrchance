<?php

use Illuminate\Support\Facades\Route;

Route::resource('/user', 'Admin\UserController')->names('admin.user');

Route::post('/user/search', 'Admin\UserController@search')->name('admin.user.search');
