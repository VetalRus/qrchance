<?php

use Illuminate\Support\Facades\Route;

Route::resource('/social-network', 'Admin\SocialNetworkController')->names('admin.social_network');

Route::post('/social-network/search', 'Admin\SocialNetworkController@search')->name('admin.social_network.search');
