<?php

use Illuminate\Support\Facades\Route;

Route::get('admin/login','Admin\LoginController@index')->name('admin.login');

Route::post('admin/login','Admin\LoginController@authenticate')->name('admin.login');

Route::get('admin/logout','Admin\LoginController@logout')->name('admin.logout');


