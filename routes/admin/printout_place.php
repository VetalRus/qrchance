<?php

use Illuminate\Support\Facades\Route;

Route::resource('/printout-place', 'Admin\PrintoutPlaceController')->names('admin.printout_place');

Route::post('/printout-place/search', 'Admin\PrintoutPlaceController@search')->name('admin.printout_place.search');
