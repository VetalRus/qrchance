<?php

use Illuminate\Support\Facades\Route;

Route::resource('/history', 'Admin\HistoryController')->names('admin.history');

Route::post('/history/search', 'Admin\HistoryController@search')->name('admin.history.search');
