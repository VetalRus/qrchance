<?php

use Illuminate\Support\Facades\Route;

Route::resource('/faq-lang', 'Admin\FaqLangController')->names('admin.faq_lang');

Route::post('/faq-lang/search', 'Admin\FaqLangController@search')->name('admin.faq_lang.search');
