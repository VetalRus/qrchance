<?php

use Illuminate\Support\Facades\Route;

Route::resource('/country-lang', 'Admin\CountryLangController')->names('admin.country_lang');

Route::post('/country-lang/search', 'Admin\CountryLangController@search')->name('admin.country_lang.search');
