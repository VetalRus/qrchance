<?php

use Illuminate\Support\Facades\Route;

Route::resource('/smoking', 'Admin\SmokingController')->names('admin.smoking');

Route::post('/smoking/search', 'Admin\SmokingController@search')->name('admin.smoking.search');
