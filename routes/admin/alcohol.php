<?php

use Illuminate\Support\Facades\Route;

Route::resource('/alcohol', 'Admin\AlcoholController')->names('admin.alcohol');

Route::post('/alcohol/search', 'Admin\AlcoholController@search')->name('admin.alcohol.search');
