<?php

use Illuminate\Support\Facades\Route;

Route::resource('/user-status-lang', 'Admin\UserStatusLangController')->names('admin.user_status_lang');

Route::post('/user-status-lang/search', 'Admin\UserStatusLangController@search')->name('admin.user_status_lang.search');
