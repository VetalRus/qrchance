/**
 * Account
 */

$('.deleteMessage').submit(function (e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            form.closest('.row').remove();
            if(data.count == 0) {
                location.reload(true);
            }
        },
        error: function (data) {
            console.dir(data.responseJSON);
        }
    });
});


$('.message-unread').click(function() {
    $(this).submit();



});


$('.message-unread').submit(function (e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            if(data.status == 200) {
                if (data.count != '0') {
                    $('#count-1').html(data.count);
                } else {
                    $('#count-1').addClass('d-none');
                }
                $('#count-2').html(data.count);
                form.removeClass('bg-orange-no-gradient message-unread');
                form.find('.basket-message').removeClass('bg-orange-no-gradient').addClass('bg-white');
                form.find('.fa-trash-o').removeClass('text-white');
                form.find('.fa-check').removeClass('d-none');

            }
        },

        error: function (data) {
            console.dir(data.responseJSON);
        }
    });
});

