/**
 * Отправка ajax формы регистрации
 * */
$("#formRegister").submit(function(e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr("action");

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data) {
            console.dir(data);
            if (data.status == "200") {
				$("#modalRegister .modal-body").addClass("d-none");
                $("#modalRegisterThirdStep").removeClass("d-none");
                // location.href = data.url;
            }
        },
        error: function(data) {
            // Массив для ошибок
            let fields = [];
            for (key in data.responseJSON.errors) {
                fields.push(key);
            }

            // вызываем функцию вывода ошибок
            validateRegisterForm(fields);

            $("#modalRegister .modal-body").addClass("d-none");
            $("#modalRegisterFirstStep").removeClass("d-none");
        }
    });
});
// Переход на первый шаг регистрации
$("#modalRegister").on("hide.bs.modal", function() {
    $("#modalRegister .modal-body").addClass("d-none");
    $("#modalRegisterFirstStep").removeClass("d-none");
});

$("#modalRegister").on("shown.bs.modal", function() {
    $("body").addClass("modal-open");
});

// Переход на второй шаг регистрации
$("#registerNextStep2").click(function() {
    if (validateRegisterForm()) {
        $("#modalRegisterFirstStep").addClass("d-none");
        $("#modalRegisterSecondStep").removeClass("d-none");
    }
});
// показ модального окна когда ссуществует get referral
$(document).ready(function() {
	if( $("input[name='referral_token']").val() ){
		$("#modalRegister").modal('show');
	}
	
});

/*
 * @todo доработать валидацию
 * */
function validateRegisterForm(fields = []) {
    let errors = 0;

    $("#formRegister .alert").addClass("d-none");

    if (
        $('#formRegister input[name="name"]').val() == "" ||
        fields.indexOf("name") != -1
    ) {
        $('#formRegister input[name="name"]')
            .parent(".input-group")
            .siblings(".alert")
            .removeClass("d-none");

        $('#formRegister input[name="name"]').focus();
        errors++;
    }

    // Прописать регулярное выражение на проверку email
    if (!validateEmail()) {
        $('#formRegister input[name="email"]')
            .parent(".input-group")
            .siblings(".alert:not(.duplicate-email)")
            .removeClass("d-none");

        $('#formRegister input[name="email"]').focus();
        errors++;
    }

    if (fields.indexOf("email") != -1) {
        $('#formRegister input[name="email"]')
            .parent(".input-group")
            .siblings(".duplicate-email")
            .removeClass("d-none");
        $('#formRegister input[name="email"]').focus();
        errors++;
    }

    if (
        $('#formRegister input[name="password"]').val() == "" ||
        $('#formRegister input[name="password_confirmation"]').val() == "" ||
        fields.indexOf("password") != -1
    ) {
        $('#formRegister input[name="password"]')
            .parent(".input-group")
            .siblings(".alert")
            .removeClass("d-none");
        $('#formRegister input[name="password"]').focus();
        errors++;
    }

    if (
        $('#formRegister input[name="password"]').val() !=
        $('#formRegister input[name="password_confirmation"]').val()
    ) {
        $('#formRegister input[name="password_confirmation"]')
            .parent(".input-group")
            .siblings(".alert")
            .removeClass("d-none");
        $('#formRegister input[name="password"]').focus();
        errors++;
	}
	
	if (
        $('#formRegister textarea[name="g-recaptcha-response"]').val() == "" ||
        fields.indexOf("g-recaptcha-response") != -1
    ) {
        $('#formRegister textarea[name="g-recaptcha-response"]')
            .parents(".input-group")
            .siblings(".alert")
			.removeClass("d-none");
		errors++;
	}
	
	if (
        !$('#formRegister input[name="18_years"]').is(':checked') ||
        fields.indexOf("18_years") != -1
    ) {
        $('#formRegister input[name="18_years"]')
            .parent(".input-group")
            .siblings(".alert")
            .removeClass("d-none");

        $('#formRegister input[name="18_years"]').focus();
        errors++;
    }

    if (errors > 0) {
        return false;
    }

    return true;
}

function validateEmail() {
	var reg = /^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u;
	var address = $('#formRegister input[name="email"]').val();
	if(reg.test(address) == false) {
		return false;
	}
	return true;
	
}

function validatePassword() {}
