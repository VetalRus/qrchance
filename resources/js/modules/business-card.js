/**
 * Отправка ajax формы создания qr code
 * */
$("#formCreateQrCode").submit(function(e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr("action");

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data) {
            if (data.status == "success") {
                // location.href = data.url;
                $('textarea[name="description"]').val("");
                $("#modalSuccess .modal-message").html(data.message_key);
                $("#businessCardDownloadPdf").html(data.html);
                $("#modalSuccess").modal("show");

                $(".form_gray_s .form-control").focus(function(e) {
                    $(this)
                        .siblings(".fa")
                        .removeClass("fa-angle-right")
                        .addClass("fa-angle-down");
                });

                $(".form_gray_s .form-control").blur(function(e) {
                    $(this)
                        .siblings(".fa")
                        .removeClass("fa-angle-down")
                        .addClass("fa-angle-right");
                });
                var selector = $("input[name='phone']");
                var key_placeholder = $("input[name='phone']").attr(
                    "placeholder"
                );

                if (key_placeholder) {
                    var im = new Inputmask(key_placeholder.replace(/_/g, "9"));
                    im.mask($(selector));
                }
            }
        },
        error: function(data) {
            console.dir(data.responseJSON);
            // Массив для ошибок
            let fields = [];
            for (key in data.responseJSON.errors) {
                fields.push(key);
            }
            // вызываем функцию вывода ошибок
            validateCreateQrCodeForm(fields);
        }
    });
});

function validateCreateQrCodeForm(fields = []) {
    let errors = 0;

    $("#formCreateQrCode .alert").addClass("d-none");

    if (
        $('#formCreateQrCode input[name="description"]').val() == "" ||
        fields.indexOf("description") != -1
    ) {
        $("#formCreateQrCode .alert").removeClass("d-none");

        $('#formCreateQrCode input[name="description"]').focus();
        errors++;
    }

    if (errors > 0) {
        return false;
    }

    return true;
}
