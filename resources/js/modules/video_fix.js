// Select the block with the modal window #modalVideoPresentation and hang on it hide.bs.modal
$("#modalVideoPresentation").on("hide.bs.modal", function( e ){
  // get attribute src from iframe.video
  const link = $('.video').attr("src");
  // and then rewrite it
  $('.video').attr("src", link);
});