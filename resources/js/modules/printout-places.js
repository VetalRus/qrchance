/**
* Отправка ajax формы создания qr code
 * @todo сделать валидацию на фронте и сервере
* */
$('#formAddPrintoutPlace').submit(function (e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            if(data.status == 200) {
                location.reload();
            }
        }
    });
});

