/**
* Отправка ajax формы регистрации
* */
$('#faqAjaxLoad').submit(function (e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            if(data.status == 200) {
                $('#faqAccordion').append(data.html);
                form.find('input[name="offset"]').val(data.offset);
                if(data.end == 'yes') {
                    form.remove();
                }
            }
        }
    });
});

$(document).on('show.bs.collapse', ".collapse", function(){
    $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
}).on('hide.bs.collapse', ".collapse", function(){
    $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
});
