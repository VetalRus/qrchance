

function selectDropdown(){
	$('.form_gray_s .form-control').focus( function(e){
		$( this ).siblings('.fa').removeClass('fa-angle-right').addClass('fa-angle-down');
	});
	
	$('.form_gray_s .form-control').blur( function(e){
		$( this ).siblings('.fa').removeClass('fa-angle-down').addClass('fa-angle-right');
	});
	
	clickInputID();

	$(document).ready(function(){
		var selector = $("input[name='phone']");
		var key_placeholder = $("input[name='phone']").attr('placeholder');
	
		if(key_placeholder){
			var im = new Inputmask(key_placeholder.replace(/_/g,"9"));
			im.mask($(selector));
		}
	});
}

$('.gray-select .input_id').each(function( index ) {
	var input_id = $(this).val();
	if(input_id) {
		var title = $(this).parent('.gray-select').find('.list-country input[value="'+ input_id +'"]').siblings('span').html();
		$(this).siblings('input[type="button"]').val(title);
	}
});

function clickInputID() {
	
	$('.form_gray_s .list-country').on('click', function(){
		var inputyId = $(this).find('input[type="hidden"]').val();
		var title = $(this).find('span').html();
		var input = $(this).parent('.dropdown-menu').siblings('input[type="hidden"]');

		$(input).siblings('.form-control').val(title);
		$(input).val(inputyId);
		
		if( $(input).attr('name') == 'country_id' ){
			var divSelectCity = $('#selectCity').parents('.form_gray_s');
			$(divSelectCity).html('');
			$('#selectCityGif').removeClass('d-none')
				
			let infoCitiesForm = $("#personalInfoCitiesForm");
			let infoCitiesUrl = infoCitiesForm.attr("action");
			$(infoCitiesForm).find(' input[name="current_country_id"] ').val(inputyId);
			$.ajax({
				type: "POST",
				url: infoCitiesUrl,
				data: infoCitiesForm.serialize(),
				success: function (data) {
					if(data.status == '200') {
						$(divSelectCity).html(data.html);
						$('#selectCityGif').addClass('d-none')

						selectDropdown();

					}
				},
				error: function (data) {
					console.dir(data.responseJSON);
					// Массив для ошибок
					let fields = [];
					for (key in data.responseJSON.errors) {
						fields.push(key);
					}
				}
			});

			var inputPhoneForm = $("#personalInfoInputPhoneForm");
			var inputPhoneUrl = inputPhoneForm.attr("action");
			$(inputPhoneForm).find(' input[name="current_country_id"] ').val(inputyId);
			
			$.ajax({
				type: "POST",
				url: inputPhoneUrl,
				data: inputPhoneForm.serialize(),
				success: function (data) {
					if(data.status == '200') {
						var input = $('.form-group input[name="phone"]');
						var parent = $(input).parent();
						$(input).remove();
						$(parent).append(data.html)
						selectDropdown();

					}
				},
				error: function (data) {
					console.dir(data.responseJSON);
					// Массив для ошибок
					let fields = [];
					for (key in data.responseJSON.errors) {
						fields.push(key);
					}
				}
			});
		}
	});
}



selectDropdown();