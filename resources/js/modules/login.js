/**
* Отправка ajax формы регистрации
* */
$('#formLogin').submit(function (e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            console.dir(data)
            if(data.status == 'success') {
                location.href = data.url;
            }
            if(data.status == 'error') {
				$('#formLogin .alert').addClass('d-none');
				$('#formLogin .error-validate').removeClass('d-none');
			}
        },
        error: function (data) {
            console.dir(data.responseJSON);
            // Массив для ошибок
            let fields = [];
            for (key in data.responseJSON.errors) {
                fields.push(key);
            }
            // вызываем функцию вывода ошибок
            validateLoginForm(fields);
        }
    });
});


/*
* @todo доработать валидацию
* */
function validateLoginForm(fields = []) {
    let errors = 0;

    $('#formLogin .alert').addClass('d-none');

    if ($('#formLogin input[name="email"]').val() == "" || fields.indexOf('email') != -1) {
        $('#formLogin .alert:not(.error-validate)').removeClass('d-none');

        $('#formLogin input[name="email"]').focus();
        errors++;
    }

    if (errors > 0) {
        return false;
    }

    return true;
}

$('.form_gray .changeTypeInput').click(function() {

	var input = $(this).siblings('input');
	if($(this).hasClass('fa-eye-slash')) {
		$(input).attr('type', 'text');
		$(this)
			.removeClass('fa-eye-slash')
			.addClass('fa-eye');
	} else {
		$(input).attr('type', 'password');
		$(this)
			.removeClass('fa-eye')
			.addClass('fa-eye-slash');
	}
});