import Swiper from "swiper";

$("#historyImages").change(function() {
    $("#cardUploadPhoto").addClass("d-none");
    var parent = $(this).parent();

    if (this.files && this.files["0"]) {
        $(this.files).each(function(index) {
            var reader = new FileReader();

            reader.onload = function(e) {
                parent.append(
                    '<img src="' + e.target.result + '" class="img-fluid mb-2">'
                );
            };

            reader.readAsDataURL(this);
        });
    }
});

$("#historyAjaxLoad").submit(function(e) {
    e.preventDefault();

    let form = $(this);
    let url = form.attr("action");

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data) {
            if (data.status == 200) {
                $("#historyRow").append(data.html);
                form.find('input[name="offset"]').val(data.offset);
                if (data.end == "yes") {
                    form.remove();
                }
            }
        }
    });
});

function historyDeleteButton() {
    $("#historyCloseButton").click(function() {
        $("#historyImages").click();

        $("#historyImages").change(function() {
            // перенесення поля для удаления в формочку редактирования
            $('input[name="images[]"]')
                .parent()
                .append($('input[name="delete_image_id"]'));
            $("#historyEditSlider").remove();
        });
    });
}

function multiCarousel() {
    let swiper = new Swiper(".swiper-container", {
        slidesPerView: 3,
        spaceBetween: 30,
        autoHeight: true,
        loop: true,
        centeredSlides: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        }
    });

    $(".deletePhotoHistoryAjax").submit(function(e) {
        e.preventDefault();

        let form = $(this);
        let url = form.attr("action");

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                if (data.status == 200) {
                    var parent = $("#historyEditSlider").parent();
                    $("#historyEditSlider").remove();
                    $(parent).append(data.html);
                    multiCarousel();
                    historyDeleteButton();
                }
            }
        });
    });
}

historyDeleteButton();
multiCarousel();

// слушаем клик по кнопке выбора языка на страницах edit и create в админке
$( ' .lang-click' ).click(function(e) {
    e.preventDefault; // Prevent the default behavior of the  element.
    let langId = $(this).attr('data-id');
    let langTitle = $(this).attr('data-title');
    $('#dropdownLanguage').text(langTitle);
    $('#inputValue').attr('value', langId);
});

// временно для загрузки одной фотографии при редактирования 
$("#historyImages").change(function() {
	// перенесення поля для удаления в формочку редактирования
	$('input[name="images[]"]')
		.parent()
		.append($('input[name="delete_image_id"]'));
	$("#historyEditSlider").remove();
});