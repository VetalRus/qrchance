/**
 * Загрузка аватарки через ajax
 * */
$(".uploadAvatarButton").click(function() {
    $("#uploadAvatar").click();
});

$("#uploadAvatar").on("change", function() {
    $("#downloadGif").removeClass("d-none");

    let form = $("#formUploadAvatar");
    let url = form.attr("action");
	var reader = new FileReader();

    var file_data = $(this).prop("files")[0];
    var form_data = new FormData();
    form_data.append("avatar", file_data);
    form_data.append("_token", $('input[name="_token"]').val());

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        type: "POST",
        url: url,
        data: form_data,
        success: function(data) {
            if (data.status == 200) {
				reader.onload = function(e) {
					$("#avatarImg")
					.attr("src", e.target.result); 
					$(".avatar img").attr("src",  e.target.result);
					$("#downloadGif").addClass("d-none");
					
				};
				reader.readAsDataURL(file_data);
                $("#uploadDefaltAvatarButton").addClass("d-none");
                $("#uploadAvatarButton").removeClass("d-none");

                $("#defaultAvatar")
                    .removeClass("d-block")
					.addClass("d-none");

				$("#avatarImg")
                    .removeClass("d-none")
					.addClass("d-block");

            }
        },

        error: function(data) {
            console.dir(data.responseJSON);
            // Массив для ошибок
            let fields = [];
            for (key in data.responseJSON.errors) {
                fields.push(key);
            }
        }
    });
});

$(".navsMenuBtn").click(function() {
    $(".navsMenuCard").toggleClass("d-none");
});

$(".change-status .change").click(function(e) {
    e.preventDefault();
	var userStatusId = $(this).data('userStatusId');
	$('#modalWarningChanceStatus').modal('show');
	$('#modalWarningChanceStatus').find('input[name="status_id"]').val(userStatusId);
});