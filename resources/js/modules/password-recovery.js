/**
 * Отправка ajax формы востановления пароля
 * */
$('#formPasswordRecovery').submit(function (e) {

    e.preventDefault();
    console.log('Entering ajax');
    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            if(data.status == 'success') {
                $('#modalPasswordRecoveryFirstStep').addClass('d-none');
                $('#modalPasswordRecoverySecondStep').removeClass('d-none');
            }
        },
        error: function (data) {
            console.dir(data.responseJSON);
            // Массив для ошибок
            let fields = [];
            for (key in data.responseJSON.errors) {
                fields.push(key);
            }
            // вызываем функцию вывода ошибок
            validatePasswordRecoveryForm(fields);
        }
    });
});

// востановление классов (измененных в ajax-запросе) при клике на кнопку "Перейти"
$('#loginNextStep2').click(function() {
    $('#modalPasswordRecoverySecondStep').addClass('d-none');
    $('#modalPasswordRecoveryFirstStep').removeClass('d-none');
})

/*
* @todo доработать валидацию
* */

function validatePasswordRecoveryForm(fields = []) {
    let errors = 0;

    $('#formPasswordRecovery .alert').addClass('d-none');

    if ($('#formPasswordRecovery input[name="email"]').val() == "" || fields.indexOf('email') != -1) {
        $('#formPasswordRecovery .alert').removeClass('d-none');

        $('#formPasswordRecovery input[name="email"]').focus();
        errors++;
    }

    if (errors > 0) {
        return false;
    }

    return true;
}
