<?php

return array (
  'account' => 
  array (
    'add_printout_place' => 'Do you know where you can print business cards? Indicate and other participants can use',
    'histories' => 
    array (
      'create' => 'Write history',
      'your_story' => 'Your story',
      'text_history' => 'Write your dating story, first date and win valuable prizes',
    ),
    'list' => 
    array (
      'answered_yes' => 'List of those who answered Yes',
      'btn_show_message' => 'Show more posts',
      'delete_message' => 'Delete all messages',
    ),
    'notification' => 
    array (
      'payment' => 
      array (
        'attention' => 'Attention!',
        'btn_return' => 'Back to personal page',
        'confirm' => 'Оплата прошла успешно',
        'congratulation' => 'Congratulations! Your account is activated',
        'error' => 'Payment failed!',
        'go_account' => 'Go to your personal account',
        'info' => 'You may not have enough money on the card or there may be a limit on payments over the Internet',
        'recommendation' => 'Contact your bank operator to determine the reason',
      ),
      'status' => 
      array (
        'attention' => 'Attention!',
        'btn_confirm' => 'To confirm',
        'info' => 'After changing your status to Invisible or In a Relationship, you can no longer receive notifications from those who have received the business card.',
      ),
    ),
    'payment' => 
    array (
      'currency' => 'GBR',
      'form_payment' => 'Choose a convenient payment method',
      'sum' => 'Enter the amount',
      'title' => 'refill',
    ),
    'personal-info' => 
    array (
      'alcohol_attitude' => 'Attitude to alcohol',
      'alcohol_compromise' => 'Compromise',
      'alcohol_negative' => 'negative',
      'alcohol_neutral' => 'Neutral',
      'alcohol_positive' => 'positive',
      'alcohol_very_negative' => 'Very negative',
      'birthday' => 'Date of birth',
      'btn_save' => 'Save up',
      'btn_update_photo' => 'Update photo',
      'btn_upload_photo' => 'Upload a photo',
      'children' => 'Сhildren',
      'country' => 'Сountry',
      'have_children' => 'Do you have children?',
      'in_search' => 'In search',
      'invisibility' => 'Invisible',
      'profession' => 'Job',
      'smoking_attitude' => 'Attitude to smoking',
      'smoking_compromise' => 'Compromise',
      'smoking_negative' => 'Negative',
      'smoking_neutral' => 'Neutral',
      'smoking_positive' => 'Positive',
      'smoking_very_negative' => 'Very negative',
      'status' => 'Status',
      'title_about' => 'About me',
      'title_contact_info' => 'Contact information',
      'title_personal_info' => 'Personal information',
      'town' => 'The town',
      'your_last_name' => 'Your family name',
      'your_last_placeholder' => 'User name',
      'your_name' => 'What\'s your name',
      'your_name_placeholder' => 'User name',
      'facebook' => 'Facebook',
      'twitter' => 'Twitter',
      'vk' => 'VK',
      'youtube' => 'Youtube',
      'instagram' => 'Instagram',
      'email' => 'E-mail:',
      'have_children_not' => 'Not',
      'have_children_yes' => 'Yes',
      'phone' => 'Phone number:',
    ),
    'printout_places' => 'You can print business cards at these addresses',
    'qr' => 
    array (
      'adress_not_exist' => 'There is no such address',
      'anothe' => 'Other',
      'btn_complain' => 'Complain',
      'btn_create' => 'Generate code',
      'btn_download' => 'Download the business card layout',
      'change' => 'Download the business card layout',
      'email_placeholder' => 'e-mail',
      'find' => 'Search by city',
      'info' => 'You can download a business card and send it for printing yourself.',
      'phone' => 'Phone number:',
      'print_info' => 'You can print business cards at the following addresses:',
      'save' => 'Save up',
      'slogan' => 'It is easy to meet',
      'slogan1' => 'From',
      'text' => 'Enter text for the encoding',
      'text_example_placeholder' => 'For example:',
      'text_placeholder' => 'Hello, I like you very much. Shall we have dinner together?',
      'title' => 'QR code generator',
      'title_info1' => 'Do you know where to print business cards?',
      'title_info2' => 'Ads and other participants can use',
      'town' => 'Street house:',
      'using' => 'Use of:',
      'using_1' => '1. Take a cell phone with a camera.',
      'using_2' => '2. Run the program to scan the code',
      'using_3' => '3. Point the camera lens at the code',
      'using_4' => 'Get information!',
      'phone_placeholder' => '+44(__)___-__-__',
      'error' => 
      array (
        'address' => 'Error entering address',
        'city' => 'City selection error',
        'email' => 'Error, incorrect E-mail',
        'phone' => 'Error, incorrect phone number',
      ),
      'street/town' => 'Street house:',
    ),
    'security' => 
    array (
      'btn_off' => 'Out',
      'btn_on' => 'On',
      'delete_account' => 'Delete your account',
      'email' => 'e-mail',
      'new_password' => 'New Password:',
      'notification' => 'Notifications',
      'old_password' => 'Old password:',
      'password_placeholder' => 'Password (10 characters or more)',
      'receive_info_fail' => 'I want to receive notifications',
      'receive_message' => 'I want to receive important news from the company',
      'repeat_password' => 'Repeat the password',
      'save' => 'Save up',
      'title' => 'Safety!',
    ),
    'sidebar' => 
    array (
      'activation_info' => 'Your account will remain active until you change your status to "In a relationship" or "Account deleted".',
      'btn_psq' => 'Pay',
      'creating_visitcard' => 'Create a business card',
      'message' => 'Message',
      'other_stories' => 'More stories',
      'payment' => 'Payment',
      'personal_info' => 'Personal data',
      'settings' => 'The settings',
      'activation' => 'To activate your account, you need to pay a one-time payment of UAH 100.',
    ),
  ),
  'about' => 
  array (
    'project' => 
    array (
      'title' => 'Аbout the project',
      'about' => 'QRChance.com is a portal for quick and easy dating',
    ),
  ),
  'auth' => 
  array (
    'forgot' => 
    array (
      'agreement' => 'By registering you agree to our terms of use',
      'email' => 'Enter mail',
      'email_placeholder' => 'e-mail',
      'go' => 'e-mail',
      'login_with' => 'Login with:',
      'password' => 'Forgot Password?',
      'recovery' => 'Password recovery',
      'send' => 'The message was sent to your email',
    ),
    'login' => 
    array (
      'account' => 'I don\'t have an account.',
      'btn_submit' => 'To enter',
      'forgot_password' => 'Forgot Password?',
      'error_login_or_password' => 'Login or password entered incorrectly',
      'name' => 'Enter your name',
      'name_placeholder' => 'User name',
      'password' => 'Enter password',
      'password_placeholder' => 'Password (10 characters or more)',
      'register' => 'Join Now',
      'remember' => 'To remember me',
      'social' => 'Login with:',
      'title' => 'entrance',
      'welcome' => 'Welcome to the portal',
    ),
    'register' => 
    array (
      'btn_next' => 'Further',
      'btn_submit' => 'Check in',
      'email' => 'Enter mail',
      'email_placeholder' => 'e-mail',
      'enter' => 'entrance',
      'error_email' => 'Wrong email entered',
      'error_password' => 'Enter password',
      'goals' => 'Together with us you can easily find your love as well as new friends.',
      'name' => 'Enter your name',
      'name_placeholder' => 'User name',
      'password' => 'Enter password',
      'error_password_confirmation' => 'Confirm the password',
      'error_name' => 'Enter your name',
      'have_account' => 'Do you already have an account?',
      'password_confirmation' => 'Confirm the password',
      'password_confirmation_placeholder' => 'Password (10 characters or more)',
      'password_placeholder' => 'Password (10 characters or more)',
      'sex_man' => 'I am a boy',
      'sex_woman' => 'I\'m a girl',
      'title' => 'Check in',
      'welcome' => 'Welcome to the portal',
    ),
  ),
  'feedback' => 
  array (
    'email' => 'e-mail',
    'email_placeholder' => 'e-mail',
    'error_email' => 'Enter your e-mail address',
    'name_placeholder' => 'User name',
    'send' => 'Send Message',
    'phone' => 'Mobile phone',
    'title' => 'Feedback',
    'name' => 'Enter your name:',
    'error_phone' => 'Enter your mobile number',
    'error_name' => 'Enter your name',
    'phone_placeholder' => '+44(__)___-__-__',
  ),
  'language' => 'English',
  'main' => 
  array (
    'index' => 
    array (
      'acquaintance_content' => 'Sometimes there is no time or opportunity to be alone with someone to communicate personally and get to know each other better. You can use this website to continue communication at any time.',
      'acquaintance' => 'Easy acquaintance',
      'create_history' => 'Write history',
      'details' => 'More details',
      'how_it_works' => 'How it works',
      'impression' => 'First impression',
      'item1' => 'Sign up',
      'item2' => 'Create a unique business card',
      'item3' => 'Meet quickly and easily',
      'more' => 'Show more stories',
      'people' => 'Real people',
      'people_content' => 'Real people - no bots or fake accounts. You yourself choose with whom to communicate, but not with whom.',
      'play' => 'Start the game',
      'stories' => 'Dating stories of our members',
      'text_history' => 'Write your dating story, first date and win',
      'title' => 'Meet easily together',
      'title1' => 'with',
      'why_this_better' => 'Why is this way of dating better?',
      'your_story' => 'Why is this way of dating better?',
      'impression_content' => 'You had the opportunity to meet a person and create the first impression of him in person, and not from a photo.',
    ),
  ),
  'layouts' => 
  array (
    'main' => 
    array (
      'support' => 'Support service:',
      'partner' => 'Become a project partner',
      'more' => 'Show more questions',
      'made' => 'Made with love in Ukraine',
      'language' => 'Site language',
      'follow' => 'Follow us:',
      'feedback' => 'Feedback',
      'faq' => 'Frequently asked Questions',
      'about' => 'About the project',
    ),
  ),
  'laravel' => 'Laravel',
  'message' => 
  array (
    'btn_no' => 'Not',
    'btn_yes' => 'Not',
    'info' => 
    array (
      'smoking_attitude' => 'Attitude towards smoking:',
      'profession' => 'Profession:',
      'birthday' => 'Date of Birth',
      'alcohol_attitude' => 'Attitude to alcohol:',
    ),
    'notification' => 'This page is visible only to those who have a business card with a QR code',
    'pop_up' => 
    array (
      'btn_send' => 'Send message',
      'name' => 'Name',
      'name_placeholder' => 'Enter your name',
      'phone' => 'Phone number',
      'title_next' => 'Will dial you',
      'title' => 'Write your name, phone number and',
      'phone_placeholder' => '+44(__)___-__-__',
    ),
    'social' => 'My social networks',
    'title' => 'Sergey left a message for you!',
    'answer_not' => 'Not',
    'answer_yes' => 'Not',
    'status' => 
    array (
      'end_search' => 'The search is over!',
      'in_search_of' => 'As a joke!',
    ),
    'title_1' => 'For you',
    'title_2' => 'left a message',
  ),
  'stories' => 
  array (
    'btn_like' => 'Vote',
    'btn_send' => 'Send message',
    'btn_upload' => 'Upload a photo',
    'content_placeholder' => 'Write your story, the story of how you met thanks to QRChance.com',
    'photo' => 'You can upload 1 or more photos',
    'prize' => 'Write your date story and win 1 of the prizes listed below',
    'btn_edit' => 'Edit',
  ),
  'pagination' => 
  array (
    'next' => 'Following',
    'previous' => 'Previous',
  ),
  'passwords' => 
  array (
    'reset' => 'Your password has been reset!',
    'sent' => 'We\'ve sent you a link to reset your password by email!',
    'throttled' => 'Please wait before trying again.',
    'token' => 'This password reset ID is not valid',
    'user' => 'We cannot find a user with this email address.',
  ),
);
