<?php

return array (
  'about' => 
  array (
    'project' => 
    array (
      'about' => 'QRChance.com - এটি দ্রুত এবং সহজ ডেটিংয়ের জন্য একটি পোর্টাল',
      'title' => 'প্রকল্প সম্পর্কে',
    ),
  ),
  'account' => 
  array (
    'add_printout_place' => 'আপনি কি জানেন যেখানে আপনি ব্যবসায়ের কার্ডগুলি মুদ্রণ করতে পারবেন? অন্যান্য অংশগ্রহণকারীরা সুবিধা নিতে পারে তা দেখান',
    'histories' => 
    array (
      'create' => 'একটি গল্প লিখুন',
      'text_history' => 'আপনার ডেটিং গল্পটি লিখুন, প্রথম তারিখটি এবং মূল্যবান পুরষ্কার জিতে নিন',
      'your_story' => 'তোমার গল্প',
    ),
    'list' => 
    array (
      'answered_yes' => 'যারা হ্যাঁ উত্তর দিয়েছে তাদের তালিকা',
      'btn_show_message' => 'আরও পোস্ট দেখান',
      'delete_message' => 'সমস্ত বার্তা মুছুন',
    ),
    'notification' => 
    array (
      'payment' => 
      array (
        'attention' => 'মনোযোগ!',
        'btn_return' => 'ব্যক্তিগত পৃষ্ঠায় ফিরে যান',
        'confirm' => 'অর্থ প্রদান সফল হয়েছিল',
        'congratulation' => 'অভিনন্দন! আপনার অ্যাকাউন্ট সক্রিয় করা হয়েছে',
        'error' => 'পেমেন্ট ব্যর্থ হয়েছে!',
        'go_account' => 'ব্যক্তিগত অ্যাকাউন্টে যান',
        'info' => 'কার্ডে আপনার যথেষ্ট পরিমাণ অর্থ নাও থাকতে পারে বা আপনার অনলাইন পেমেন্টের সীমা রয়েছে',
        'recommendation' => 'কারণ নির্ধারণ করতে আপনার ব্যাংকের অপারেটরের সাথে যোগাযোগ করুন।',
      ),
      'status' => 
      array (
        'attention' => 'মনোযোগ!',
        'btn_confirm' => 'নিশ্চিত করুন',
        'info' => 'স্থিতিটি "অদৃশ্য" বা "একটি সম্পর্কের ক্ষেত্রে" তে পরিবর্তন করার পরে, আপনি আর যাদের ব্যবসার কার্ড হস্তান্তরিত হয়েছিল তাদের কাছ থেকে বার্তা পাওয়া যাবে না।',
      ),
    ),
    'payment' => 
    array (
      'currency' => 'INR',
      'form_payment' => 'অর্থ প্রদানের একটি সুবিধাজনক ফর্ম চয়ন করুন',
      'sum' => 'পরিমাণ লিখুন',
      'title' => 'অ্যাকাউন্ট পুনরায় পূরণ',
    ),
    'personal-info' => 
    array (
      'alcohol_attitude' => 'অ্যালকোহলের প্রতি মনোভাব',
      'alcohol_compromise' => 'আপস',
      'alcohol_negative' => 'নেতিবাচক',
      'alcohol_neutral' => 'নিরপেক্ষ',
      'alcohol_positive' => 'ধনাত্মক',
      'alcohol_very_negative' => 'তীব্র নেতিবাচক',
      'birthday' => 'জন্ম তারিখ',
      'btn_save' => 'সংরক্ষণ',
      'btn_update_photo' => 'আপডেট ফটো',
      'btn_upload_photo' => 'আপডেট ফটো',
      'children' => 'শিশু',
      'country' => 'দেশ',
      'facebook' => 'Facebook',
      'have_children' => 'আপনার কি সন্তান আছে?',
      'in_search' => 'অনুসন্ধান',
      'invisibility' => 'অদৃশ্য',
      'profession' => 'পেশা',
      'smoking_attitude' => 'ধূমপানের প্রতি মনোভাব',
      'smoking_compromise' => 'আপস',
      'smoking_negative' => 'নেতিবাচক',
      'smoking_neutral' => 'নিরপেক্ষ',
      'smoking_positive' => 'ধনাত্মক',
      'smoking_very_negative' => 'তীব্র নেতিবাচক',
      'status' => 'অবস্থা',
      'title_about' => 'আমার সম্পর্কে',
      'title_contact_info' => 'যোগাযোগের তথ্য',
      'town' => 'শহর',
      'title_personal_info' => 'ব্যক্তিগত তথ্য',
      'your_last_name' => 'আপনার নামের শেষাংশ',
      'your_last_placeholder' => 'ব্যবহারকারীর শেষ নাম',
      'your_name' => 'তোমার নাম',
      'your_name_placeholder' => 'ব্যবহারকারীর নাম',
      'email' => 'ই-মেইল:',
      'have_children_not' => 'না',
      'have_children_yes' => 'হ্যাঁ',
      'instagram' => 'Instagram',
      'phone' => 'ফোন নম্বর:',
      'twitter' => 'Twitter',
      'vk' => 'VK',
      'youtube' => 'Youtube',
    ),
    'printout_places' => 'আপনি এই ঠিকানাগুলিতে ব্যবসায়িক কার্ড মুদ্রণ করতে পারেন',
    'qr' => 
    array (
      'adress_not_exist' => 'এর মতো কোনও ঠিকানা নেই',
      'anothe' => 'অন্য',
      'btn_complain' => 'নালিশ করা',
      'btn_create' => 'কোড উত্পন্ন',
      'btn_download' => 'একটি ব্যবসায়িক কার্ড লেআউট ডাউনলোড করুন',
      'change' => 'পরিবর্তন',
      'email_placeholder' => 'ইমেল ঠিকানা',
      'find' => 'দ্বারা অনুসন্ধান',
      'info' => 'আপনি একটি ব্যবসায়িক কার্ড ডাউনলোড করতে পারেন এবং নিজের মুদ্রণের জন্য এটি প্রেরণ করতে পারেন।',
      'phone' => 'ফোন নম্বর:',
      'print_info' => 'আপনি এই ঠিকানাগুলিতে ব্যবসায় কার্ড মুদ্রণ করতে পারেন:',
      'save' => 'সংরক্ষণ',
      'slogan' => 'একসাথে সহজেই দেখা',
      'text' => 'এনকোড করতে পাঠ্য প্রবেশ করান',
      'text_example_placeholder' => 'উদাহরণ স্বরূপ:',
      'text_placeholder' => 'হাই, আমি আপনাকে সত্যিই পছন্দ করি এক সাথে রাতের খাবার খেয়েছি?',
      'title' => 'কিউআর কোড জেনারেটর',
      'title_info1' => 'Ведаеце дзе можна раздрукаваць візітоўкі?',
      'title_info2' => 'অন্যান্য অংশগ্রহণকারীরা সুবিধা নিতে পারে তা দেখান',
      'town' => 'রাস্তা / বাড়ি:',
      'using' => 'ব্যবহার:',
      'using_1' => '1. একটি ক্যামেরা সহ একটি সেল ফোন নিন।',
      'using_2' => '২. কোডটি স্ক্যান করতে প্রোগ্রামটি চালান',
      'using_3' => '৩. কোডে ক্যামেরার লেন্সটি লক্ষ্য করুন',
      'using_4' => 'তথ্য পান!',
      'error' => 
      array (
        'address' => 'ঠিকানা প্রবেশের সময় ত্রুটি',
        'city' => 'শহর নির্বাচনের ত্রুটি',
        'email' => 'ত্রুটি, ভুল ইমেল',
        'phone' => 'ত্রুটি, ভুল ফোন নম্বর',
      ),
      'phone_placeholder' => '+880(__)___-__-__',
      'slogan1' => 'থেকে',
      'street/town' => 'রাস্তার বাড়ি:',
    ),
    'security' => 
    array (
      'btn_off' => 'বন্ধ',
      'btn_on' => 'চালু',
      'delete_account' => 'হিসাব মুছে ফেলা',
      'email' => 'ইমেল ঠিকানা',
      'new_password' => 'নতুন পাসওয়ার্ড:',
      'notification' => 'বিজ্ঞপ্তিগুলি',
      'old_password' => 'পুরানো পাসওয়ার্ড:',
      'password_placeholder' => 'পাসওয়ার্ড (10 টি অক্ষর)',
      'receive_info_fail' => 'আমি বার্তা পেতে চাই',
      'receive_message' => 'আমি সংস্থা থেকে গুরুত্বপূর্ণ বার্তা পেতে চাই',
      'repeat_password' => 'পাসওয়ার্ড পুনরাবৃত্তি করুন',
      'save' => 'সংরক্ষণ',
      'title' => 'সিকিউরিটি!',
    ),
    'sidebar' => 
    array (
      'activation' => 'আপনার অ্যাকাউন্টটি সক্রিয় করতে আপনাকে 100 ইউএইচ-এর এককালীন ফি প্রদান করতে হবে।',
      'activation_info' => 'আপনার অ্যাকাউন্টটি সক্রিয় থাকবে যতক্ষণ না আপনি আপনার স্থিতিকে "সম্পর্কের সাথে" বা "অ্যাকাউন্ট মোছা" তে পরিবর্তন করেন',
      'btn_psq' => 'পে',
      'creating_visitcard' => 'একটি বিজনেস কার্ড তৈরি করা হচ্ছে',
      'message' => 'বার্তা',
      'other_stories' => 'অন্যান্য গল্প',
      'payment' => 'পারিশ্রমিক',
      'personal_info' => 'ব্যক্তিগত তথ্য',
      'settings' => 'সেটিংস',
    ),
  ),
  'auth' => 
  array (
    'forgot' => 
    array (
      'agreement' => 'নিবন্ধন করে আপনি আমাদের পরিষেবার শর্তাদিতে সম্মত হন',
      'email' => 'মেলটি প্রবেশ করান',
      'email_placeholder' => 'ইমেল ঠিকানা',
      'go' => 'যাওয়া',
      'login_with' => 'এর সাথে লগইন করুন:',
      'password' => 'আপনি কি পাসওয়ার্ড ভুলে গেছেন?',
      'recovery' => 'পাসওয়ার্ড পুনরুদ্ধার',
      'send' => 'আপনাকে একটি বার্তা মেল মাধ্যমে প্রেরণ করা হয়েছে',
    ),
    'login' => 
    array (
      'account' => 'আমার কোন অ্যাকাউন্ট নেই',
      'btn_submit' => 'প্রবেশ করুন',
      'error_login_or_password' => 'প্রবেশ করা নাম বা পাসওয়ার্ডটি ভুল',
      'forgot_password' => 'আপনি কি পাসওয়ার্ড ভুলে গেছেন?',
      'name' => 'একটি নাম লিখুন',
      'name_placeholder' => 'ব্যবহারকারীর নাম',
      'password' => 'পাসওয়ার্ড লিখুন',
      'password_placeholder' => 'পাসওয়ার্ড (10 টি অক্ষর)',
      'register' => 'নিবন্ধন',
      'remember' => 'আমাকে মনে কর',
      'social' => 'এর সাথে লগইন করুন:',
      'title' => 'প্রবেশদ্বার',
      'welcome' => 'পোর্টালে স্বাগতম',
    ),
    'register' => 
    array (
      'btn_next' => 'পরবর্তী',
      'btn_submit' => 'নিবন্ধন',
      'email' => 'মেলটি প্রবেশ করান',
      'email_placeholder' => 'ইমেল ঠিকানা',
      'enter' => 'প্রবেশদ্বার',
      'error_email' => 'প্রবেশ করা ইমেলটি ভুল',
      'error_name' => 'একটি নাম লিখুন',
      'error_password' => 'পাসওয়ার্ড লিখুন',
      'error_password_confirmation' => 'পাসওয়ার্ড নিশ্চিত করুন',
      'goals' => 'আমাদের সাথে আপনি সহজেই আপনার ভালবাসা, পাশাপাশি নতুন বন্ধুগুলি খুঁজে পেতে পারেন',
      'have_account' => 'আপনার কি ইতিমধ্যে একটি সদস্যপদ আছে?',
      'name' => 'একটি নাম লিখুন',
      'name_placeholder' => 'ব্যবহারকারীর নাম',
      'password' => 'পাসওয়ার্ড লিখুন',
      'password_confirmation' => 'পাসওয়ার্ড নিশ্চিত করুন',
      'password_confirmation_placeholder' => 'পাসওয়ার্ড (10 টি অক্ষর)',
      'password_placeholder' => 'পাসওয়ার্ড (10 টি অক্ষর)',
      'sex_man' => 'আমি একজন পুরুষ',
      'sex_woman' => 'অমি একটি ময়ে',
      'title' => 'নিবন্ধন',
      'welcome' => 'পোর্টালে স্বাগতম',
    ),
  ),
  'feedback' => 
  array (
    'email' => 'ই-ছোট',
    'email_placeholder' => 'ইমেল ঠিকানা',
    'error_email' => 'তোমার ই - মেইল ​​ঠিকানা লেখো',
    'error_name' => 'একটি নাম লিখুন',
    'error_phone' => 'আপনার মোবাইল ফোন প্রবেশ করুন',
    'name' => 'একটি নাম লিখুন',
    'name_placeholder' => 'ব্যবহারকারীর নাম',
    'phone' => 'মোবাইল ফোন',
    'send' => 'জমা দিন',
    'title' => 'প্রতিক্রিয়া',
    'phone_placeholder' => '+880(__)___-__-__',
  ),
  'language' => 'বাংলা',
  'layouts' => 
  array (
    'main' => 
    array (
      'about' => 'প্রকল্প সম্পর্কে',
      'faq' => 'সচরাচর জিজ্ঞাস্য',
      'feedback' => 'প্রতিক্রিয়া',
      'follow' => 'আমাদের অনুসরণ করো:',
      'language' => 'সাইটের ভাষা',
      'made' => 'প্রেম দিয়ে তৈরি ইউক্রেন',
      'more' => 'আরও প্রশ্ন দেখান',
      'partner' => 'একটি প্রকল্পের অংশীদার হন',
      'support' => 'সহায়তা সেবা:',
    ),
  ),
  'laravel' => 'লারাভেল',
  'main' => 
  array (
    'index' => 
    array (
      'acquaintance' => 'পরিচিতি সহজ',
      'acquaintance_content' => 'কখনও কখনও ব্যক্তিগতভাবে যোগাযোগ করার জন্য এবং একে অপরকে আরও ভালভাবে জানার জন্য কোনও ব্যক্তির সাথে একা থাকার সময় ও সুযোগ থাকে না, এই সাইটের সাহায্যে আপনি যে কোনও সুবিধাজনক সময়ে যোগাযোগ চালিয়ে যেতে পারেন।',
      'create_history' => 'ইতিহাস লিখুন',
      'details' => 'আরো বিস্তারিত',
      'how_it_works' => 'কিভাবে এটা কাজ করে',
      'impression' => 'প্রথম ছাপ',
      'impression_content' => 'আপনি কোনও ব্যক্তির সাথে দেখা করার এবং ব্যক্তিগতভাবে তাঁর প্রথম ধারণা তৈরি করার সুযোগ পেয়েছিলেন, কোনও ফটো থেকে নয়।',
      'item1' => 'নিবন্ধন করুন',
      'item2' => 'একটি অনন্য ব্যবসা কার্ড তৈরি করুন',
      'item3' => 'দ্রুত এবং সহজে দেখা',
      'more' => 'আরও গল্প দেখান',
      'people' => 'আসল মানুষ',
      'people_content' => 'আসল লোক - কোনও বট বা জাল অ্যাকাউন্ট নেই। আপনি নিজেই বেছে নিন কার সাথে যোগাযোগ করবেন এবং কার সাথে নয়।',
      'play' => 'খেলা শুরু কর',
      'stories' => 'আমাদের সদস্যদের ডেটিং গল্প',
      'text_history' => 'আপনার ডেটিং গল্পটি লিখুন, প্রথম তারিখটি এবং মূল্যবান পুরষ্কার জিতে নিন',
      'title' => 'সহজেই একসাথে মিলিত হয়',
      'title1' => 'থেকে',
      'why_this_better' => 'কেন ডেটিংয়ের এই পদ্ধতিটি আরও ভাল?',
      'your_story' => 'তোমার গল্প',
    ),
  ),
  'message' => 
  array (
    'answer_not' => 'না',
    'btn_no' => 'না',
    'answer_yes' => 'হ্যাঁ',
    'btn_yes' => 'হ্যাঁ',
    'info' => 
    array (
      'alcohol_attitude' => 'অ্যালকোহলের প্রতি মনোভাব:',
      'birthday' => 'জন্ম তারিখ',
      'profession' => 'পেশা:',
      'smoking_attitude' => 'পেশা:',
    ),
    'notification' => 'এই পৃষ্ঠাটি কেবল তাদের কাছে দৃশ্যমান যাঁদের একটি QR কোড সহ একটি ব্যবসায়িক কার্ড রয়েছে',
    'pop_up' => 
    array (
      'btn_send' => 'বার্তা পাঠান',
      'name' => 'Имя',
      'name_placeholder' => 'আপনার নাম প্রবেশ করুন',
      'phone' => 'ফোন নম্বর',
      'phone_placeholder' => '+880(__)___-__-__',
      'title' => 'আপনার নাম, ফোন নম্বর এবং লিখুন',
      'title_next' => 'আপনাকে ডায়াল করবে',
    ),
    'social' => 'আমার সামাজিক নেটওয়ার্কগুলি',
    'status' => 
    array (
      'end_search' => 'সন্ধান শেষ!',
      'in_search_of' => 'অনুসন্ধানে!',
    ),
    'title' => 'সার্জি আপনার জন্য একটি বার্তা রেখে গেছে!',
    'title_1' => 'তোমার জন্য',
    'title_2' => 'একটি বার্তা রেখে যাও',
  ),
  'pagination' => 
  array (
    'next' => 'অনুসরণ',
    'previous' => 'আগে',
  ),
  'passwords' => 
  array (
    'reset' => 'পাসওয়ার্ড পুনরায় সেট করা হয়েছে!',
    'sent' => 'আমরা আপনাকে ইমেল দ্বারা আপনার পাসওয়ার্ড পুনরায় সেট করতে একটি লিঙ্ক প্রেরণ করেছি!',
    'throttled' => 'আবার চেষ্টা করার আগে দয়া করে অপেক্ষা করুন।',
    'token' => 'এই পাসওয়ার্ড রিসেট আইডি অবৈধ',
    'user' => 'আমরা এই ইমেল ঠিকানা সহ কোনও ব্যবহারকারীর সন্ধান করতে পারি না।',
  ),
  'stories' => 
  array (
    'btn_edit' => 'সম্পাদন করা',
    'btn_like' => 'ভোট',
    'btn_send' => 'বার্তা পাঠান',
    'btn_upload' => 'একটি ছবি আপলোড করুন',
    'content_placeholder' => 'আপনার গল্পটি লিখুন, কীভাবে আপনি কিউআর চ্যানস ডট কমকে ধন্যবাদ জানিয়েছেন',
    'photo' => 'আপনি 1 বা আরও বেশি ফটো আপলোড করতে পারেন',
    'prize' => 'আপনার তারিখের গল্পটি লিখুন এবং নীচে তালিকাভুক্ত পুরস্কারগুলির মধ্যে 1 জিতে নিন',
  ),
);
