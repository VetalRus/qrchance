<?php

return array (
  'about' => 
  array (
    'project' => 
    array (
      'about' => 'QRChance.com is een portaal voor snelle en gemakkelijke dating',
      'title' => 'Over het project',
    ),
  ),
  'account' => 
  array (
    'add_printout_place' => 'Weet u waar u visitekaartjes kunt printen? Geef aan en andere deelnemers kunnen gebruiken',
    'histories' => 
    array (
      'create' => 'Schrijf geschiedenis',
      'text_history' => 'Schrijf je datingverhaal, eerste date en win waardevolle prijzen',
      'your_story' => 'Jouw verhaal',
    ),
    'list' => 
    array (
      'answered_yes' => 'Lijst met degenen die ja hebben geantwoord',
      'btn_show_message' => 'Toon meer berichten',
      'delete_message' => 'Verwijder alle berichten',
    ),
    'notification' => 
    array (
      'payment' => 
      array (
        'attention' => 'Aandacht!',
        'btn_return' => 'Terug naar persoonlijke pagina',
        'confirm' => 'De betaling is geslaagd',
        'congratulation' => 'Gefeliciteerd! Uw account is geactiveerd',
        'error' => 'Betaling mislukt!',
        'go_account' => 'Ga naar je persoonlijke account',
        'info' => 'Misschien heb je niet genoeg geld op de kaart of is er een limiet aan betalingen via internet',
        'recommendation' => 'Neem contact op met uw bankexploitant om de reden vast te stellen',
      ),
      'status' => 
      array (
        'attention' => 'Aandacht!',
        'btn_confirm' => 'Bevestigen',
        'info' => 'Nadat u uw status heeft gewijzigd in "Onzichtbaar" of "In een relatie", kunt u geen meldingen meer ontvangen van degenen die het visitekaartje hebben gekregen.',
      ),
    ),
    'payment' => 
    array (
      'currency' => 'EUR',
      'form_payment' => 'Kies een handige betaalmethode',
      'sum' => 'Voer het bedrag in',
      'title' => 'Bijvullen',
    ),
    'personal-info' => 
    array (
      'alcohol_attitude' => 'Houding ten opzichte van alcohol',
      'alcohol_compromise' => 'Compromis',
      'alcohol_negative' => 'Negatief',
      'alcohol_neutral' => 'Neutrale',
      'alcohol_positive' => 'Positief',
      'alcohol_very_negative' => 'Sterk negatief',
      'birthday' => 'Geboortedatum',
      'btn_save' => 'Sparen',
      'btn_update_photo' => 'Foto vernieuwen',
      'btn_upload_photo' => 'Upload een foto',
      'children' => 'Kinderen',
      'country' => 'Land',
      'have_children' => 'Heeft u kinderen?',
      'in_search' => 'Op zoek naar',
      'invisibility' => 'Onzichtbaar',
      'profession' => 'Beroep',
      'smoking_attitude' => 'Houding tegenover roken',
      'smoking_compromise' => 'Compromis',
      'smoking_negative' => 'Negatief',
      'smoking_neutral' => 'Neutrale',
      'smoking_positive' => 'Positief',
      'smoking_very_negative' => 'Sterk negatief',
      'status' => 'Toestand',
      'title_about' => 'Over mij',
      'title_contact_info' => 'Contactgegevens',
      'title_personal_info' => 'Persoonlijke informatie',
      'town' => 'Stad',
      'your_last_name' => 'Jouw achternaam',
      'your_last_placeholder' => 'Achternaam van de gebruiker',
      'your_name' => 'Uw naam',
      'your_name_placeholder' => 'Gebruikersnaam',
      'facebook' => 'Facebook',
      'twitter' => 'Twitter',
      'vk' => 'VK',
      'youtube' => 'Youtube',
      'instagram' => 'Instagram',
      'email' => 'E-mail:',
      'have_children_not' => 'Niet',
      'have_children_yes' => 'Ja',
      'phone' => 'Telefoonnummer:',
    ),
    'printout_places' => 'Op deze adressen kunt u visitekaartjes afdrukken',
    'qr' => 
    array (
      'adress_not_exist' => 'Dit adres bestaat niet',
      'anothe' => 'Andere',
      'btn_complain' => 'Klagen',
      'btn_create' => 'Genereer code',
      'btn_download' => 'Visitekaartjeslay-out downloaden',
      'change' => 'Bewerk',
      'email_placeholder' => 'E-mailadres',
      'find' => 'Zoek op stad',
      'info' => 'U kunt een visitekaartje downloaden en opsturen om zelf af te drukken.',
      'phone' => 'Telefoonnummer:',
      'print_info' => 'U kunt visitekaartjes afdrukken op deze adressen:',
      'save' => 'Sparen',
      'slogan' => 'Het is gemakkelijk om elkaar te ontmoeten',
      'slogan1' => 'Van',
      'text' => 'Voer tekst in voor codering',
      'text_example_placeholder' => 'Bijvoorbeeld:',
      'text_placeholder' => 'Hallo, ik vind je heel leuk. Zullen we samen eten?',
      'title' => 'QR-codegenerator',
      'title_info1' => 'Weet u waar u visitekaartjes kunt printen?',
      'title_info2' => 'Geef aan en andere deelnemers kunnen gebruiken',
      'town' => 'Straat huis:',
      'using' => 'Gebruik makend van:',
      'using_1' => '1. Neem een ​​mobiele telefoon met een camera.',
      'using_2' => '2. Voer het programma uit om de code te scannen',
      'using_3' => '3. Richt de cameralens op de code',
      'using_4' => 'Informatie krijgen!',
      'phone_placeholder' => '+31(__)___-__-__',
      'error' => 
      array (
        'address' => 'Fout bij invoeren adres',
        'city' => 'Stadsselectiefout',
        'email' => 'Fout, onjuist e-mailbericht',
        'phone' => 'Fout, onjuist telefoonnummer',
      ),
      'street/town' => 'Straat huis:',
    ),
    'security' => 
    array (
      'btn_off' => 'Uit',
      'btn_on' => 'Aan',
      'delete_account' => 'Verwijder je account',
      'email' => 'E-mailadres',
      'new_password' => 'Nieuw paswoord:',
      'notification' => 'Kennisgevingen',
      'old_password' => 'Oud Wachtwoord:',
      'password_placeholder' => 'Wachtwoord (10 tekens of meer)',
      'receive_info_fail' => 'Ik wil meldingen ontvangen',
      'receive_message' => 'Ik wil belangrijke berichten van het bedrijf ontvangen',
      'repeat_password' => 'Bevestig wachtwoord',
      'save' => 'Sparen',
      'title' => 'Veiligheid!',
    ),
    'sidebar' => 
    array (
      'activation' => 'Om uw account te activeren, moet u een eenmalige betaling van UAH 100 betalen.',
      'activation_info' => 'Uw account blijft actief totdat u uw status wijzigt in \'in een relatie\' of \'account verwijderd\'',
      'btn_psq' => 'Betalen',
      'creating_visitcard' => 'Maak een visitekaartje',
      'message' => 'Berichten',
      'other_stories' => 'Meer verhalen',
      'payment' => 'Betaling',
      'personal_info' => 'Persoonlijke gegevens',
      'settings' => 'Instellingen',
    ),
  ),
  'auth' => 
  array (
    'forgot' => 
    array (
      'agreement' => 'Door te registreren gaat u akkoord met onze servicevoorwaarden',
      'email' => 'Voer mail in',
      'email_placeholder' => 'E-mailadres',
      'go' => 'Ga naar',
      'login_with' => 'Login met:',
      'password' => 'Uw wachtwoord vergeten?',
      'recovery' => 'Wachtwoord herstel',
      'send' => 'Het bericht is naar uw e-mail gestuurd',
    ),
    'login' => 
    array (
      'account' => 'Ik heb geen account.',
      'btn_submit' => 'Binnenkomen',
      'error_login_or_password' => 'Login of wachtwoord verkeerd ingevoerd',
      'forgot_password' => 'Uw wachtwoord vergeten?',
      'name' => 'Vul uw naam in',
      'name_placeholder' => 'Gebruikersnaam',
      'password' => 'Voer wachtwoord in',
      'password_placeholder' => 'Wachtwoord (10 tekens of meer)',
      'register' => 'Registreer nu',
      'remember' => 'Om mij te herinneren',
      'social' => 'Login met:',
      'title' => 'Ingang',
      'welcome' => 'Welkom bij de portal',
    ),
    'register' => 
    array (
      'btn_next' => 'Verder',
      'btn_submit' => 'Check in',
      'email' => 'Voer mail in',
      'email_placeholder' => 'E-mailadres',
      'enter' => 'Ingang',
      'error_email' => 'E-mail is onjuist ingevoerd',
      'error_name' => 'Vul uw naam in',
      'error_password' => 'Voer wachtwoord in',
      'error_password_confirmation' => 'Bevestig het wachtwoord',
      'goals' => 'Samen met ons kunt u gemakkelijk uw liefde vinden, evenals nieuwe vrienden',
      'have_account' => 'Heeft u al een account?',
      'name' => 'Vul uw naam in',
      'name_placeholder' => 'Gebruikersnaam',
      'password' => 'Voer wachtwoord in',
      'password_confirmation' => 'Bevestig het wachtwoord',
      'password_confirmation_placeholder' => 'Wachtwoord (10 tekens of meer)',
      'password_placeholder' => 'Wachtwoord (10 tekens of meer)',
      'sex_man' => 'Ik ben een jongen',
      'sex_woman' => 'Ik ben een meisje',
      'title' => 'Check in',
      'welcome' => 'Welkom bij de portal',
    ),
  ),
  'feedback' => 
  array (
    'email' => 'E-mail',
    'email_placeholder' => 'E-mailadres',
    'error_email' => 'Vul je e-mailadres in',
    'error_name' => 'Vul uw naam in',
    'error_phone' => 'Voer uw mobiele telefoon in',
    'name' => 'Vul uw naam in:',
    'name_placeholder' => 'Gebruikersnaam',
    'phone' => 'Mobiele telefoon',
    'send' => 'Bericht versturen',
    'title' => 'Feedback',
    'phone_placeholder' => '+31(__)___-__-__',
  ),
  'language' => 'Niderand',
  'laravel' => 'Laravel',
  'layouts' => 
  array (
    'main' => 
    array (
      'about' => 'Over het project',
      'faq' => 'Veel Gestelde Vragen',
      'feedback' => 'Feedback',
      'follow' => 'Volg ons:',
      'language' => 'Site taal',
      'made' => 'Met liefde gemaakt in Oekraïne',
      'more' => 'Laat meer vragen zien',
      'partner' => 'Word projectpartner',
      'support' => 'Ondersteunende dienst:',
    ),
  ),
  'main' => 
  array (
    'index' => 
    array (
      'acquaintance' => 'Gemakkelijke kennismaking',
      'acquaintance_content' => 'Soms is er geen tijd en gelegenheid om alleen te blijven met een persoon om persoonlijk te communiceren en elkaar beter te leren kennen, met behulp van deze site kunt u op elk gewenst moment de communicatie voortzetten.',
      'create_history' => 'Schrijf geschiedenis',
      'details' => 'Meer details',
      'how_it_works' => 'Hoe het werkt',
      'impression' => 'Eerste indruk',
      'impression_content' => 'Je had de mogelijkheid om een ​​persoon te ontmoeten en de eerste indruk van hem persoonlijk te maken, en niet van een foto.',
      'item1' => 'Inschrijven',
      'item2' => 'Maak een uniek visitekaartje',
      'item3' => 'Ontmoet snel en gemakkelijk',
      'more' => 'Toon meer verhalen',
      'people' => 'Echte mensen',
      'people_content' => 'Echte mensen - geen bots of nepaccounts. U kiest zelf met wie u wilt communiceren en met wie niet.',
      'play' => 'Start het spel',
      'stories' => 'Datingverhalen van onze leden',
      'text_history' => 'Schrijf je geschiedenis van daten, eerste date en win waardevolle prijzen',
      'title' => 'Het is gemakkelijk om elkaar te ontmoeten',
      'title1' => 'Van',
      'why_this_better' => 'Waarom is deze manier van daten beter?',
      'your_story' => 'Jouw verhaal',
    ),
  ),
  'message' => 
  array (
    'btn_no' => 'Niet',
    'btn_yes' => 'Ja',
    'info' => 
    array (
      'alcohol_attitude' => 'Houding ten opzichte van alcohol:',
      'birthday' => 'Geboortedatum',
      'profession' => 'Beroep:',
      'smoking_attitude' => 'Houding ten opzichte van roken:',
    ),
    'notification' => 'Deze pagina is alleen zichtbaar voor mensen met een visitekaartje met een QR-code',
    'pop_up' => 
    array (
      'btn_send' => 'Bericht versturen',
      'name' => 'Naam',
      'name_placeholder' => 'Vul uw naam in',
      'phone' => 'Telefoonnummer',
      'title_next' => 'Zal je bellen',
      'title' => 'Schrijf uw naam, telefoonnummer en',
      'phone_placeholder' => '+31(__)___-__-__',
    ),
    'social' => 'Mijn sociale netwerken',
    'title' => 'Sergey heeft een bericht voor je achtergelaten!',
    'answer_not' => 'Niet',
    'answer_yes' => 'Ja',
    'status' => 
    array (
      'end_search' => 'De zoektocht is voorbij!',
      'in_search_of' => 'Op zoek naar!',
    ),
    'title_1' => 'Voor jou',
    'title_2' => 'heeft een bericht achtergelaten',
  ),
  'stories' => 
  array (
    'btn_like' => 'Stemmen',
    'btn_send' => 'Bericht versturen',
    'btn_upload' => 'Upload een foto',
    'content_placeholder' => 'Schrijf je verhaal, het verhaal van hoe je elkaar hebt ontmoet dankzij QRChance.com',
    'photo' => 'Je kunt 1 of meer foto\'s uploaden',
    'prize' => 'Schrijf je date-verhaal en win 1 van de onderstaande prijzen',
    'btn_edit' => 'Bewerk',
  ),
  'pagination' => 
  array (
    'next' => 'Volgend',
    'previous' => 'Vorige',
  ),
  'passwords' => 
  array (
    'reset' => 'Sua senha foi alterada!',
    'sent' => 'We hebben u een link gestuurd om uw wachtwoord per e-mail te resetten!',
    'throttled' => 'Wacht a.u.b. voordat u het opnieuw probeert.',
    'token' => 'Deze wachtwoordherstel-ID is niet geldig',
    'user' => 'We kunnen geen gebruiker vinden met dit e-mailadres.',
  ),
);
