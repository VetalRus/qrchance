<?php

return array (
  'laravel' => 'Ларавел',
  'language' => 'Русский',
  'main' => 
  array (
    'index' => 
    array (
	  'title' => 'Знакомся легко вместе',
      'title1' => 'c',
      'item1' => 'Пройдите регистрацию',
      'item2' => 'Создайте уникальную визитку',
      'item3' => 'Знакомьтесь легко и быстро',
      'play' => 'Начать игру',
      'how_it_works' => 'Как это работает',
      'why_this_better' => 'Чем этот способ знакомства лучше?',
      'impression' => 'Первое впечатление',
      'impression_content' => 'У Вас была возможность увидеться с человеком и создать первое впечатление о нем при личной встрече, а не по фото.',
      'acquaintance' => 'Лёгкость в знакомстве',
      'acquaintance_content' => 'Иногда нет времени и возможности остаться с человеком наедине лично пообщаться и узнать друг друга по лучше, с помощью данного сайта Вы можете продолжить общение в любое удобное время.',
      'people' => 'Реальные люди',
      'people_content' => 'Реальные люди - никаких ботов или подставленных аккаунтов. Вы сами выбираете с кем общаться, а с кем нет.',
      'stories' => 'Истории знакомств наших участников',
      'your_story' => 'Ваша история',
      'create_history' => 'Написать историю',
      'text_history' => 'Напишите Вашу историю знакомства, первого свидания и выигрывайте ценные призы',
      'details' => 'Подробнее',
      'more' => 'Показать ещё истории',
    ),
  ),
  'layouts' => 
  array (
    'main' => 
    array (
      'faq' => 'Часто задаваемые вопросы',
      'more' => 'Показать еще вопросы',
      'partner' => 'Стать партнером проекта',
      'about' => 'О проекте',
      'feedback' => 'Обратная связь',
      'language' => 'Язык сайта',
      'support' => 'Служба поддержки:',
      'follow' => 'Следите за нами:',
      'made' => 'Сделано в Украине с любовью',
    ),
  ),
  'stories' => 
  array (
    'btn_upload' => 'Загрузить фото',
    'content_placeholder' => 'Напишите Вашу историю, историю о том как Вы познакомились благодаря порталу QRChance.com',
    'photo' => 'Вы можете загрузить 1 или несколько фото',
    'btn_send' => 'Отправить',
    'btn_like' => 'Проголосовать',
    'prize' => 'Напишите историю Вашего свидания и выиграйте 1 из призов указанных ниже',
    'btn_edit' => 'Отредактировать',
  ),
  'about' => 
  array (
    'project' => 
    array (
      'title' => 'О проекте',
      'about' => 'QRChance.com - Это портал для быстрого и удобного знакомства',
    ),
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'title' => 'Вход',
      'name' => 'Введите имя',
      'name_placeholder' => 'Имя пользователя',
      'password' => 'Введите пароль',
      'password_placeholder' => 'Пароль (от 10 символов)',
      'welcome' => 'Добро пожаловать на портал',
      'btn_submit' => 'Войти',
      'account' => 'У меня нет аккаунта.',
      'remember' => 'Запомнить меня',
      'forgot_password' => 'Забыли пароль?',
      'register' => 'Зарегистрироваться',
      'error_login_or_password' => 'Логин или пароль введен не верно',
      'social' => 'Войти с помощью:',
    ),
    'forgot' => 
    array (
      'password' => 'Забыли пароль?',
      'email' => 'Введите почту',
      'email_placeholder' => 'Электронный адрес',
      'login_with' => 'Войти с помощью:',
      'recovery' => 'Восстановление пароля',
      'send' => 'Сообщение отправлено Вам на почту',
      'go' => 'Перейти',
      'agreement' => 'Регистрируясь вы соглашаетесь с нашими условиями предоставления услуг',
    ),
    'register' => 
    array (
      'title' => 'Регистрация',
      'name' => 'Введите имя',
      'name_placeholder' => 'Имя пользователя',
      'email' => 'Введите почту',
      'email_placeholder' => 'Электронный адрес',
      'password' => 'Введите пароль',
      'password_placeholder' => 'Пароль (от 10 символов)',
      'password_confirmation' => 'Подтвердите пароль',
      'password_confirmation_placeholder' => 'Пароль (от 10 символов)',
      'welcome' => 'Добро пожаловать на портал',
      'btn_next' => 'Дальше',
      'btn_submit' => 'Регистрация',
      'have_account' => 'У вас уже есть учетная запись?',
      'enter' => 'Вход',
      'goals' => 'Вместе с нами вы легко сможете найти свою любовь, а так же новых друзей',
      'sex_man' => 'Я парень',
      'sex_woman' => 'Я девушка',
      'error_name' => 'Введите имя',
      'error_email' => 'Email введен не верно',
      'error_password' => 'Введите пароль',
      'error_password_confirmation' => 'Подтвердите пароль',
      'verify_text' => 'Сообщение о веритификации отправлено Вам на почту',
    ),
  ),
  'feedback' => 
  array (
    'title' => 'Обратная связь',
    'name' => 'Введите имя:',
    'name_placeholder' => 'Имя пользователя',
    'error_name' => 'Введите имя',
    'email' => 'E-mail',
    'email_placeholder' => 'Электронный адрес',
    'error_email' => 'Введите электронный адрес',
    'phone' => 'Мобильный телефон',
    'phone_placeholder' => '+7(__)___-__-__',
    'error_phone' => 'Введите мобильный телефон',
    'send' => 'Отправить',
  ),
  'account' => 
  array (
    'sidebar' => 
    array (
      'personal_info' => 'Личные данные',
      'creating_visitcard' => 'Создание визитки',
      'message' => 'Сообщения',
      'other_stories' => 'Другие истории',
      'payment' => 'Оплата',
      'settings' => 'Настройки',
      'activation' => 'Для активации аккаунта Вам нужно заплатить едино разовый взнос 100 грн.',
      'activation_info' => 'Ваш аккаунт будет активный пока вы не измените статус на "в отношениях" или "аккаунт удален"',
      'btn_psq' => 'Оплатить',
    ),
    'personal-info' => 
    array (
      'title_personal_info' => 'Личная информация',
      'your_name' => 'Ваше имя',
      'your_name_placeholder' => 'Имя пользователя',
      'your_last_name' => 'Ваша фамилия',
      'your_last_placeholder' => 'Фамилия пользователя',
      'birthday' => 'Дата рождения',
      'country' => 'Страна',
      'town' => 'Город',
      'status' => 'Статус',
      'title_about' => 'Обо мне',
      'have_children' => 'У Вас есть дети?',
      'children' => 'Дети',
      'smoking_attitude' => 'Отношение к курению',
      'smoking_positive' => 'Положительное',
      'smoking_neutral' => 'Нейтральное',
      'smoking_compromise' => 'Компромиссное',
      'smoking_negative' => 'Негативное',
      'smoking_very_negative' => 'Резко негативное',
      'alcohol_attitude' => 'Отношение к алкоголю',
      'alcohol_positive' => 'Положительное',
      'alcohol_neutral' => 'Нейтральное',
      'alcohol_compromise' => 'Компромиссное',
      'alcohol_negative' => 'Негативное',
      'alcohol_very_negative' => 'Резко негативное',
      'profession' => 'Профессия',
      'title_contact_info' => 'Контактная информация',
      'btn_save' => 'Сохранить',
      'in_search' => 'В поиске',
      'invisibility' => 'Невидимка',
      'btn_upload_photo' => 'Загрузить фото',
      'btn_update_photo' => 'Обновить фото',
      'have_children_not' => 'Нет',
      'have_children_yes' => 'Да',
      'email' => 'E-mail:',
      'phone' => 'Номер телефона:',
      'vk' => 'VK',
      'instagram' => 'Instagram',
      'facebook' => 'Facebook',
      'twitter' => 'Twitter',
      'youtube' => 'Youtube',
      'error_profession' => 'Напишите свою профессию',
      'error_smoking' => 'Выберите своё отношение к курению',
      'error_city_id' => 'Выберите город',
      'error_country_id' => 'Выберите страну',
      'error_second_name' => 'Напишите свою фамилию',
      'error_alcohol' => 'Выберите своё отношение к алкоголю',
    ),
    'histories' => 
    array (
      'your_story' => 'Ваша история',
      'create' => 'Написать историю',
      'text_history' => 'Напишите Вашу историю знакомства, первого свидания и выигрывайте ценные призы',
    ),
    'list' => 
    array (
      'answered_yes' => 'Список тех, кто ответил Да',
      'delete_message' => 'Удалить все сообщения',
      'btn_show_message' => 'Показать еще сообщения',
    ),
    'qr' => 
    array (
      'title' => 'Генератор QR кодов',
      'text' => 'Введите текст для кодировки',
      'text_example_placeholder' => 'Например:',
      'text_placeholder' => 'Привет, ты мне очень нравишься. Поужинаем вместе?',
      'btn_create' => 'Создать код',
      'using' => 'Использование:',
      'using_1' => '1. Возьмите мобильный телефон с камерой.',
      'using_2' => '2. Запустите программу для сканирования кода',
      'using_3' => '3. Наведите объектив камеры на код',
      'using_4' => 'Получите информацию!',
      'slogan' => 'Знакомится легко вместе',
      'slogan1' => 'c',
      'btn_download' => 'Скачать макет визитки',
      'info' => 'Вы можете скачать визитку и самостоятельно отправить её на печать.',
      'print_info' => 'Вы можете распечатать визитки по этим адресам:',
      'find' => 'Поиск по г.',
      'change' => 'Изменить',
      'btn_complain' => 'Пожаловаться',
      'adress_not_exist' => 'Такого адреса не существует',
      'anothe' => 'Другое',
      'title_info1' => 'Знаете где можно распечатать визитки?',
      'title_info2' => 'Укажите и другие участники могут воспользоваться',
      'town' => 'Город:',
      'email_placeholder' => 'Электронный адрес',
      'phone' => 'Номер телефона:',
      'phone_placeholder' => '+7(__)___-__-__',
      'save' => 'Сохранить',
      'street/town' => 'Улица/дом:',
      'error' => 
      array (
        'city' => 'Ошибка при выборе города',
        'address' => 'Ошибка при вводе адреса',
        'email' => 'Ошибка, некоректный E-mail',
        'phone' => 'Ошибка, некоректный номер телефона',
      ),
    ),
    'payment' => 
    array (
      'title' => 'Пополнение счета',
      'sum' => 'Введите сумму',
      'currency' => 'RUB',
      'form_payment' => 'Выберете удобную форму оплаты',
    ),
    'notification' => 
    array (
      'payment' => 
      array (
        'confirm' => 'Оплата прошла успешно',
        'congratulation' => 'Поздравляем! Ваш аккаунт активирован',
        'go_account' => 'Перейти в личный кабинет',
        'attention' => 'Внимание!',
        'error' => 'Оплата не удалась!',
        'info' => 'Возможно у вас недостаточно денежных средств на карте или установлен лимит на оплаты через интернет',
        'recommendation' => 'Свяжитесь с оператором вашего банка для установления причины.',
        'btn_return' => 'Вернутся на личную страницу',
      ),
      'status' => 
      array (
        'attention' => 'Внимание!',
        'info' => 'После изменения статуса на <<Невидимка>>, или <<В отношениях>> Вы больше не сможете получать уведомления от тех кому вручили визитку.',
        'btn_confirm' => 'Подтвердить',
      ),
    ),
    'security' => 
    array (
      'title' => 'Безопасность!',
      'old_password' => 'Старый пароль:',
      'new_password' => 'Новый пароль:',
      'password_placeholder' => 'Пароль (от 10 символов)',
      'repeat_password' => 'Повторите пароль',
      'delete_account' => 'Удалить учетную запись',
      'save' => 'Сохранить',
      'notification' => 'Уведомления',
      'email' => 'Электронный адрес',
      'receive_info_fail' => 'Хочу получать уведомления',
      'receive_message' => 'Хочу получать важные сообщения от компании',
      'btn_on' => 'Вкл',
      'btn_off' => 'Выкл',
      'referral_title' => 'Рефералка',
      'referral_input' => 'Реферальная ссылка',
      'no_exist' => 'Не создана',
      'btn_create' => 'Создать',
    ),
    'add_printout_place' => 'Знаете где можно распечатать визитки? Укажите и другие участники могут воспользоваться',
    'printout_places' => 'Вы можете распечатать визитки по этим адресам',
  ),
  'message' => 
  array (
    'title' => 'Для Вас Сергей оставил сообщение!',
    'btn_yes' => 'Да',
    'btn_no' => 'Нет',
    'social' => 'Мои социальные сети',
	'notification' => 'Эту страницу видят только те, у кого есть визитка с QR кодом',
	'info_1' => 'После совпадение пары попросите изменить статус с "в поиске" на "в отношениях" или "акаунт закрыт" для того что бы',
	'info_2' => 'не смог получать новые уведомления',
    'info' => 
    array (
      'alcohol_attitude' => 'Отношение к алкоголю:',
      'birthday' => 'Дата рождения',
      'profession' => 'Профессия:',
      'smoking_attitude' => 'Отношение к курению:',
    ),
    'pop_up' => 
    array (
      'title' => 'Напишите свое имя, номер телефона и',
      'title_next' => 'наберет Вас',
      'name' => 'Имя',
      'name_placeholder' => 'Укажите Ваше имя',
      'phone' => 'Номер телефона',
      'phone_placeholder' => '+7(__)___-__-__',
      'btn_send' => 'Отправить',
    ),
    'title_1' => 'Для Вас',
    'title_2' => 'оставил сообщение',
    'status' => 
    array (
      'in_search_of' => 'В поиске!',
      'end_search' => 'Поиск закончен!',
    ),
    'answer_yes' => 'Да',
    'answer_not' => 'Нет',
  ),
  'pagination' => 
  array (
    'previous' => 'Предыдущий',
    'next' => 'Следующий',
  ),
  'passwords' => 
  array (
    'reset' => 'Пароль был сброшен!',
    'sent' => 'Мы отправили вам ссылку для сброса пароля по электронной почте!',
    'throttled' => 'Пожалуйста, подождите, прежде чем повторить попытку.',
    'token' => 'Этот идентификатор сброса пароля недействителен',
    'user' => 'Мы не можем найти пользователя с этим адресом электронной почты.',
  ),
  'error' => 
  array (
    'btn_go' => 'Дальше',
    'title' => 'Внимание!',
    'fill_inputs' => 'Заполните все поля на личной странице и поставьте свое фото для продолжения игры!',
    'no_exist_qrcode' => 'Прежде чем скачать визитку, для начала создайте QRcode',
  ),


   //Rusal----------------------------------------------------------------------------------------
   'pay' =>
   array(
	   'for_activation' => ' Для активации аккаунта Вам нужно
		   заплатить единоразовый взнос',
	   'info_about_change_status' => 'Ваш аккаунт будет активный пока вы не
		   измените статус на "в отношениях" или
		   "аккаунт удалён"',
	   'pay' => 'Оплатить',

   ),

   'profile' =>
   array(
	   'btn_upload_photo' => 'Загрузить фото',
	   'btn_update_photo' => 'Обновить фото',
	   'in_search' => 'В поиске',
	   'invisibility' => 'Невидимка',
	   'birthday' => 'Дата рождения',
	   'children' => 'Дети',
	   'have_children_not' => 'Нет',
	   'have_children_yes' => 'Да',
	   'smoking_attitude' => 'Отношение к курению',
	   'alcohol_attitude' => 'Отношение к алкоголю',
	   'profession' => 'Профессия',
   ),

   'history' =>
   array(
	   'share_social_networks' => 'Поделиться в социальных сетях',
   ),

   'about_project' =>
   array(
	   'title' => 'О проекте',
	   'description' => 'Happymoments.ua - это портал быстрого и удобного
		   поиска профессионалов для проведения любого типа мероприятия, а также
		   для удобного поиска фотостудии, банкетного зала, ивент агенства, которые
			помогут создать праздник, которого Вы достойны. На нашем портале Вы можете
			 найти такие услуги как: аренда автомобилей, развлечения и шоу программы,
			  которые порадуют Вас и Ваших гостей, а так же другие предложения....
			   В каком бы городе Вы не находились, участники нашего сайта смогут
			   организовать самый незабываемый и яркий праздник для Вас, Вашей семьи, друзей, коллег...',
	   'feedback' => 'Обратная связь',
	   'name' => 'Введите имя',
	   'name_placeholder' => 'Имя пользователя',
	   'email' => 'E-mail',
	   'email_placeholder' => 'Электронный адрес',
	   'phone' => 'Мобильный телефон',
	   'phone_placeholder' => '+7(__)___-__-__',
	   'write_message'=> 'Напишите сообщение',
	   'send_message'=> 'Отправить',

   ),

   'dropdown_header_user_menu' =>
   array(
	 'my_profile' => 'Мой профиль',
	 'personal_info' => 'Личные данные',
	 'creating_visitcard' => 'Создание визитки',
	 'message' => 'Сообщения',
	 'other_stories' => 'Другие истории',
	 'payment' => 'Оплата',
	 'settings' => 'Настройки',
   ),

   'feedback' =>
   array(
	   'write_message'=> 'Напишите сообщение',
   ),

   'account.qr.title_qrcode' => "Ваш QR код:",
   'error.authenticate' => 'Войдите на ваш акаут или создайте его чтобы перейти на страницу',
   'login' => 'Войти',
   'register' => 'Регистрация',
   'error.admin.is-admin' => 'Войдите под админном!!!',
   'error.admin.authenticate' => 'Войдите на ваш акаут с доступом Admin чтобы перейти на страницу',
   'account.qr.email' => 'Email:',
   'account.qr.address.placeholder' => '',
   'error.upload_avatar' => 'Загрузите фото чтобы сохранить свои данные',

   'personal-info.month.january' => 'Январь',
	'personal-info.month.february' => 'Февраль',
	'personal-info.month.march' => 'Март',
	'personal-info.month.april' => 'Апрель',
	'personal-info.month.may' => 'Май',
	'personal-info.month.june' => 'Июнь',
	'personal-info.month.july' => 'Июль',
	'personal-info.month.august' => 'Август',
	'personal-info.month.september' => 'Сентябрь',
	'personal-info.month.october' => 'Октябрь',
	'personal-info.month.november' => 'Ноябрь',
	'personal-info.month.december' => 'Декабрь',

	'warning.user_stopped_searching' => 'Пользователь прекратил поиск',
	'account.warning.update_status' => 'Статус был изменён, успешно!!!',
	'account.warning.new_view_messages' => 'Вы сменили статус и теперь можете смотреть новые сообщения и пользоватся QRcode',
	'account.warning.create_history' => 'Вы сменили статус и теперь можете написать свою историю знакомства',
	'account.profile.warning_change_status' => 'После изменения статуса на',
  'account.profile.warning_change_status1' => '"в отношениях"',
  'account.profile.warning_change_status2' => 'вы не сможете больше получать уведомление о совпадении пары.',
	'account.profile.btn_change' => 'Сменить',
	'message.no_messages' => 'Пока что сообщений нет :(',
	'message.change_status' => 'Чтобы увидеть новые сообщение смените статус',
	'stories.your_photos' => 'Ваши фотографии:',
	'error.change-status-relationship' => 'Смените статус на "В отношениях" чтобы перейти на страницу',
	'qrcode.send-message.phone_placeholder' => '+7(__)___-__-__',
	'qrcode.send-message.name' => 'Имя',
	'qrcode.send-message.btn_go' => 'Отправить',
	'qrcode.send-message.name_placeholder' => 'Укажите Ваше имя',
	'qrcode.modal_title_1' => 'Напишите свое имя, номер телефона и',
	'qrcode.modal_title_2' => 'наберет Вас',
	'qrcode.send-message.phone' => 'Номер телефона',
	'auth.register.18_years' => 'Подтверждение что вам есть 18 лет',
	'error.admin.authenticate'  => 'Войдите пользователем с Админ Правами',
	'auth.register.error_18_years' => 'Вы не подтвердили что вам есть 18 лет.',
	'account.personal-info.error_birthday' => 'Выберите свою дату розждения полностью',
	'account.personal-info.error_children' => 'Выберите есть ли у вас дети',
	'auth.register.error_recaptcha_response' => 'Убедитесь, что вы не робот.',
	'auth.register.error_duplicate-email' => 'Email дублируется, используйте другой',
	'account.qr.slogan2' => 'Знакомься легко',
	'account.qr.slogan3' => 'вместе с',
	'account.qr.slogan4' => 'Сделано в Украине c любовью компанией ',
	'account.qr.slogan5' => 'Сканируй',
	'account.qr.slogan6' => ' код сейчас',
	'account.qr.slogan7' => 'Для вас оставлено сообщение',
	'account.qr.company' => 'educoin.biz',
	'auth.login.email' => 'Введите почту',
	'auth.login.email_placeholder' => 'Электронный адрес',
   'auth.login.error_validate' => 'Вы не пройшли веритификацию',
   'success.title' => 'Успешно!',
   'success.btn_go' => 'Дальше',
   'succses.verify' => 'Веритификация пройшла успешно',
   'succses.business_card' => 'Визитка создалась успешно',
   'account.succses.update-info' => 'Изменения успешно сохранены',
   'feedback.title' => 'Обратная связь!',
   'qr.thanks.title' => 'Спасибо за Ваш ответ!',
   'qr.thanks.welcome' => 'Знакомся легко вместе с',
);
