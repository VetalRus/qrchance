<?php

return array (
  'about' => 
  array (
    'project' => 
    array (
      'about' => 'QRChance.com adalah portal untuk kencan yang cepat dan mudah',
      'title' => 'Tentang proyek tersebut',
    ),
  ),
  'account' => 
  array (
    'add_printout_place' => 'Tahukah Anda di mana Anda dapat mencetak kartu nama? Tunjukkan dan peserta lain dapat menggunakan',
    'histories' => 
    array (
      'create' => 'Tulis sejarah',
      'text_history' => 'Tulis sejarah kencan Anda, kencan pertama dan menangkan hadiah berharga',
      'your_story' => 'Ceritamu',
    ),
    'list' => 
    array (
      'answered_yes' => 'Daftar mereka yang menjawab Ya',
      'btn_show_message' => 'Tampilkan lebih banyak posting',
      'delete_message' => 'Hapus semua pesan',
    ),
    'notification' => 
    array (
      'payment' => 
      array (
        'attention' => 'Perhatian!',
        'btn_return' => 'Kembali ke halaman pribadi',
        'confirm' => 'Pembayaran berhasil',
        'congratulation' => 'Selamat! Akun Anda diaktifkan',
        'error' => 'Pembayaran gagal!',
        'go_account' => 'Buka akun pribadi Anda',
        'info' => 'Mungkin Anda tidak memiliki cukup uang di kartu Anda atau Anda memiliki batasan pembayaran melalui Internet',
        'recommendation' => 'Hubungi operator bank Anda untuk mengetahui alasannya.',
      ),
      'status' => 
      array (
        'attention' => 'Perhatian!',
        'btn_confirm' => 'Konfirmasi',
        'info' => 'Setelah mengubah status Anda menjadi "Tak terlihat", atau "Dalam suatu hubungan", Anda tidak dapat lagi menerima pemberitahuan dari mereka yang diberi kartu nama.',
      ),
    ),
    'payment' => 
    array (
      'currency' => 'IDR',
      'form_payment' => 'Pilih bentuk pembayaran yang nyaman',
      'sum' => 'Masukkan jumlahnya',
      'title' => 'Isi ulang',
    ),
    'personal-info' => 
    array (
      'alcohol_attitude' => 'Sikap terhadap alkohol',
      'alcohol_compromise' => 'Kompromi',
      'alcohol_negative' => 'Negatif',
      'alcohol_neutral' => 'Netral',
      'alcohol_positive' => 'Positif',
      'alcohol_very_negative' => 'Sangat negatif',
      'birthday' => 'Tanggal lahir',
      'btn_save' => 'Menyimpan',
      'btn_update_photo' => 'Segarkan foto',
      'btn_upload_photo' => 'Mengunggah foto',
      'children' => 'Anak-anak',
      'country' => 'Negara',
      'email' => 'Surel:',
      'facebook' => 'Facebook',
      'have_children' => 'Apa anda punya anak?',
      'have_children_not' => 'Tidak',
      'have_children_yes' => 'Iya',
      'in_search' => 'Mencari',
      'instagram' => 'Instagram',
      'invisibility' => 'Tak terlihat',
      'phone' => 'Nomor telepon:',
      'profession' => 'Profesi',
      'smoking_attitude' => 'Sikap terhadap merokok',
      'smoking_compromise' => 'Kompromi',
      'smoking_negative' => 'Negatif',
      'smoking_neutral' => 'Netral',
      'smoking_positive' => 'Positif',
      'smoking_very_negative' => 'Sangat negatif',
      'status' => 'Status',
      'title_about' => 'Tentang saya',
      'title_contact_info' => 'Kontak informasi',
      'title_personal_info' => 'Informasi pribadi',
      'town' => 'Kota',
      'twitter' => 'Twitter',
      'vk' => 'VK',
      'your_last_name' => 'Nama belakang Anda',
      'your_last_placeholder' => 'Nama belakang pengguna',
      'your_name' => 'Namamu',
      'your_name_placeholder' => 'Nama pengguna',
      'youtube' => 'Youtube',
    ),
    'printout_places' => 'Anda dapat mencetak kartu nama di alamat ini',
    'qr' => 
    array (
      'adress_not_exist' => 'Tidak ada alamat seperti itu',
      'anothe' => 'Lain',
      'btn_complain' => 'Mengeluh',
      'btn_create' => 'Hasilkan kode',
      'btn_download' => 'Unduh tata letak kartu nama',
      'change' => 'Sunting',
      'email_placeholder' => 'Alamat email',
      'error' => 
      array (
        'address' => 'Terjadi kesalahan saat memasukkan alamat',
        'city' => 'Kesalahan pemilihan kota',
        'email' => 'Error, email salah',
        'phone' => 'Kesalahan, nomor telepon salah',
      ),
      'find' => 'Cari berdasarkan kota',
      'info' => 'Anda dapat mengunduh kartu nama dan mengirimkannya untuk dicetak sendiri.',
      'phone' => 'Nomor telepon:',
      'phone_placeholder' => '+62(__)___-__-__',
      'print_info' => 'Anda dapat mencetak kartu nama di alamat berikut:',
      'save' => 'Menyimpan',
      'slogan' => 'Mudah untuk bertemu bersama',
      'slogan1' => 'Dari',
      'street/town' => 'Rumah jalanan:',
      'text' => 'Masukkan teks untuk encoding',
      'text_example_placeholder' => 'Sebagai contoh:',
      'text_placeholder' => 'Hai, aku sangat menyukaimu. Haruskah kita makan malam bersama?',
      'title' => 'Generator kode QR',
      'title_info1' => 'Tahukah Anda di mana Anda dapat mencetak kartu nama?',
      'title_info2' => 'Tunjukkan dan peserta lain dapat menggunakan',
      'town' => 'Rumah jalanan:',
      'using' => 'Menggunakan:',
      'using_1' => '1. Ambil ponsel dengan kamera.',
      'using_2' => '2. Jalankan program untuk memindai kode',
      'using_3' => '3. Arahkan lensa kamera ke kode',
      'using_4' => 'Mendapatkan informasi!',
    ),
    'security' => 
    array (
      'btn_off' => 'Mati',
      'btn_on' => 'Di',
      'delete_account' => 'Hapus akun anda',
      'email' => 'Alamat email',
      'new_password' => 'Kata sandi baru:',
      'notification' => 'Notifikasi',
      'old_password' => 'Password lama:',
      'password_placeholder' => 'Kata sandi (10 karakter atau lebih)',
      'receive_info_fail' => 'Saya ingin menerima notifikasi',
      'receive_message' => 'Saya ingin menerima pesan penting dari perusahaan',
      'repeat_password' => 'Konfirmasi sandi',
      'save' => 'Menyimpan',
      'title' => 'Keamanan!',
    ),
    'sidebar' => 
    array (
      'activation' => 'Untuk mengaktifkan akun Anda, Anda perlu membayar satu kali pembayaran sejumlah UAH 100.',
      'activation_info' => 'Akun Anda akan aktif sampai Anda mengubah status Anda menjadi "dalam suatu hubungan" atau "akun dihapus"',
      'btn_psq' => 'Membayar',
      'creating_visitcard' => 'Buat kartu nama',
      'message' => 'Posting',
      'other_stories' => 'Lebih banyak cerita',
      'payment' => 'Pembayaran',
      'personal_info' => 'Data pribadi',
      'settings' => 'Pengaturan',
    ),
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'password_placeholder' => 'Kata sandi (10 karakter atau lebih)',
      'account' => 'Saya tidak punya akun.',
      'btn_submit' => 'Untuk masuk',
      'error_login_or_password' => 'Login atau kata sandi salah dimasukkan',
      'forgot_password' => 'lupa kata sandi Anda?',
      'name' => 'Masukkan nama Anda',
      'name_placeholder' => 'Nama pengguna',
      'password' => 'Masukkan kata kunci',
      'register' => 'Daftar sekarang',
      'remember' => 'Untuk mengingat saya',
      'social' => 'Masuk dengan:',
      'title' => 'Jalan masuk',
      'welcome' => 'Selamat datang di portal',
    ),
    'register' => 
    array (
      'password_confirmation_placeholder' => 'Kata sandi (10 karakter atau lebih)',
      'password_placeholder' => 'Kata sandi (10 karakter atau lebih)',
      'btn_next' => 'Lebih jauh',
      'btn_submit' => 'Mendaftar',
      'email' => 'Masukkan surat',
      'email_placeholder' => 'Alamat email',
      'enter' => 'Jalan masuk',
      'error_email' => 'Email salah dimasukkan',
      'error_name' => 'Masukkan nama Anda',
      'error_password' => 'Masukkan kata kunci',
      'error_password_confirmation' => 'Konfirmasikan kata sandi',
      'goals' => 'Bersama kami, Anda dapat dengan mudah menemukan cinta, serta teman baru',
      'have_account' => 'Sudah memiliki akun?',
      'name' => 'Masukkan nama Anda',
      'name_placeholder' => 'Nama pengguna',
      'password' => 'Masukkan kata kunci',
      'password_confirmation' => 'Konfirmasikan kata sandi',
      'sex_man' => 'Saya anak lelaki',
      'sex_woman' => 'Aku seorang gadis',
      'title' => 'Mendaftar',
      'welcome' => 'Selamat datang di portal',
    ),
    'forgot' => 
    array (
      'agreement' => 'Dengan mendaftar, Anda menyetujui persyaratan layanan kami',
      'email' => 'Masukkan surat',
      'email_placeholder' => 'Alamat email',
      'go' => 'Pergi ke',
      'login_with' => 'Masuk dengan:',
      'password' => 'lupa kata sandi Anda?',
      'recovery' => 'Pemulihan kata sandi',
      'send' => 'Pesan telah dikirim ke email Anda',
    ),
  ),
  'feedback' => 
  array (
    'email' => 'Surel',
    'email_placeholder' => 'Alamat email',
    'error_email' => 'Masukkan alamat email Anda',
    'error_name' => 'Masukkan nama Anda',
    'error_phone' => 'Masukkan ponsel Anda',
    'name' => 'Masukkan nama Anda:',
    'name_placeholder' => 'Nama pengguna',
    'phone' => 'Telepon genggam',
    'phone_placeholder' => '+62(__)___-__-__',
    'send' => 'Mengirim pesan',
    'title' => 'Umpan balik',
  ),
  'language' => 'Indonesia',
  'laravel' => 'Laravel',
  'layouts' => 
  array (
    'main' => 
    array (
      'about' => 'Тentang proyek tersebut',
      'faq' => 'Pertanyaan yang Sering Diajukan',
      'feedback' => 'Umpan balik',
      'follow' => 'Ikuti kami:',
      'language' => 'Bahasa situs',
      'made' => 'Dibuat di Ukraina dengan cinta',
      'more' => 'Tampilkan lebih banyak pertanyaan',
      'partner' => 'Menjadi mitra proyek',
      'support' => 'Layanan dukungan:',
    ),
  ),
  'main' => 
  array (
    'index' => 
    array (
      'acquaintance' => 'Kemudahan berkenalan',
      'acquaintance_content' => 'Terkadang tidak ada waktu dan kesempatan untuk tinggal sendiri dengan seseorang untuk berkomunikasi secara pribadi dan saling mengenal lebih baik, dengan bantuan situs ini Anda dapat melanjutkan komunikasi kapan saja.',
      'create_history' => 'Tulis sejarah',
      'details' => 'Keterangan lebih lanjut',
      'how_it_works' => 'Bagaimana itu bekerja',
      'impression' => 'Kesan pertama',
      'impression_content' => 'Anda memiliki kesempatan untuk bertemu seseorang dan menciptakan kesan pertama tentang dirinya secara langsung, dan bukan dari foto.',
      'item1' => 'Daftar',
      'item2' => 'Buat kartu nama yang unik',
      'item3' => 'Bertemu dengan cepat dan mudah',
      'more' => 'Tampilkan lebih banyak cerita',
      'people' => 'Orang sungguhan',
      'people_content' => 'Orang sungguhan - tidak ada bot atau akun palsu. Anda memilih dengan siapa untuk berkomunikasi dan dengan siapa tidak.',
      'play' => 'Memulai permainan',
      'stories' => 'Cerita kencan anggota kami',
      'text_history' => 'Tulis sejarah kencan Anda, kencan pertama dan menangkan hadiah berharga',
      'title' => 'Mudah untuk bertemu bersama',
      'title1' => 'Dari',
      'why_this_better' => 'Mengapa cara berkencan ini lebih baik?',
      'your_story' => 'Ceritamu',
    ),
  ),
  'message' => 
  array (
    'answer_not' => 'Tidak',
    'answer_yes' => 'Iya',
    'btn_yes' => 'Iya',
    'btn_no' => 'Tidak',
    'info' => 
    array (
      'alcohol_attitude' => 'Sikap terhadap alkohol:',
      'birthday' => 'Tanggal lahir',
      'profession' => 'Profesi:',
      'smoking_attitude' => 'Sikap terhadap merokok:',
    ),
    'notification' => 'Halaman ini hanya dapat dilihat oleh mereka yang memiliki kartu bisnis dengan kode QR',
    'pop_up' => 
    array (
      'btn_send' => 'Mengirim pesan',
      'name' => 'Nama',
      'name_placeholder' => 'Masukkan nama Anda',
      'phone' => 'Nomor telepon',
      'phone_placeholder' => '+62(__)___-__-__',
      'title' => 'Tulis nama Anda, nomor telepon dan',
      'title_next' => 'Akan menghubungi Anda',
    ),
    'social' => 'Jejaring sosial saya',
    'status' => 
    array (
      'end_search' => 'Pencarian selesai!',
      'in_search_of' => 'Mencari!',
    ),
    'title' => 'Sergey meninggalkan pesan untuk Anda!',
    'title_1' => 'Untukmu',
    'title_2' => 'Tinggalkan pesan',
  ),
  'pagination' => 
  array (
    'next' => 'Berikut',
    'previous' => 'Sebelumnya',
  ),
  'passwords' => 
  array (
    'reset' => 'Kata sandi telah disetel ulang!',
    'sent' => 'Kami telah mengirimi Anda tautan untuk mengatur ulang kata sandi Anda melalui email!',
    'throttled' => 'Harap tunggu sebelum mencoba lagi.',
    'token' => 'ID pengaturan ulang kata sandi ini tidak valid',
    'user' => 'Kami tidak dapat menemukan pengguna dengan alamat email ini.',
  ),
  'stories' => 
  array (
    'btn_edit' => 'Sunting',
    'btn_like' => 'Pilih',
    'btn_send' => 'Mengirim pesan',
    'btn_upload' => 'Mengunggah foto',
    'content_placeholder' => 'Tulis kisah Anda, kisah bagaimana Anda bertemu berkat QRChance.com',
    'photo' => 'Anda dapat mengunggah 1 foto atau lebih',
    'prize' => 'Tulis kisah kencan Anda dan menangkan salah satu hadiah yang tercantum di bawah ini',
  ),
);
