<div class="row mt-3">
    <div class="col-12">
        {{ $history->content }}
    </div>
</div>

<div class="row mt-3 py-3">
    <div class="col-12 col-md-5 ">
        <form action="{{ route('main.history.create-likes', $history->id) }}" method="POST" class=" d-flex align-items-center">
            @csrf
            <button type="submit" class="btn btn-lg bg-orange py-2 px-4" style="max-width: 180px" @if($is_like) disabled @endif>{{ __('main.stories.btn_like') }}</button>
            <object class="heart_history mr-2 ml-4" type="image/svg+xml" data="/img/icon/heart(orange).svg"></object>
            <span style="font-size: 17px">{{ $history->likes->count() }}</span>
        </form>
    </div>
    <div class="col-12 col-md-7 d-md-flex  text-center justify-content-md-end text-md-right  mt-4 mt-md-0 pl-0">

        <div class=" d-flex align-items-center pl-md-4 px-md-0 mr-md-1 px-lg-0 justify-content-center justify-content-md-end my-2 mr-lg-3">
            <span class=" mr-1 my-auto share-text">{{ __('main.history.share_social_networks') }}</span>
        </div>

        <div class="  px-md-0 pl-lg-0">
            <div class="social" data-url="{{ Request::url() }}" data-title="Поделится историей в соц сетях">
                <div id="share">
                    <a class="push mr-1 btn btn-circle bg-orange btn-social" data-id="fb" >
                        <i class="fa fa-facebook btn-social-icon" aria-hidden="true"></i>
                    </a>
                    <a class="push mr-1 btn btn-circle bg-orange btn-social" data-id="vk">
                        <i class="fa fa-vk btn-social-icon " aria-hidden="true"></i>
                    </a>
                    <a class="push btn btn-circle bg-orange btn-social" data-id="tw">
                        <i class="fa fa-twitter btn-social-icon " aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
@if($history->images != null)
@php
$images = json_decode($history->images, true);
$imagesCount = count($images);
@endphp
@if($imagesCount>3)
<div style="position: relative">
    <div class="swiper-container mt-4">
        <div class="swiper-wrapper">
            @foreach($images as $key => $item)
            <div class="swiper-slide h-100%">
                <img class="img-fluid" src="{{ asset($item) }}" alt="{{ $history->title }}">
            </div>
            @endforeach

        </div>

        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>

@else
<div class="row d-flex mt-3">
    @foreach($images as $key => $item)
    <div class="col-12 px-0 px-sm-2">
        <img class="w-100 mx-0" src="{{ asset($item) }}" alt="{{ $history->title }}">
    </div>
    @endforeach
</div>

@endif
@endif
