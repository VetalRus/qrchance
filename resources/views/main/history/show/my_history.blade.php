<div class="row mt-3">
    <div class="col-sm-8">
        <div class="form-group">
            <textarea class="form-control" name="content" rows="9" placeholder="{{ __('main.stories.content_placeholder') }}" disabled>{{$history->content}}</textarea>
        </div>

        <div class="form-group row justify-content-end">
            <div class="col-sm-5">
                <a type="submit" href="{{ route('main.history.edit', $history->id) }}" class="btn bg-orange  btn-block">{{ __('main.stories.btn_edit') }}</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        @if($history->images != null)
        @php
        $images = json_decode($history->images, true);
        @endphp
		@foreach($images as $key => $item)
        	<img src="{{ asset($item) }}" class="img-fluid mb-2">
		@endforeach
		@else
        <div id="cardUploadPhoto" class="card shadowP" role="button">
            <div class="card-body pt-1 m-4">
                <div class="text-center row justify-content-center">
                    <div class="col-sm-7">
                        <i class="fa fa-download text-color__orange"></i>
                        <p class="mb-0">{{ __('main.stories.btn_upload') }}</p>
                        <p class="text-color__orange mt-3">{{ __('main.stories.photo')}}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
