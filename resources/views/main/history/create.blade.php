@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row pt-5">
        <div class="col-3 col-sm-2  col-lg-1  avatar-history text-center">
            <img class="card-img-top img-fluid img-avatar-history " src="{{ asset($user->photo) }}">
        </div>
        <div class="col-9 img  col-sm-7  col-lg-6 ">
            <div class="row">
                <p class="col-6 col-sm-12 mb-0 px-0">
                    @if($user->city)
                    {{ $user->city->translate()->title }}, {{ $user->country->translate()->title }}
                    @endif
                </p>
            </div>
            <div class="row">
                <h5 class="col-12 mb-0 px-0"><b>{{ $user->name }} {{ $user->second_name }}</b></h5>
            </div>
        </div>
        <div class="d-none d-sm-block col-sm-3  col-lg-5 text-right">
            <p>{{ date('d.m.Y', strtotime(new Date())) }}</p>
        </div>
    </div>
    <form action="{{ route('main.history.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row mt-3">
            <div class="col-12 col-lg-8">
                <div class="form-group">
                    <textarea class="form-control bg-light-grey" name="content" rows="9" placeholder="{{ __('main.stories.content_placeholder') }}"></textarea>
                </div>

                <div class="form-group row justify-content-end">
                    <div class="col-sm-5 bg-light-grey">
                        <button type="submit" class="btn bg-orange  btn-block">{{ __('main.stories.btn_send') }}</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 my-4 my-lg-0">
                <div id="cardUploadPhoto" class="card shadowP  bg-light-grey" role="button" onclick="document.getElementById('historyImages').click();">
                    <div class="card-body pt-1 m-4">
                        <div class="text-center row justify-content-center">
                            <div class="col-sm-7">
                                 <object class="img-message-orange mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/download(orange).svg"></object>
                                <p class="mb-0">{{ __('main.stories.btn_upload') }}</p>
                                <p class="text-color__orange mt-3 mb-0">{{ __('main.stories.photo')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="file" id="historyImages" class="d-none" name="images[]">
                {{-- <input type="file" id="historyImages" class="d-none" name="images[]" multiple> --}}
            </div>

        </div>
    </form>
</div>

<div class="container">
    <div class="row text-center mb-3">
        <div class="col-12">
             <h5 class="mt-5">{{ __('main.stories.prize') }}</h5>
        </div>
    </div>
    <div class="swiper-container mb-5" >
        <div class="swiper-wrapper">
            @php
                $counter = [1,2,3,4]
            @endphp
            @foreach($counter as $i)
                <div class="swiper-slide">
                    <img src="{{ asset('/img/static_history_images/static_history_img'.$i.'.jpg') }}" alt="Couples" style="width: 100%;">
                </div>
            @endforeach

        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
@endsection
