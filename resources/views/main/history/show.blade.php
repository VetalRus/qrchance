@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row pt-5">
        <div class="col-3 col-sm-2 col-lg-1 avatar-history text-center">
            <img class="card-img-top img-fluid img-avatar-history" src="{{ asset($history->user->photo) }}">
        </div>
        <div class="col-9 col-sm-7 col-lg-5 ">
            <div class="row">
                <p class="col-6 col-sm-12 mb-0 px-0">
                    @if($history->user->city)
                    {{ $history->user->city->translate()->title }}, {{ $history->user->country->translate()->title }}
                    @endif
                </p>
                <p class="col-6 d-block d-sm-none text-right mb-0">
                    {{ date('d.m.Y', strtotime($history->created_at)) }}
                </p>
            </div>
            <div class="row">
                <h5 class="col-12 mb-0 px-0"><b>{{ $history->user->name }} {{ $history->user->second_name }}</b></h5>
            </div>
        </div>
        <div class="d-none d-sm-block col-sm-3 col-lg-6 text-right ">
            <p>{{ date('d.m.Y', strtotime($history->created_at)) }}</p>
        </div>
    </div>
	@if(Auth::id() == $history->user_id)
		@include('main.history.show.my_history')
	@else
		@include('main.history.show.history')
	@endif
    <div id="historyRow" class="row mt-3 ">

    </div>
    <div class="col-12 d-flex justify-content-center justify-content-lg-start pb-5 px-0">
        <form id="historyAjaxLoad" action="{{ route('ajax.widget.history') }}" method="POST">
        @csrf
            <input type="hidden" name="offset" value="0">
            <input type="hidden" name="not_show_history_id" value="{{ $history->id }}">
            <button type="submit" class="btn btn-outline-orange show-more py-1  px-4  d-flex align-items-center mt-3 mb-5">
                <object class="img-show-more mr-1" type="image/svg+xml" data="/img/icon/refresh(about)orange.svg" ></object> 
                 {{ __('main.main.index.more') }}
            </button>
        </form>
    </div>
</div>

@endsection
