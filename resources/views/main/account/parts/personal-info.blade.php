<div class="card w-100">
    <div class="card-body ">
        <h5 class="card-title  personal-info-title">{{ __('main.account.personal-info.title_personal_info') }}</h5>
        <div class="dropdown-divider"></div>
        <form method="post" action="{{ route('main.account.update.info') }}">
            @csrf
            <div class="row justify-content-md-center py-3 py-md-5">
                <div class="col-md-10">
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.your_name') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                            <input name="name" type="text" class="form-control" value="{{ old('name') ?? $user->name }}" placeholder="{{ __('main.account.personal-info.your_name_placeholder') }}">
                            @error('name')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_name') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.your_last_name') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                            <input name="second_name" type="text" class="form-control" value="{{ old('second_name') ?? $user->second_name }}" placeholder="{{ __('main.account.personal-info.your_last_placeholder') }}">
                            @error('second_name')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_second_name') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.birthday') }}</label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-3 form_gray_s">
                                    <div class="gray-select">
                                        <input class="form-control dropdown-toggle " type="button" id="selectDateBirthday" value="{{ old('date_birthday') ??  date('j', strtotime($user->birthday))  }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <input name="date_birthday" type="hidden" value="{{ old('date_birthday') ?? date('j', strtotime($user->birthday)) }}">
                                        <ul class="dropdown-menu   col-12 pt-2 border-0 mt-0" aria-labelledby="selectDateBirthday" role="menu">
                                            @for($i = 1; $i <= 31; $i++) <li class="list-country  py-1 text-center p-0">
                                                <input type="hidden" value="{{ $i }}">
                                                <span>{{ $i }}</span>
                                                </li>
                                                @endfor
                                        </ul>
                                    </div>
                                </div>
                                @php $personal_info = [
                                'january',
                                'february',
                                'march',
                                'april',
                                'may',
                                'june',
                                'july',
                                'august',
                                'september',
                                'october',
                                'november',
                                'december',
                                ];
                                @endphp

                                <div class="col-5 form_gray_s px-0">
                                    <div class="gray-select">
                                        <input class="form-control dropdown-toggle " type="button" id="selectMonthBirthday" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <input class="input_id" name="month_birthday" type="hidden" value="{{ old('month_birthday') ??  date('n', strtotime($user->birthday))  }}">
                                        <ul class="dropdown-menu   col-12 pt-2 border-0 mt-0" aria-labelledby="selectMonthBirthday" role="menu">
                                            @for($i = 1; $i < count($personal_info); $i++) <li class="list-country  py-1 text-center px-0">
                                                <input type="hidden" value="{{ $i }}">
                                                <span>{{ __('main.personal-info.month.'.$personal_info[$i -1]) }}</span>
                                                </li>
                                                @endfor
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-4 form_gray_s">
                                    <div class="gray-select">
                                        <input class="form-control dropdown-toggle " type="button" id="selectYearBirthday" value="{{ old('year_birthday') ??  date('Y', strtotime($user->birthday)) }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <input name="year_birthday" type="hidden" value="{{ old('year_birthday') ?? date('Y', strtotime($user->birthday)) }}">
                                        <ul class="dropdown-menu   col-12 pt-2 border-0 mt-0" aria-labelledby="selectYearBirthday" role="menu">
                                            @for($i = \Carbon\Carbon::now()->subYears(18)->format("Y"); $i >= 1960; $i--)
                                            <li class="list-country  py-1 text-center px-0">
                                                <input type="hidden" value="{{ $i }}">
                                                <span>{{ $i }}</span>
                                            </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @error('month_birthday')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_birthday') }}
                            </div>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row dropdown form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.country') }}</label>
                        <div class="col-sm-8 form_gray_s">
                            <div class="gray-select">
                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                <input class="form-control dropdown-toggle " type="button" id="selectCountry" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <input class="input_id" name="country_id" type="hidden" value="{{ old('country_id') ?? $user->country_id }}">
                                <ul class="dropdown-menu col-12 category-list-two border-0 mt-0" aria-labelledby="selectCountry">
                                    @foreach($countries as $item)
                                    <li class="list-country ml-2 py-2 d-inline-flex d-md-flex flex-md-column flex-xl-row px-0" type="button">
                                        <i class="mr-2 flag-icon flag-icon-{{ $item->flag }}"></i>
                                        <input type="hidden" value="{{ $item->id }}">
                                        <span>{{$item->translate()->title}}</span>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                            @error('country_id')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_country_id') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.town') }}</label>
                        <div class="col-sm-8 ">
                            <div class="form_gray_s">
                                <div class="gray-select">
                                    <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                    <input class="form-control dropdown-toggle " type="button" id="selectCity" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <input class="input_id" name="city_id" type="hidden" value="{{ $errors->has('city_id') ? old('city_id') : $user->city_id }}">
                                    <ul class="dropdown-menu col-12 text-center border-0 mt-0" aria-labelledby="selectCity">
                                        @foreach( App\Models\City::getCountryID( old('country_id') ?? $user->country_id ) as $item)
                                        <li class="list-country ml-2 py-2 px-0">
                                            <input type="hidden" value="{{ $item->id }}">
                                            <span>{{ $item->translate()->title }}</span>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @error('city_id')
                                <div class="alert alert-danger mt-1 " role="alert">
                                    {{ __('main.account.personal-info.error_city_id') }}
                                </div>
                                @enderror
                            </div>
                            <img src="{{ asset('img/download.gif') }}" id="selectCityGif" class="d-none" alt="">
                        </div>
                    </div>

                </div>
            </div>

            <h5 class="card-title personal-info-title">{{ __('main.account.personal-info.title_about') }}</h5>
            <div class="dropdown-divider"></div>
            <div class="row justify-content-md-center py-3 py-md-5">
                <div class="col-md-10">
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.have_children') }}</label>
                        <div class="col-sm-8 form_gray_s">
                            <div class="gray-select">
                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                <input class="form-control dropdown-toggle " type="button" id="selectChildren" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <input class="input_id" name="children" type="hidden" value="{{ old('children') ?? $user->children }}">
                                <ul class="dropdown-menu col-12 text-center border-0 mt-0" aria-labelledby="selectChildren">
                                    <li class="list-country ml-2 py-2 px-0">
                                        <input type="hidden" value="1">
                                        <span>{{ __('main.account.personal-info.have_children_not') }}</span>
                                    </li>
                                    <li class="list-country ml-2 py-2 px-0">
                                        <input type="hidden" value="2">
                                        <span>{{ __('main.account.personal-info.have_children_yes') }}</span>
                                    </li>
                                </ul>
                            </div>
                            @error('children')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_children') }}
                            </div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label pr-0 py-2 py-sm-0 py-xl-2">{{ __('main.account.personal-info.smoking_attitude') }}</label>
                        <div class="col-sm-8 form_gray_s">
                            <div class="gray-select">
                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                <input class="form-control dropdown-toggle " type="button" id="selectSmoking" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <input class="input_id" name="smoking_id" type="hidden" value="{{ old('smoking_id') ?? $user->smoking_id }}">
                                <ul class="dropdown-menu col-12 text-center border-0 mt-0" aria-labelledby="selectSmoking">
                                    @foreach($smokings as $item)
                                    <li class="list-country ml-2 py-2 px-0">
                                        <input type="hidden" value="{{ $item->id }}">
                                        <span>{{ __($item->key_translation) }}</span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @error('smoking_id')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_smoking') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label pr-0 small-text py-2 py-sm-0 py-xl-2" style="letter-spacing: normal;">{{ __('main.account.personal-info.alcohol_attitude') }}</label>
                        <div class="col-sm-8 form_gray_s">
                            <div class="gray-select">
                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                <input class="form-control dropdown-toggle " type="button" id="selectAlcohol" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <input class="input_id" name="alcohol_id" type="hidden" value="{{ old('alcohol_id') ?? $user->alcohol_id }}">
                                <ul class="dropdown-menu col-12 text-center border-0 mt-0" aria-labelledby="selectAlcohol">
                                    @foreach($alcohols as $item)
                                    <li class="list-country ml-2 py-2 px-0">
                                        <input type="hidden" value="{{ $item->id }}">
                                        <span>{{ __($item->key_translation) }}</span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @error('alcohol_id')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_alcohol') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.profession') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                            <input type="text" name="profession" class="form-control " value="{{ old('profession') ?? $user->profession }}" placeholder="Профессия пользователя">
                            @error('profession')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_profession') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                </div>
            </div>
            <h5 class="card-title personal-info-title">{{ __('main.account.personal-info.title_contact_info') }}</h5>
            <div class="dropdown-divider"></div>
            <div class="row justify-content-md-center py-3 py-md-5">

                <div class="col-md-10">
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.email') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                            <input type="text" name="email" class="form-control" value="{{ old('email') ?? $user->email }}" placeholder="Электронный адрес">
                            @error('email')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_email') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.phone') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                            @widget('InputPhone', ['phone' => old('phone') ?? $user->phone ])
                            @error('phone')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_phone') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.youtube') }}</label>
                        <div class="col-sm-8 form_gray">
                            <i class="fa fa-youtube inst" aria-hidden="true"></i>
                            <input type="text" class="form-control" name="youtube" value="{{ old('youtube') ?? $user->youtube }}" placeholder="https://www.youtube.com/">
                            @error('youtube')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_youtube') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.instagram') }}</label>
                        <div class="col-sm-8 form_gray">
                            <i class="fa fa-instagram inst" aria-hidden="true"></i>
                            <input type="text" class="form-control" name="instagram" value="{{ old('instagram') ?? $user->instagram }}" placeholder="https://www.instagram.com/">
                            @error('instagram')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_instagram') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.vk') }}</label>
                        <div class="col-sm-8 form_gray">
                            <i class="fa fa-vk vk" title="vk"></i>
                            <input type="text" class="form-control" name="vk" value="{{ old('vk') ?? $user->vk }}" placeholder="https://vk.com/">
                            @error('vk')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_vk') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.facebook') }}</label>
                        <div class="col-sm-8 form_gray">
                            <i class="fa fa-facebook-f faceb" title="facebook"></i>
                            <input type="text" class="form-control" name="facebook" value="{{ old('facebook') ?? $user->facebook }}" placeholder="https://www.facebook.com/">
                            @error('facebook')
                            <div class="alert alert-danger mt-1 " role="alert">
                                {{ __('main.account.personal-info.error_facebook') }}
                            </div>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.twitter') }}</label>
                        <div class="col-sm-8 form_gray">
                            <i class="fa fa-twitter twitt" title="twitter"></i>
                            <input type="text" class="form-control " name="twitter" value="{{ old('twitter') ?? $user->twitter }}" placeholder="https://twitter.com/">
                            @error('twitter')
                            <div class="alert alert-danger mt-1" role="alert">
                                {{ __('main.account.personal-info.error_twitter') }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row justify-content-end">
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-lg bg-orange btn-block mt-4 mt-md-2">{{ __('main.account.personal-info.btn_save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form action="{{route('main.account.cities.get')}}" method="GET" id="personalInfoCitiesForm">
            @csrf
            <input name="current_country_id" type="hidden">
        </form>
        <form action="{{route('ajax.widget.account.inputPhone')}}" method="GET" id="personalInfoInputPhoneForm">
            @csrf
            <input name="current_country_id" type="hidden">

        </form>
    </div>
</div>
