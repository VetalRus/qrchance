<div class="card px-3 py-2 p-sm-2 navsMenuCard d-none d-md-block">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link personal_link @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.index') active @endif  py-2 py-md-1  py-lg-1 mb-md-1 px-md-2 pb-2 pl-lg-3" href="{{ route('main.account.index') }}">
             <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/id-card(orange).svg"></object> <p class="m_link_text my-0 my-lg-1"> {{ __('main.account.sidebar.personal_info') }} </p>
        </a>
        <a class="nav-link personal_link @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.business-card') active @endif  py-2 py-md-0 py-md-1  py-lg-1 mb-md-1 px-md-2 pb-2 pl-lg-3" href="{{ route('main.account.business-card') }}">
            <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/give-card (orange).svg"></object> <p class="m_link_text my-0 my-lg-1"> {{ __('main.account.sidebar.creating_visitcard') }} </p>
        </a>
        <a class="nav-link personal_link @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.messages') active @endif  py-2 py-md-1  py-lg-1  mb-md-1 px-md-2 pb-2 pl-lg-3" href="{{ route('main.account.messages') }}">
            <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/chat(orange).svg"></object> <p class="m_link_text my-0 my-lg-1">{{ __('main.account.sidebar.message') }}</p>
            @if(  Auth::user() &&  Auth::user()->unreadMessages()->count() > 0)
            <div class="float-right">
                <span id="count-1" class="notification">{{  Auth::user()->unreadMessages()->count() }}</span>
            </div>
            @endif
        </a>
        <a class="nav-link personal_link  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.histories') active @endif  py-2 py-md-1  py-lg-1 mb-md-1 px-md-2 pb-2 pl-lg-3" href="{{ route('main.account.histories') }}">
             <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/contract(orange).svg"></object> <p class="m_link_text my-0 my-lg-1"> {{ __('main.account.sidebar.other_stories') }} </p>
        </a>
        <a class="nav-link personal_link @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.payment') active @endif  py-2   py-md-1 px-md-1 pb-2 pl-lg-3" href="#v-pills-settings">
             <object class="menu_avatar size mx-3 ml-md-0 " type="image/svg+xml" data="/img/icon/credit-cards-payment(orange).svg"></object> <p class="m_link_text my-0 my-lg-1"> {{ __('main.account.sidebar.payment') }} </p>
        </a>
        <div class="dropdown-divider  my-md-1 my-lg-2 pb-Много текст"></div>
        <a class="nav-link personal_link @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.security') active @endif  py-2  py-md-0 pb-lg-2  px-md-1 pb-2 pl-lg-3" href="{{ route('main.account.security') }}">
            <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/settings(orange).svg"></object> <p class="m_link_text my-0 my-lg-1"> {{ __('main.account.sidebar.settings') }} </p>
        </a>

         <div class="dropdown-divider  my-md-1 my-lg-2 pb-Много текст d-block d-md-none"></div>
         <a class="nav-link personal_link d-flex
         py-2  py-md-0 pb-lg-2  px-md-1 pb-2 pl-lg-3 d-block d-md-none" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <object class="menu_avatar size mx-3 ml-md-0" type="image/svg+xml" data="/img/icon/logout(orange).svg"></object>
            <p class="m_link_text my-0 my-lg-1">{{ __('Выйти') }}</p>
        </a>
        
        <form id="logout-form d-block d-md-none" action="{{ route('logout') }}" method="POST"
              style="display: none;">
            @csrf
        </form>
       
    </div>
</div>
