<div class="card w-100">
    <div class="card-body">
        <h5 class="card-title font-weight-bold">{{ __('main.account.security.title') }}</h5>
        <div class="dropdown-divider mb-3 mb-sm-5"></div>
        <form method="post" action="{{ route('main.account.update.security') }}">
            @csrf
            <div class="row justify-content-md-center">
                <div class="col-md-10">
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.security.old_password') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/unlock (registr).svg"></object>
                            <input name="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" value="" placeholder="{{ __('main.account.security.password_placeholder') }}">
                            @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.security.new_password') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/unlock (registr).svg"></object>
                            <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" value="" placeholder="{{ __('main.account.security.password_placeholder') }}">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row form-group-text">
                        <label class="col-sm-4 col-form-label">{{ __('main.account.security.repeat_password') }}</label>
                        <div class="col-sm-8 form_gray">
                            <object class="inp_avatar" type="image/svg+xml" data="/img/icon/unlock (registr).svg"></object>
                            <input name="password_confirmation" type="password" class="form-control" value="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8">
                            <a href="" class="text-dark">
                                <h6 class="card-title font-weight-bold">{{ __('main.account.security.delete_account') }}</h6>
                            </a>
                        </div>
                    </div>

                    <div class="form-group row justify-content-center">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-lg bg-orange btn-block ml-md-5">{{ __('main.account.security.save') }}</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>

        <h5 class="card-title mt-5 mt-sm-4 font-weight-bold">{{ __('main.account.security.referral_title') }}</h5>
        <div class="dropdown-divider mb-3 mb-sm-5"></div>

        <div class="row justify-content-md-center">
            <div class="col-md-10">

                <div class="form-group row form_gray form-group-text">
                    <label class="col-sm-4 col-form-label">{{ __('main.account.security.referral_input') }}</label>
                    <div class="col-sm-8 ">
                        <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                        <a class="form-control" href="{{ ($user->referral_token) ? url('?referral=' . $user->referral_token) :  ''  }}" disabled>{{ ($user->referral_token) ? url('?referral=' . $user->referral_token) : __('main.account.security.no_exist') }}</a>
                    </div>
                </div>

                @if(!$user->referral_token)
                <form method="post" action="{{ route('main.account.create.referral') }}">
                    @csrf
                    <div class="form-group row justify-content-center">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-lg bg-orange btn-block ml-md-5 mb-4 mt-2">{{ __('main.account.security.btn_create') }}</button>
                        </div>
                    </div>
                </form>

                @endif

            </div>
        </div>
    </div>
</div>
