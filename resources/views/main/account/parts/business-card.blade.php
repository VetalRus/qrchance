<div class="card w-100 ">
    <div class="card-body">

        <h4 class="card-title font-weight-bold"><b>{{ __('main.account.qr.title') }}</b></h4>
        <div class="dropdown-divider mb-5"></div>
        <form id="formCreateQrCode" method="post" action="{{ route('main.account.create.business-card') }}">
            @csrf
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-sm-12 col-form-label form-group-text">{{ __('main.account.qr.text') }}</label>
                        <div class="col-sm-12 pb-sm-2 form_gray" style="height: 8rem">
                            <!--@todo поставил весь текст в placeholder , отличаеться от макета, там  идет placeholder а потом как будто value поля у меня либо либо другое получаеться -->
                            <textarea name="description" type="text" class="form-control" value="" placeholder="{{ __('main.account.qr.text_example_placeholder') }} {{ __('main.account.qr.text_placeholder') }}" style="height: 100%"> </textarea>
                        </div>

                    </div>
                    <div class="alert alert-danger d-none" role="alert">

                    </div>

                    <div class="form-group row justify-content-center">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-lg bg-orange btn-block">{{ __('main.account.qr.btn_create') }}</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
		<div id="businessCardDownloadPdf">
			@include('widgets.account.business_card_download_pdf')
		</div>
    </div>
</div>
