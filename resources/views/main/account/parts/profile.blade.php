<div class="card mb-3 w-100">
    <div class="row no-gutters">

        <div class="card-img  col-md-4 col-lg-4 avatar bg-gray h-auto w-100">

            <img src="{{ asset( $user->photo  ) }}" class="{{ ($user->photo == '/img/avatars/user.svg') ? "d-block" : 'd-none' }} mx-auto  img-svg  opacity-20 card-img w-50 h-75" id="defaultAvatar">
            <img src="{{ asset( $user->photo ) }}" class=" {{ ($user->photo == '/img/avatars/user.svg') ? "d-none" : 'd-block' }} mx-auto card-img h-100 w-100 " id="avatarImg" style="object-fit: cover;">
            <form action="{{ route('main.account.upload.avatar') }}" id="formUploadAvatar" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" id="uploadAvatar" class="d-none" name="avatar">
                <span id="uploadDefaltAvatarButton" class="uploadAvatarButton {{ ($user->photo == '/img/avatars/user.svg') ? "" : 'd-none' }} avatar-text btn"><i class="fa fa-angle-up d-none " id="fa-angle-up" aria-hidden="true"></i>
                    <span id="button-text mb-5">{{ __('main.profile.btn_upload_photo') }}</span>
                </span>
                <span id="uploadAvatarButton" class="uploadAvatarButton {{ ($user->photo == '/img/avatars/user.svg') ? "d-none" : '' }} avatar__add-image text-white btn bg-dark"><i class="fa fa-angle-up" aria-hidden="true"></i>
                    <span id="button-text">{{ __('main.profile.btn_update_photo') }}</span>
                </span>
            </form>
            <img src="{{ asset('img/download.gif') }}" id="downloadGif" class="d-none" alt="">
        </div>

        <div class="col-md-8 col-lg-8 mt-4 mt-md-0">
            <div class="card-body">
                <div class="mr-auto bd-highlight">
                    <div class="dropdown d-block d-md-none">
                        <button type="button" class="btn btn-status px-4 mb-4" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,10">
                            {{ $user->status->translate()->title }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                        <div class="dropdown-menu " aria-labelledby="dropdownMenuOffset">
                            <a class="dropdown-item " href="#">{{ $user->status->translate()->title }}</a>
                            @foreach($statuses as $item)
                            <a class="dropdown-item text-color__orange" href="#">{{ $item->translate()->title  }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <p class="card-text mb-1">
                    @if($user->city)
                    {{ $user->city->translate()->title }}, {{ $user->country->translate()->title }}
                    <span class="flag-icon flag-icon-{{ $user->country->flag }}"> </span>
                    @endif
                </p>
                <div class=" d-md-flex bd-highlight mb-1 mb-md-0 mb-lg-1">
                    <h3 class="mr-auto bd-highlight">{{ $user->name }} {{ $user->second_name }} </h3>
                    <div class="ml-auto bd-highlight">
                        <div class="dropdown d-none d-md-block">
                            <button type="button" class="btn btn-status px-lg-4 " id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,10">
                                {{ $user->status->translate()->title }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu change-status" aria-labelledby="dropdownMenuOffset" >
                                <a class="dropdown-item " href="#">{{ $user->status->translate()->title }}</a>
                                @foreach($statuses as $item)
                                <a class="dropdown-item change text-color__orange" data-user-status-id="{{$item->id}}" href="#">{{ $item->translate()->title  }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
                <p class="card-text my-md-1 mb-lg-2">{{ __('main.profile.birthday') }}:
                    @if($user->birthday)
                    <span class="font-weight-bold">{{ Date::parse($user->birthday)->format('j F Y г.') }}</span>
                    @endif
                </p>

                <p class="card-text mb-md-1 mb-lg-2">
                    {{ __('main.profile.smoking_attitude') }}:
                    @if($user->smoking_id)
                    <span class="font-weight-bold">
                        {{ __($user->smoking->key_translation) }}
                    </span>
                    @endif
                </p>

                <p class="card-text mb-md-1 mb-lg-2">
                    {{ __('main.profile.alcohol_attitude') }}
                    @if($user->alcohol_id)
                    <span class="font-weight-bold">
                        {{ __($user->alcohol->key_translation) }}
                    </span>
                    @endif
                </p>
                <p class="card-text mb-md-1 mb-lg-2">{{ __('main.profile.profession') }}: <span class="font-weight-bold">{{ $user->profession }}</span></p>
                <p class="card-text mb-md-1  mb-lg-2">{{ __('main.profile.children') }}:
                    <span class="font-weight-bold">
                        @if($user->children == 1)
                        {{ __('main.profile.have_children_yes') }}
                        @else
                        {{ __('main.profile.have_children_not') }}
                        @endif
                    </span>
                </p>
                <div class="float-md-left float-lg-right mb-3">
                    @foreach($social_networks as $item)
                    <a class="mr-1  mr-lg-1 mb-1 btn btn-circle bg-orange btn-social" href="{{$item->url}}">
                        <i class="fa fa-{{$item->icon}}  btn-social-icon" aria-hidden="true"></i>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@include('widgets.account.warning_change_status')
