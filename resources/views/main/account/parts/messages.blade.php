<div class="card w-100 rounded-0 border-0 p-0">
    <div class="card-body pt-0 pb-0 pl-1 pr-1">
        @if(count($messages) > 0 )
        @foreach($messages as $item)

        @if($item->read == 0)
        <form method="POST" action="{{ route('main.account.message-read', $item->id) }}" role="button" class="bg-orange-no-gradient message-unread">
            @csrf
            <div class="row border border-light p-3">
        </form>
        @else
        <div class="row border border-light p-3">
            @endif

            <div class="col-xs-12 col-sm-12 col-md-3">
                <p class="-bold">{{ $item->name }}</p>
                <p class="text-black-50"><i class="fa fa-phone" aria-hidden="true"></i> {{ $item->phone }}</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <p class="card-text">{{ $item->message }}</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <form method="POST" action="{{ route('main.account.message-delete', $item->id) }}" class="deleteMessage text-right">
                    @csrf
                    <button class="text-black-50 border-0 btn-link basket-message @if($item->read == 1) bg-white @else bg-orange-no-gradient @endif">
                        <i class="fa fa-trash-o @if($item->read == 0) text-white @endif" aria-hidden="true"></i>
                    </button>
                </form>

                <p class="small text-right text-black-50">
                    <i class="fa fa-check text-color__orange mr-2  @if($item->read == 0) d-none @endif" aria-hidden="true"></i>
                    {{ date("d.m.Y, H:m", strtotime($item->created_at)) }}</p>
            </div>
        </div>
        @endforeach
        @elseif( count($user->unreadMessages()) <= 0 )
        <h3 class="card-title text-center mt-3">{{ __('main.message.no_messages') }}</h3>
        @endif
	@if( count($user->unreadMessages()) > 0 && !$user->isSearch() )
        <h3 class="card-title text-center mt-3 ">{{ __('main.message.change_status') }}</h3>
	@endif
    </div>
</div>
