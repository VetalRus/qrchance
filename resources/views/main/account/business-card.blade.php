@extends('layouts.main')

@section('content')
     
    <div class="container">

        <div class="row pb-lg-5 pb-0">
            <div class="col-12 d-lg-none d-xl-none my-3 btn-small">
                 @include('main.account.parts.pay')
            </div>
        </div>

        <div class="row">

            <div class="col-md-4 d-none d-md-block pr-md-0 pl-lg-3  ">
                @include('main.account.parts.navs')
            </div>


            <div class="col-md-8 pl-md-0 pl-lg-3">
                @if($user)
                	@include('main.account.parts.profile')
                @endif
            </div>
        </div>

        <div class="row pb-5">

            <div class="col-md-4 pr-lg-0 pl-lg-3 d-none d-lg-block">
                 @include('main.account.parts.pay')
            </div>

            <div class="col-md-12 col-lg-8">
                 @if($user)
                    @include('main.account.parts.business-card')
                @endif
            </div>
            
        </div>

    </div>

@endsection
