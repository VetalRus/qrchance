@extends('layouts.main')

@section('content')

@if($qr_code)
@php $user = $qr_code->user; @endphp
<section>
    <div class="pt-5 bg-light-grey">
        <div class="container">
            <div class="row ">
                 <div class="col-12 d-block d-md-none  col-md-8 col-12 col-lg-6 order-1 order-md-2">
                    <div class="card border-0 bg-light-grey">
                        <div class="card-body pt-0 pb-5 px-0 text-center">
                            <h5 class="card-title font-weight-bolder">{{ __('main.message.title_1') }} {{ $user->name }} {{ __('main.message.title_2') }}</h5>

                            <div class="alert alert-success success-area bg-white" role="alert">
                                <p class="mb-0 text-success"> {{$qr_code->description}}</p>
                                <div class="d-flex justify-content-end mt-4">
                                    <object class="img-message-orange" type="image/svg+xml" data="/img/icon/mail(orange).svg"></object>
                                </div>
                            </div>
                            <div class="text-right d-flex flex-row justify-content-end">
                                <div class="col-6 col-sm-4 px-1">
                                    <btn class="btn bg-orange btn-block p-2" data-toggle="modal" data-target="#modalQRCodeAnswer">{{ __('main.message.answer_yes') }}</btn>

                                </div>
                                <div class="col-6 col-sm-4 px-1">
                                    <btn class="btn btn-secondary btn-block btn-gray p-2"  data-toggle="modal" data-target="#modalQRCodeAnswerThanks">{{ __('main.message.answer_not') }}</btn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="card-img show-img-message col-md-4 col-lg-3 avatar  mb--1 pr-md-0  pr-xl-4 ">
                    <img src="{{ asset( $user->photo ) }}" class=" mx-auto d-block card-img h-100 w-100" style="object-fit: cover;">
                 </div>   
                <div class="d-none d-md-block col-md-8 col-12 col-lg-6 order-1 order-md-2 ">
                    <div class="card border-0 bg-light-grey">
                        <div class="card-body pt-0 px-md-0 ">
                            <h5 class="card-title font-weight-bolder">{{ __('main.message.title_1') }} {{ $user->name }} {{ __('main.message.title_2') }}</h5>

                            <div class="alert alert-success bg-white success-area" role="alert">
                                <p class="mb-0 text-success"> {{$qr_code->description}}</p>
                                <div class="d-flex justify-content-end mt-4">
                                    <object class="img-message-orange" type="image/svg+xml" data="/img/icon/mail(orange).svg"></object>
                                </div>
                            </div>
                            <div class="text-right d-flex flex-row justify-content-end">
                                <div class="col-6 col-sm-4 px-1">
                                    <btn class="btn bg-orange btn-block p-2" data-toggle="modal" data-target="#modalQRCodeAnswer">{{ __('main.message.answer_yes') }}</btn>

                                </div>
                                <div class="col-6 col-sm-4 px-1">
                                    <btn class="btn btn-secondary btn-block btn-gray p-2">{{ __('main.message.answer_not') }}</btn>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- <div class="col-md-3 col-12 order-3">

              </div> -->
            </div>
        </div>
    </div>
    <div class="py-5 mt-5 mt-md-0 bg-white">
        <div class="container">
            <div class="row mt-2">
                <div class="col-md-4 col-12 col-lg-3 order-4 order-md-3 px-md-0 pl-lg-1 pr-lg-0 pl-xl-0  pt-5">
                    <span class="mr-1 d-none d-sm-block text-center mb-3">{{ __('main.message.social')  }}</span>
                    <div class="text-center">

    				@if($user->facebook)
    					<a class="mr-1 mb-1 btn btn-circle btn-social bg-orange" href="{{$user->facebook}}">
                            <i class="fa fa-facebook btn-social-icon" aria-hidden="true"></i>
                        </a>
    				@endif

    				@if($user->vk)
                        <a class="mr-1 mb-1 btn btn-circle bg-orange btn-social" href="{{$user->vk}}">
                            <i class="fa fa-vk btn-social-icon" aria-hidden="true"></i>
                        </a>
    				@endif

    				@if($user->twitter)
                        <a class="mr-1 mb-1 btn btn-circle bg-orange btn-social" href="{{$user->twitter}}">
                            <i class="fa fa-twitter btn-social-icon" aria-hidden="true"></i>
                        </a>
    				@endif

    				@if($user->instagram)
                        <a class="mr-1 mb-1 btn btn-circle bg-orange btn-social" href="{{$user->instagram}}">
                            <i class="fa fa-instagram btn-social-icon" aria-hidden="true"></i>
                        </a>
    				@endif

    				@if($user->youtube)
                        <a class=" mb-1 btn btn-circle bg-orange btn-social" href="{{$user->youtube}}">
                            <i class="fa fa-youtube btn-social-icon" aria-hidden="true"></i>
                        </a>
    				@endif
                    </div>
                </div>
                <div class="col-12 col-md-8 col-lg-4 order-3 order-md-4">
                    <div class="d-flex flex-row justify-content-between justify-content-md-start">
                        <p class=" mb-0 mt-5 mt-sm-0">
                            @if($user->city)
                            {{ $user->city->translate()->title }}, {{ $user->country->translate()->title }}
                            <span class="flag-icon flag-icon-{{ $user->country->flag }}"> </span>
                            @endif
                        </p>
                        <div class="col-6 col-sm-4 col-lg-5 px-0 px-sm-3 ml-0 ">
                            <button class="btn btn-outline-orange btn-sm ml-1 btn-block ">{{ __('main.message.status.in_search_of') }}</button>
                        </div>

                    </div>
                    <div class="col-12 pl-0 pr-lg-0">
                        <h3 class="mt-0 font-weight-normal">{{ $user->name }} {{ $user->second_name }}</h3>
                        @if($user->birthday)
                        <p class="card-text">{{ __('main.message.info.birthday') }} <span class="font-weight-bold">{{ Date::parse($user->birthday)->format('j F Y г.') }}</span></p>
                        @endif

                        @if($user->smoking_id)
                        <p class="card-text">
    						{{ __('main.message.info.smoking_attitude') }}
    					    <span class="font-weight-bold">
    						{{ __($user->smoking->key_translation) }}
                            </span>
    					</p>
                        @endif

                        @if($user->alcohol_id)
                        <p class="card-text">
    						{{ __('main.message.info.alcohol_attitude') }} 
    						<span class="font-weight-bold">
    						{{ __($user->alcohol->key_translation) }}
    						</span>
    					</p>
                        @endif

                        @if($user->profession)
                        <p class="card-text">{{ __('main.message.info.profession') }}<span class="font-weight-bold">{{ $user->profession }}</span></p>
                        @endif

                        @if($user->children)
                        <p class="card-text">{{ __('main.account.personal-info.children') }}: <span class="font-weight-bold">@if($user->children == 1) Да @else
                                    {{ __('main.account.personal-info.have_children_not') }} @endif</span></p>
                        @endif
                    </div>
                </div>
                <!-- <div class="col-1"></div> -->
                <div class="col-sm-12 col-lg-5 order-5 mt-4 ">
                    <div class="card shadow">
                        <div class="col-12">
                            <button type='button' class='close mr-1' aria-label='Close'>
                                <span id="close" aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                        <div class="card-body ">
                            <div class="text-center">
                                 <object class="img-message-orange-big mx-auto mb-1" type="image/svg+xml" data="../img/icon/question(orange).svg"></object>             
                                <p class="card-text mb-0 mx-md-5">{{ __('main.message.notification') }}</p>
                                <div class="dropdown-divider w-50 my-3 mx-auto"></div>
                               
                                <p class="card-text mx-md-5">
                                    {{ __('main.message.info_1') }} {{ $user->name }} {{  __('main.message.info_2') }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>


<section class="py-5 " style="background-color:white;">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 py-4 text-center">
                <h2 class="about-title">{{ __('main.main.index.why_this_better') }}</h2>
            </div>
        </div>
        <div class="row py-4 py-sm-5">
            <div class="col-12 col-sm-4 col-lg-4 text-center">
                <img class="text-center" src="{{ asset('/img/icon/couple.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.impression') }}</p>
                <span>{{ __('main.main.index.impression_content') }}</span>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 text-center my-5 my-sm-0">
                <img class="text-center" src="{{ asset('/img/icon/smartphone.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.acquaintance') }}</p>
                <span>{{ __('main.main.index.acquaintance_content') }}</span>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 text-center">
                <img class="text-center" src="{{ asset('/img/icon/parents.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.people') }}</p>
                <span>{{ __('main.main.index.people_content') }}</span>
            </div>
        </div>
    </div>
</section>
<section class="py-5" style="background-color: #f5f5f5">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 py-5 text-center">
                <h2 class="about-title">{{ __('main.main.index.stories') }}</h2>
            </div>
        </div>
        @widget('History')
    </div>
</section>

@include('widgets.message.qrcode-answer')
@include('widgets.message.qrcode-answer-thanks')

@else
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h2>{{__( 'main.message.status.end_search')}}</h2>
        </div>
    </div>
</div>
@endif

@endsection
