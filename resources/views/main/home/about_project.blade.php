@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row mt-5">
        <div class="col-12 col-lg-6 px-0">
            <h2 class="col-md-auto  about-title ">{{ __('main.about_project.title') }}</h2>
            <p class="col-md-auto mt-3  ">{{ __('main.about_project.description') }}</p>
        </div>
        <div class="col-12 col-lg-6 d-none d-sm-block">
             <div class=" embed-responsive embed-responsive-16by9 w-100 " >
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
            </div>
        </div>
       
    </div>
</div>
<div class=" d-block d-sm-none">
    <div class=" embed-responsive embed-responsive-16by9 w-100 " >
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
    </div>
</div>
<section class="mt-5 py-4" style="background: url({{ asset('/img/fon.jpg')}}); background-position-x: 80%; background-size: cover;">
    <div class="container w-100 h-100">
        <div class="row py-5">
            <div class="col-12 col-sm-12 col-lg-6 col-xl-6 py-0 ">
                <div class=" col-sm-auto text-center text-light text-lg-left px-0">
                    <h2 class="font-weight-bold about-title mt-0 ">{{ __('main.layouts.main.faq') }}</h2>
                </div>

                <div class="col-lg-auto  col-lx-auto mt-5  p-0 accordion md-accordion" id="faqAccordion" role="tablist" aria-multiselectable="true">

                    @include('widgets.faq_loop')

                </div>
                <div class="col-auto px-0 col-sm-auto pt-3 pt-sm-4 px-lg-0 d-flex justify-content-center pt-lg-4 pt-xl-5">
                    <form class=" ml-lg-auto " id="faqAjaxLoad" action="{{ route('ajax.widget.faq') }}" method="POST">
                        @csrf
                        <input type="hidden" name="offset" value="{{ $offset }}" class="" >
                        <button type="submit" class="btn btn-outline-light  btn-block show-more py-1 mx-0 px-4 px-sm-4 d-flex align-items-center"  >
                             <object class="img-show-more img-black mr-1" type="image/svg+xml" data="/img/icon/refresh(about).svg" ></object>
                            {{ __('main.layouts.main.more') }}
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-12 col-sm-12 pt-5 pt-sm-5 col-lg-6 col-xl-6 justify-content-center pt-lg-1">
                <div class="card pt-2 pb-1 px-0">
                   <div class="d-flex justify-content-center mt-4">
                      <object class="about_avatar" type="image/svg+xml" data="/img/icon/support(about).svg"></object>
                   </div>
                    <div class="card-title text-center mt-2 mb-0 ">
                        <h3 class="font-weight-bold">{{ __('main.about_project.feedback') }}</h3>
                    </div>
                    @if(Session::has('message'))
                    <p class="text-center color-text font-mb15 m-0 p-0">
                        {{Session::get('message')}}
                    </p>
                    @endif
                    <div class="card-body pt-2">
                        <form method="POST" action="{{ route('mails.feedback') }}">
                            @csrf

                            <div class="form-group row mb-4">
                                <label for="name" class="col-12  col-form-label ml-1 ml-md-5  font-mr15 py-0">{{ __('main.about_project.name') }}:</label>
                                <div class="input-group col-md-10 m-auto form_gray">
                                     <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                                    <input id="name" type="text" class="form-control input-group-text text-left" name="name" required autocomplete="name" autofocus placeholder="{{ __('main.about_project.name_placeholder') }}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="email" class=" col-12 col-form-label ml-1 ml-md-5 font-mr15 py-0">{{ __('main.about_project.email') }}:</label>

                                <div class="input-group col-md-10 m-auto form_gray">
                                    <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                                    <input id="email" type="email" class="input-group-text text-left form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" placeholder="{{ __('main.about_project.email_placeholder') }}">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="tel" class="col-12 col-form-label ml-1 ml-md-5 font-mr15 py-0">{{ __('main.about_project.phone') }}:</label>

                                <div class="input-group col-md-10 m-auto form_gray">
                                    <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                                    <input id="tel" type="text" class="input-group-text text-left form-control" name="phone" required autocomplete="tel" placeholder="{{ __('main.about_project.phone_placeholder') }}" pattern="[0-9]{10}">

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="massage" class="col-12 col-form-label ml-1 ml-md-5 font-mr15 py-0">{{ __('main.about_project.write_message') }}:</label>

                                <div class="col-md-10 m-auto form_gray" style="height: 100px">
                                    <textarea id="text" type="text" class="input-group-text text-left form-control h-100" name="text" rows="4" style="resize: none; height: 100%"> </textarea>
                                </div>
                            </div>
                            <div class="form-group row justify-content-center mt-4">
                                <div class="col-sm-6 py-2">
                                    <button type="submit" class="btn btn-lg bg-orange btn-block py-2">{{ __('main.about_project.send_message') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
