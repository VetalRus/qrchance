@extends('layouts.main')

@section('content')
<section class="fon" style="background-image: url({{ asset('/img/fon-1.jpg') }});background-position-x: 80%; background-size: cover;">
    <div class="container">
        <div class="row py-5 background_main mr-md-0">
            <div class="col-12 col-sm-12 col-md-10 col-lg-6 py-0  pr-0">
                <div class="text-light py-3  py-sm-0 pl-0 pt-lg-5">
                    <h1 class="d-none d-sm-block mt-4 main-title">{{ __('main.main.index.title') }}</h1>
                    <h3 class="d-block d-sm-none  main-mini-title mb-0">{{ __('main.main.index.title') }}</h3>
                    <div class="d-flex">
                        <h1 class="mt-4 main-title mr-4 d-none d-sm-block">
                            {{ __('main.main.index.title1') }}
                        </h1>
                         <h3 class="mt-4 main-mini-title mr-3 mr-sm-4 d-block d-sm-none">
                            {{ __('main.main.index.title1') }}
                        </h3>
                         <h1 class="mt-4  main-title-logo d-none d-sm-block">&ensp;<span style="background: url({{ asset('/img/QRcode.png') }}); background-size: cover" class="text-dark py-2 px-1">QR</span>Chance.com</h1>
                          <h3 class="mt-4  main-mini-title-logo d-block d-sm-none">&ensp;<span style="background: url({{ asset('/img/QRcode.png') }}); background-size: cover" class="text-dark py-2 px-1">QR</span>Chance.com</h3>
                    </div>
                   
                </div>
                <div class="text-light mt-4 pl-0">
                    <div class="d-flex align-items-center mb-4">
                        <i class="header-text-circle mr-3">
                            <p class="text-circle first">1</p>
                        </i>
                        <p class="main-group-text my-auto
                        d-none d-sm-block">
                             {{ __('main.main.index.item1') }}
                        </p>
                        <p class="form-group-text text-light my-auto d-block d-sm-none">
                             {{ __('main.main.index.item1') }}
                        </p>
                    </div>
                    <div class="d-flex align-items-center mb-4">
                         <i class="header-text-circle  mr-3">
                            <p class="text-circle">2</p>
                        </i>
                        <p class="mt-4 main-group-text my-auto d-none d-sm-block">
                            {{ __('main.main.index.item2') }}
                        </p>
                        <p class="mt-4 form-group-text text-light my-auto d-block d-sm-none">
                            {{ __('main.main.index.item2') }}
                        </p>  
                    </div>
                    <div class="d-flex align-items-center ">
                        <i class="header-text-circle  mr-3">
                            <p class="text-circle">3</p>
                        </i>
                        <p class="mt-4 main-group-text my-auto d-none d-sm-block">
                            {{ __('main.main.index.item3') }}
                        </p>
                         <p class="mt-4 form-group-text text-light my-auto d-block d-sm-none">
                            {{ __('main.main.index.item3') }}
                        </p>
                    </div>
                </div>
                <div class="d-lg-flex align-items-center pb-lg-5 mb-lg-5">
                     <div class="col-11 col-sm-8  col-xl-6 col-md-5  mt-5 mr-lg-4 pl-0 float-left mb-5 ">
                        <a class="btn btn-lg btn-light btn-block py-2" @if(Auth::user()) href="{{ route('main.account.business-card') }}" @else data-toggle="modal"  data-target="#modalRegister"  @endif style="max-width: 270px">
                            {{ __('main.main.index.play') }}

                            <i class="fa fa-angle-right fa-angle-right-header ml-2" aria-hidden="true"></i>
                        </a>
                    </div>
                     <div class="col-12 col-md-12 col-xl-6 float-left py-5 py-md-5 p-lg-0 pl-0">
                          <div class="btn form-group-text text-light  pl-0 mr-4 mr-xl-3" >{{ __('main.main.index.how_it_works') }}</div>
                          <div class="pulse btn" data-toggle="modal" data-target="#modalVideoPresentation">
                            <a href="">
                                <i class="fa fa-play" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 " style="background-color:white;">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 py-4 text-center">
                <h2 class="about-title">{{ __('main.main.index.why_this_better') }}</h2>
            </div>
        </div>
        <div class="row py-4 py-sm-5">
            <div class="col-12 col-sm-4 col-lg-4 text-center">
                <img class="text-center" src="{{ asset('/img/icon/couple.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.impression') }}</p>
                <span>{{ __('main.main.index.impression_content') }}</span>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 text-center my-5 my-sm-0">
                <img class="text-center" src="{{ asset('/img/icon/smartphone.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.acquaintance') }}</p>
                <span>{{ __('main.main.index.acquaintance_content') }}</span>
            </div>
            <div class="col-12 col-sm-4 col-lg-4 text-center">
                <img class="text-center" src="{{ asset('/img/icon/parents.png') }}" alt="">
                <p class="mt-4 main-text-title">{{ __('main.main.index.people') }}</p>
                <span>{{ __('main.main.index.people_content') }}</span>
            </div>
        </div>
    </div>
</section>
<section class="py-5" style="background-color: #f5f5f5">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 py-5 text-center">
                <h2 class="about-title">{{ __('main.main.index.stories') }}</h2>
            </div>
        </div>
        @widget('History')
    </div>
</section>

@include('widgets.home.video')

@endsection
