@if(\Illuminate\Support\Facades\Route::currentRouteName() != 'main.home.about_project')
@widget('Faq')
@endif

<section class=" mt-5 mb-5 d-flex align-items-center" style="background: url({{ asset('/img/baner.jpg')}}); background-position-x: 60%; background-size: cover; height: 22rem">
    <div class="container  ">
        <div >
            <h2 class="col-12 col-sm-auto  text-md-center text-sm-center text-lg-left text-light font-weight-bold" style="font-size: 2.5rem; letter-spacing: 0.1em;">
                Рекламный баннер
            </h2>
        </div>
    </div>
</section>   
<section class="text-light bg-dark pt-5" style="font-size: 1rem">
    <div class="container">
        <div class="row ">
            <div class="col-lg-3 order-2 order-lg-1 d-none d-lg-block d-lg-none pt-lg-4" >
                <a class="text-light" href="">
                    <p>{{ __('main.layouts.main.partner') }}</p>
                </a>
                <a class="text-light" href="{{ route('main.home.about_project') }}">
                    <p>{{ __('main.layouts.main.about') }}</p>
                </a>
                <a class="text-light" data-toggle="modal" data-target="#modalFeedback" href="#">
                    <p>{{ __('main.layouts.main.feedback') }}</p>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 offset-lg-1  order-md-1  order-lg-1 pl-0 pt-lg-4">
                <h6 class="text-color__orange pl-4" style="font-size: 1.1rem">{{ __('main.layouts.main.language') }}</h6>
                @php
                $url = explode("/", Request::path(), 2);
                if( App::getLocale() == $url['0'] && isset($url['1']) ) {
                $path = $url['1'] ;
                } elseif( App::getLocale() != Request::path() ) {
                $path = Request::path();
                } else{
                $path = "" ;
                }
                @endphp
                <ul class="category-list-two pl-4">
                    @foreach($languages as $value)
                    <li><a class="text-light" href="{{ url( $value->iso . '/' . $path ) }}">{{$value->title}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-md-4 col-sm-6 col-lg-4 offset-sm-2  offset-lg-0  order-3 pt-4 pl-4 " >
                <div class=" order-2 d-block d-lg-none mb-5">
                    <a class="text-light" href="" >
                        <p >{{ __('main.layouts.main.partner') }}</p>
                    </a>
                    <a class="text-light" href="">
                        <p>{{ __('main.layouts.main.about') }}</p>
                    </a>
                    <a class="text-light" href="">
                        <p>{{ __('main.layouts.main.feedback') }}</p>
                    </a>
                </div>
                <div class="offset-lg-4 ">
                    <p>{{ __('main.layouts.main.support') }}</p>
                    <div class="d-flex align-items-center mb-3 mt-2">
                        <object class="footer_avatar mr-3" type="image/svg+xml" data="/img/icon/phone-call(footer).svg" style="width: 1.3rem; height: 1.3rem"></object>
                        <span>+380660000164</span>&ensp; 
                    </div>
                    
                    <div class="d-flex align-items-center">
                        <object class="footer_avatar mr-3" type="image/svg+xml" data="/img/icon/email(footer).svg" style="width: 1.3rem; height: 1.3rem"></object>&ensp;
                        <span>admin@gmail.com</span>&ensp;
                    </div>
                     
                </div>
                <div class="col col-sm-12 col-lg-12 d-none d-lg-block  mt-5 offset-lg-4 p-0">
                    <p>{{ __('main.layouts.main.follow') }}</p>
                    @foreach($social_networks as $item)
                    <a class=" btn btn-circle bg-orange-no-gradient btn-social  mr-1" href="{{$item->url}}" >
                        <i class="fa fa-{{$item->icon}} btn-social-icon" aria-hidden="true"></i>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
         <div class="row justify-content-sm-center pl-4 pl-sm-0 pt-4">
            <div class="col-auto d-lg-none d-xl-none  mt-4 offset-lg-4 p-0 text-sm-center">
                <p>{{ __('main.layouts.main.follow') }}</p>
                @foreach($social_networks as $item)
                <a class=" btn btn-circle bg-orange-no-gradient btn-social mr-1" href="{{$item->url}}" >
                    <i class="fa fa-{{$item->icon}} btn-social-icon" aria-hidden="true"></i>
                </a>
                @endforeach
            </div>
            
        </div>
        <div class="row justify-content-center">
            <div class="col-auto mt-5 mb-4 pl-0 pt-4 text-center">
                <h1 class="logo-header text-wight" style="font-size: 2rem "><span class="text-color__orange">QR</span>Chance.com</h1>
                <p>{{ __('main.layouts.main.made') }}</p>
            </div>
        </div>
    </div>
</section>
