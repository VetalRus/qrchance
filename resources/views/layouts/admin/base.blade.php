<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/icon/favicon.ico">

    <title>Admin - @yield('title')</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('/admin_assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">
    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/bootstrap-extend.css') }}">
    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/master_style.css') }}">
    <!-- Fab Admin skins -->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/skins/_all-skins.css') }}">
    <!-- Vector CSS -->
    <link href="{{ asset('/admin_assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <!-- Morris charts -->
    <link rel="stylesheet" href="{{ asset('/admin_assets/vendor_components/morris.js/morris.css') }}">


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        @include('admin.parts.header')
        @include('admin.parts.sitebar')

        <div class="content-wrapper">
            @yield('content')
        </div>

        @include('admin.parts.footer')

    </div>
	
@include('widgets.error_message')

    <!-- jQuery 3 -->
    <script src="{{ asset('/admin_assets/vendor_components/jquery/dist/jquery.js') }}"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('/admin_assets/vendor_components/jquery-ui/jquery-ui.js') }}"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);

    </script>

    <!-- popper -->
    <script src="{{ asset('/admin_assets/vendor_components/popper/dist/popper.min.js') }}"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{ asset('/admin_assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>

    <!-- ChartJS -->
    <script src="{{ asset('/admin_assets/vendor_components/chart.js-master/Chart.min.js') }}"></script>

    <!-- Slimscroll -->
    <script src="{{ asset('/admin_assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('/admin_assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

    <!-- peity -->
    <script src="{{ asset('/admin_assets/vendor_components/jquery.peity/jquery.peity.js') }}"></script>


    <!-- Morris.js charts -->
    <script src="{{ asset('/admin_assets/vendor_components/raphael/raphael.min.js') }}"></script>

    <script src="{{ asset('/admin_assets/vendor_components/morris.js/morris.min.js') }}"></script>


    <!-- Fab Admin App -->
    <script src="{{ asset('/admin_assets/js/template.js') }}"></script>


    <!-- Fab Admin dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('/admin_assets/js/dashboard.js') }}"></script>

    <!-- Fab Admin for demo purposes -->
    <script src="{{ asset('/admin_assets/js/demo.js') }}"></script>


    <!-- Vector map JavaScript -->
    <script src="{{ asset('/admin_assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('/admin_assets/vendor_components/jvectormap/lib2/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('/admin_assets/vendor_components/jvectormap/lib2/jquery-jvectormap-us-aea-en.js') }}"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
