<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/icon/favicon.ico">

    <title>Admin - @yield('title')</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('/admin_assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/master_style.css') }}">

    <!-- Fab Admin skins -->
    <link rel="stylesheet" href="{{ asset('/admin_assets/css/skins/_all-skins.css') }}">

</head>

<body class="hold-transition login-page">
    <div class="h-full" style="background-color: #28373e; box-shadow: 1px 0px 20px rgba(0,0,0,0.08);">


        @yield('content')


    </div>

    <!-- jQuery 3 -->
    <script src="{{ asset('/admin_assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- popper -->
    <script src="{{ asset('/admin_assets/vendor_components/popper/dist/popper.min.js') }}"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{ asset('/admin_assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>
