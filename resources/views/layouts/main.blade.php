<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
    <title>{{ config('app.name', 'QRchance') }}</title>
	
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/vendor/inputmask.min.js') }}" defer></script>
	 {!! NoCaptcha::renderJs() !!}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
   
    
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm header ">
        <div class="container">
            <a class="navbar-brand logo-header d-none d-sm-block" href="{{ route('main.home.index') }}">
                <span class="text-color__orange">QR</span>Chance.com
            </a>
            <a class="navbar-brand logo-header-small d-block d-sm-none" href="{{ route('main.home.index') }}">
                <span class="text-color__orange">QR</span>Chance.com
            </a>
             <ul class="navbar-nav mr-auto ml-md-4 ">
                    @widget('HeaderLanguages')
             </ul> 

              
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto py-auto align-items-center">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item align-items-center py-auto">
                            <a class="nav-link d-flex align-items-center" data-toggle="modal" data-target="#modalLogin" href="#">
                                 <img class="img-header-orange mr-2 mb-md-2 mt-2 mt-md-0" src="/img/icon/user(orange).svg">
                                 <p class="d-none d-md-block my-auto form-group-text left_line">{{ __('main.login') }}</p></a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item py-auto align-items-center ">
                                <a class="nav-link align-items-center" data-toggle="modal" data-target="#modalRegister" href="#">
                                 <p class="d-none d-md-block my-auto form-group-text">{{ __('main.register') }}</p></a>
                            </li>
                        @endif
                    @else
                        <div class="navbar-nav mr-md-4 mr-2">
                            <li class="nav-item">
                                @widget('NewMessages')
                            </li>
                        </div>
                        <div class="d-none d-md-block">
                            @widget('DropdownHeaderUserMenu') 
                        </div>
                    @endguest
                </ul>
          
        </div>
    </nav>
{{-- 
    <section class="offer_pay bg-orange pb-3 py-md-3 rounded">
        <div class="container">
           <div class="row align-items-center">
               <div class="col-12 pr-0 py-0">
                   <button type='button' class='close-right-btn  border-0   ml-auto d-block d-md-none' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
               </div>     
               <div class="col-12 col-md-9 col-lg-9 text-center text-lg-right  py-1 ">
                    <p class="form-group-text  text-light my-auto ">
                        {{ __('main.pay.for_activation') }} <span class="mr-1 font-weight-bold" style="font-size: 18px">100</span>грн.
                    </p>
                  
               </div>
               <div class="col-12 col-md-3 col-lg-3   d-flex justify-content-center justify-conten-md-between align-items-center">
                    <button type="submit" class="btn  btn-light btn-sm  btn-block  w-50 px-0 py-lg-2 mx-auto mr-md-auto my-4 my-md-0" style="max-width: 160px">
                      <p class="card-text">
                          {{ __('main.pay.pay') }}
                      </p>
                    </button>
                     <button type='button' class='close-right-btn  border-0   ml-md-auto d-none d-md-block' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
               </div>
           </div> 
        </div>
    </section> --}}
     
    <nav class="col-12 navbar navbar-expand-md navbar-light bg-light-grey shadow-sm header d-block d-md-none py-0">
        <div class="container">
           
             <!-- Right Side Of Navbar -->
            <ul class="col-12 navbar-nav mr-sm-auto d-block d-md-none px-0">
                <!-- Authentication Links -->
               @guest
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link" data-toggle="modal" data-target="#modalLogin" href="#">
                         <img class="img-header-orange" src="/img/icon/user(orange).svg">
                             {{ __('Login') }}
                         </a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" data-toggle="modal" data-target="#modalRegister" href="#"> {{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <div class=" bg-light-grey">
                      @widget('DropdownHeaderUserMenu') 
                    </div>
                     
                @endguest
            </ul>
          </div>
        </nav>
        <div class="row">
            <div class="col-12 d-block d-md-none">
                @include('main.account.parts.navs')
            </div>
        </div>

    <main @if(\Illuminate\Support\Facades\Route::currentRouteName() !='main.home.index' ) class="" @endif>
        @yield('content')
    </main>

</div>

@include('layouts.footer')

@include('widgets.auth.register')
@include('widgets.auth.login')
@include('widgets.auth.password-recovery')
@include('widgets.feedback')
@include('widgets.error_message')
@include('widgets.success_message')

</body>
</html>
