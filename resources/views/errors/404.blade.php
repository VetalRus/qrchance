<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

</head>

<div class="container">

    <div class="row">

        <div class="col bgerr text-center">
            <img class="col-xl-6 col-md-8" src="{{ asset('/img/errors.png') }}" alt="">
            <p class="err404">Ошибка</p>
            <p class="err">404</p>
        </div>

    </div>
    <div class="row justify-content-center text-center">
        <h2>Такой страницы не существует.</h2>
    </div>
    <div class="row justify-content-center">
        <a href="{{ route('main.home.index') }}" class="btn btn-sm col-3 bg-orange">На главную</a>
    </div>

</div>


