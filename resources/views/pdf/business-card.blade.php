<meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>

<style type="text/css" media="all">
@font-face {
  font-family: "Gotham Pro";
  src: url("../fonts/business-card/GothamPro.ttf") format("ttf"),
       url("../fonts/business-card/GothamPro-Medium.ttf") format("ttf");
}

.page-break {
    page-break-after: always;
}

.bcard-side1,
.bcard-side2 {
    width: 90mm;
    height: 50mm;
    border: 1px solid #c9c9c9;
}

/* Side 1 */

.bcard-side1__wrapper {
    height: 35mm;
}

.bcard-side1__title {
    color: #171716;
    font-family: 'Gotham Pro', Arial, sans-serif;
    font-size: 3.5mm;
    line-height: 6mm;
    font-weight: 400;
    font-style: normal;
    text-align: center;
    padding-top: 9mm;
    background-image: url('../img/bcard/qr-code-bg.png');
    background-repeat: no-repeat;
    background-position: 46.35% 102.5%;
    background-size: 75px 75px;
}

.bcard-side1__sep {
    width: 33%;
    height: 1px;
    margin: 0 auto;
    margin-top: 3mm;
    background-color: #e3602c;
}

.bcard-side1__title span {
    color: #e3602c;
}

.bcard-side1__footer {
    height: 14mm;
    background-color: #e3602c;
    margin-top: 2px;
}

.bcard-side1__footer p {
    color: white;
    font-family: 'Gotham Pro Medium', Arial, sans-serif;
    font-size: 6px;
    text-align: center;
    margin-top: 4mm;
}

.bcard-side1__footer p span {
    color: #582410;
}

/* Side 2 */

.bcard-side2__wrapper {
    width: 100%;
    height: 35mm;
    background-image: url({{$qrcode_img}});
    background-repeat: no-repeat;
    background-position: 10% 60%;
    background-size: 55mm 55mm;
}

.bcard-side2__title {
    color: #171716;
    font-family: 'Gotham Pro', Arial, sans-serif;
    font-size: 10px;
    margin-left: 33.3%;
    margin-top: 14%;
}

.bcard-side2__title span {
    color: #e3602c;
}

.bcard-side2-notify {
    height: 9mm;
    margin-left: 25mm;
}

.bcard-side2-notify__msg {
    color: #727272;
    font-family: 'Gotham Pro', Arial, sans-serif;
    font-size: 7px;
    background-color: #f2f1f1;
    width: 35mm;
    height: 7mm;
    padding-top: 2mm;
    padding-left: 2mm;
    margin-top: -9mm;
    margin-left: 16mm;
}

.bcard-side2-notify__img {
    width: 10mm;
    height: 9mm;
    background-color: #f2f1f1;
    background-image: url('../img/bcard/email.png');
    background-repeat: no-repeat;
    background-position: center center;
    background-size: 53px 48px;
    margin-left: 5mm;
    margin-top: -0.8mm;
}

.bcard-side2__footer {
    height: 14mm;
    background-color: #e3602c;
    margin-top: 2px;
    background-image: url('../img/bcard/qr-code-footer.png');
    background-repeat: no-repeat;
    background-position: 31.5mm 2.35mm;
    background-size: 57px 57px;
}

.bcard-side2__footer p {
    color: white;
    font-family: 'Gotham Pro', Arial, sans-serif;
    font-size: 13px;
    margin-top: 2.8mm;
    text-align: center;
}

.bcard-side2__footer p span {
    color: #171716;
    display: block;
}

</style>

<section class="bcard-side1">
    <div class="bcard-side1__wrapper">
        <h1 class="bcard-side1__title">
            {{ __('main.account.qr.slogan2') }}<br> 
			{{ __('main.account.qr.slogan3') }} QR<span> Chance.com</span>
        </h1>
        <div class="bcard-side1__sep"></div>
    </div>
    <div class="bcard-side1__footer">
        <p>{{ __('main.account.qr.slogan4') }}<span>{{ __('main.account.qr.company') }}</span></p>
    </div>
</section>

<div class="page-break"></div>

<section class="bcard-side2">
    <div class="bcard-side2__wrapper">
        <p class="bcard-side2__title">{{ __('main.account.qr.slogan5') }}<span>QR</span>{{ __('main.account.qr.slogan6') }}</p>
        <div class="bcard-side2-notify">
            <div class="bcard-side2-notify__img"></div>
            <p class="bcard-side2-notify__msg">{{ __('main.account.qr.slogan7') }}</p>
        </div>
    </div>
    <div class="bcard-side2__footer">
        <p><span>QR</span>Chance.com</p>
    </div>
</section>
