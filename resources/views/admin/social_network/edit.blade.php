@extends('layouts.admin.base')

@section('title', 'Edit Social Network')

@section('content')
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Edit Social Network: {{ $network->title }}</strong></h4>
                    </div>
                    <form action="{{route('admin.social_network.update', $network->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="col-sm-12 mt-4 row  justify-content-center">
                            <div class="form-group row col-sm-10">
                                <h4>Наименование</h4>
                                <input type="text" class="form-control" name="title" value="{{ $network->title }}">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>URL</h4>
                                <input type="text" name="url" class="form-control" value="{{ $network->url }}">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>Иконка</h4>
                                <input type="text" name="icon" class="form-control" value="{{ $network->icon }}">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>Order</h4>
                                <input type="text" name="order" class="form-control" value="{{ $network->order }}">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Редактировать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
