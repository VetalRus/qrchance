@extends('layouts.admin.base')

@section('title', 'Social Networks')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header col-md-10 row mx-0 justify-content-md-between">
                        <div>
                            <h4 class="box-title "><strong>Social Networks</strong></h4>
                        </div>
                        <div>
                            <a href="{{route('admin.social_network.create')}}" class="btn btn-primary pull-right">
                                <i class="fa fa-plus-square-o"></i> Создать соц. сеть
                            </a>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>URL</th>
                        <th>Иконка</th>
                        <th>Order</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        @forelse ($networks as $keyItem)
                            <tr>
                                <td><span>{{ $keyItem->id }}</span></td>
                                <td><span>{{ $keyItem->title }}</span></td>
                                <td><span>{{ $keyItem->url }}</span></td>
                                <td><h2 class="text-color__orange"><i class="fa fa-{{ $keyItem->icon }} btn-social-icon ml-1" aria-hidden="true"></i></h2></td>
                                <td><span>{{ $keyItem->order }}</span></td>
                                <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.social_network.edit', $keyItem->id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.social_network.destroy', $keyItem->id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">
                                    <h2>Данные отсутствуют</h2>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
