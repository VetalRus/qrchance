@extends('layouts.admin.base')

@section('title', 'Create New Social Network')

@section('content')
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Create New Social Network</strong></h4>
                    </div>
                    <form action="{{route('admin.social_network.store')}}" method="post">
                        @csrf
                        <div class="col-sm-12 mt-4 row  justify-content-center">
                            <div class="form-group row col-sm-10">
                                <h4>Наименование</h4>
                                <input type="text" class="form-control" name="title">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>URL</h4>
                                <input type="text" name="url" class="form-control" value="{{ $url }}">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>Иконка</h4>
                                <input type="text" name="icon" class="form-control">
                            </div>
                            <div class="form-group row col-sm-10">
                                <h4>Order</h4>
                                <input type="text" name="order" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Создать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
