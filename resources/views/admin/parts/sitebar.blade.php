<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="user-profile treeview">
                <a href="">
                    <img src="{{ asset(Auth::user()->photo) }}" alt="user">
                    <span>{{ Auth::user()->name }}</span>
                </a>
            </li>
            <li class="header nav-small-cap">PERSONAL</li>

            <li class="">
                <a href="{{ route("admin.main.dashboard") }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.user.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.faq_lang.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Faq</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.history.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Histories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.country_lang.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Countries</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.alcohol.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Alcohol</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.smoking.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Smoking</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

            <li class="">
                <a href="{{ route("admin.printout_place.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Printout Places</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

			<li class="">
                <a href="{{ route("admin.social_network.index") }}">
                    <i class="fa fa-dashboard"></i> <span>Social Networks</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>

			<li class="">
                <a href="{{ route("admin.user_status_lang.index") }}">
                    <i class="fa fa-dashboard"></i> <span>User Statuses</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
            </li>
        </ul>
    </section>
</aside>
