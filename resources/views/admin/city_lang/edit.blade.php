@extends('layouts.admin.base')

@section('title', 'Edit City-Lang')

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="box-header row mx-0 flex">
                    <h4 class="box-title col-12 col-sm flex align-items-center">
                        <strong>
                            Country -
                            <a href="{{ route('admin.country_lang.edit', $city->country->id) }}">
                                {{ $city->country->translateAdmin()->title }}
                            </a>, Edit City - {{$city->translateAdmin()->title}}
                        </strong>
                    </h4>
                </div>
                <div class="row justify-content-sm-center">
                    <div class="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                @foreach($languages as $key => $item)
                                <a class="nav-item nav-link @if($key == 0) show active @endif" data-toggle="tab" href="#id-{{ $item->id }}" role="tab">
                                    <span class="flag-icon flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                                </a>
                                @endforeach
                            </div>
                        </nav>
                    </div>
                    <div class="col-sm-10 mt-4">
                        <div class="tab-content" id="nav-tabContent">
                            @php
                            $citylangs = $city->getLangAll();
                            @endphp
                            @foreach ($citylangs as $item)
                            <div class="tab-pane fade @if($item->lang_id == 1) show active @endif" id="id-{{ $item->lang_id }}">
                                <form action="{{route('admin.city_lang.update', $item->city_id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="lang_id" value="{{ $item->lang_id }}">
                                    <input type="hidden" name="flagName" id="flagName">
                                    {{-- <div class="row">--}}
                                    <div class="form-group">
                                        <h4>Страна</h4>
                                        <select class="form-control" name="country_id" id="selectedCountryId" style="width: 100%">
                                            @foreach($countries as $country)
                                            <option class="col-md-6 list-unstyled pt-2 pb-2" value="{{ $country->id }}" @if($country->id == $city->country_id) selected @endif>
                                                <p>{{ $country->country->title }}</p>
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <h4>Город</h4>
                                        <input type="text" class="form-control" name="title" id="cityTitle" value="{{ $city->city->title }}">
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-lg btn-primary btn-block" id="bttnEdit">Редактировать {{ $item->language->title }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
