@extends('layouts.admin.base')

@section('title', 'Create City-Lang')

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="box-header row mx-0 flex">
                    <h4 class="box-title col-12 col-sm flex align-items-center">
                        <strong> Country -
                            <a href="{{ route('admin.country_lang.edit', $country->id) }}">
                                {{ $country->translateAdmin()->title }}
                            </a>, Create City
                        </strong>
                    </h4>
                </div>
                <div class="row justify-content-sm-center">
                    <div class="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                @foreach($languages as $key => $item)
                                <a class="nav-item nav-link @if($key == 0) show active @endif" data-toggle="tab" href="#id-{{ $item->id }}" role="tab">
                                    <span class="flag-icon flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                                </a>
                                @endforeach
                            </div>
                        </nav>
                    </div>
                    <div class="col-sm-10 mt-4">
                        <div class="tab-content" id="nav-tabContent">
                            @for($i = 1; $i <= $languages->count() ; $i++)

                                <div class="tab-pane fade @if($i == 1) show active @endif" id="id-{{ $i }}">
                                    <form action="{{route('admin.city_lang.store')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="lang_id" value="{{ $i }}">
                                        <div class="form-group row">
                                            <select class="form-control" name="country_id" id="selectedCountryId">
                                                <option value="">Выберите страну</option>
                                                @foreach($countries as $item)
                                                @if( old('country_id') == $item->id || $country->id == $item->id)
													<option class="col-md-6 list-unstyled pt-2 pb-2" selected value="{{ $item->id }}">
														<p>{{ $item->country->title }}</p>
													</option>
                                                @else
													<option class="col-md-6 list-unstyled pt-2 pb-2" value="{{ $item->id }}">
														<p>{{ $item->country->title }}</p>
													</option>
                                                @endif

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                            <input name="title" class="form-control" placeholder="Введите город">
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-sm-5">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block">Создать</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
