@extends('layouts.admin.base')

@section('title', 'City-Lang')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="box-header row mx-0 flex">
                    <div class="col-sm-3 col-sm flex align-items-center">
                        <h4 class="box-title ">
							<strong>Country - 
								<a href="{{ route('admin.country_lang.edit', $country->id) }}">
									{{ $country->translateAdmin()->title }}
								</a>, CITY
							</strong>
						</h4>
                    </div>
                    <div class="col-sm-9 justify-content-end">
                        <div class="row flex">
                            <div class="col-sm-10">
                                <form action="{{route('admin.city_lang.search')}}" method="post">
                                    @csrf
                                    <h3 class="box-title ">Поиск</h3>
                                    <select name="search_name" id="">
                                        <option value="city" @if( $search_name ?? old('search_name')=="content" ) selected @endif>Город</option>
                                    </select>
                                    <input type="text" name="search_text" value="{{  $search_text ?? old('search_text')  }}" placeholder="Введите текст поиска">
                                    <button type="submit" class="btn btn-primary btn-sm">Искать</button>
                                </form>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{route( 'admin.city_lang.create', $country->id )}}" class="btn btn-primary pull-right">
                                    <i class="fa fa-plus-square-o"></i> Создать город
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Город</th>
                        <th>Язык</th>
                        <th>Создано</th>
                        <th>Действие</th>
                    </thead>
                    <tbody>
                        @forelse ($cities as $city)
                        @php
                        $city_language = $city->translateAdmin();

                        @endphp
                        <tr>
                            <td><span>{{ $city_language->city_id }}</span></td>
                            <td><span>{{ $city_language->title }}</span></td>
                            <td><span>{{ $city_language->language->title }}</span></td>
                            <td><span>{{ $city->created_at }}</span></td>
                            <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.city_lang.edit', $city_language->city_id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.city_lang.destroy', $city_language->city_id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3" class="text-center">
                                <h2>Данные отсутствуют</h2>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                @if($cities->lastPage() > 1)
                <div class="row col-12 justify-content-between py-4 pr-0">
                    <div class="align-self-center ml-4 font-size-14 mb-10">
                        <span>Страницы</span>
                    </div>
                    {{ $cities->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
