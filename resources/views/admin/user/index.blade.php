@extends('layouts.admin.base')

@section('title', 'User')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <div class="col-sm-3 col-sm flex align-items-center">
                            <h4 class="box-title "><strong>USERS</strong></h4>
                        </div>
                        <div class="col-sm-9 justify-content-end">
                            <div class="row flex">
                                <div class="col-sm-10">
                                    <form action="{{route('admin.user.search')}}" method="post">
                                        @csrf
                                        <h3  class="box-title ">Поиск</h3>
                                        <select name="search_name" id="">
                                            <option value="id" @if( $search_name ?? old('search_name') == "id"  ) selected @endif >ID пользователя</option>
                                            <option value="name" @if( $search_name ?? old('search_name') == "name"  ) selected @endif >Имя пользователя</option>
                                            <option value="sex" @if(  $search_name ?? old('search_name')  == "sex") selected @endif >Пол пользователя</option>
                                        </select>
                                        <input type="text" name="search_text" value="{{  $search_text ?? old('search_text')  }}" placeholder="Введите текст поиска">
                                        <button type="submit" class="btn btn-primary btn-sm">Искать</button>
                                        @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </form>
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{route('admin.user.create')}}" class="btn btn-primary pull-right">
                                        <i class="fa fa-plus-square-o"></i> Создать пользователя
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped" >
                        <thead>
                        <th>ID</th>
                        <th>User</th>
                        <th>isAdmin</th>
                        <th>IsReadyGame</th>
                        <th>Email</th>
                        <th>Фото</th>
                        <th>Birthday</th>
                        <th>Страна</th>
                        <th>Город</th>
                        <th>Дети</th>
                        <th>Алкоголь</th>
                        <th>Курение</th>
                        <th>Sex</th>
                        <th>Create_at</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        @forelse ($users as $user)
{{--                            @php--}}
{{--                                $images = json_decode($->images, true);--}}
{{--                            @endphp--}}
                            <tr>
                                <td><span>{{ $user->id }}</span></td>
                                <td><span>{{ $user->name }} {{ $user->second_name }}</span></td>
                                <td><span class="h3">@if($user->isAdmin)<i class="fa fa-check text-color__orange" aria-hidden="true"></i>@endif</span></td>
                                <td><span class="h3">@if($user->isReadyGame)<i class="fa fa-check text-color__orange" aria-hidden="true"></i>@endif</span></td>
                                <td><span>{{ $user->email }}</span></td>
                                <td>
                                    <div style="width:30px; height:30px;">
                                        <img class="img-fluid"  src="{{ asset($user->photo) }}" alt="{{ $user->name }}">
                                    </div>
                                </td>
                                <td><span>{{ $user->birthday }}</span></td>
                                <td><span>@if($user->country_id) {{ $user->country->translate()->title }} @endif</span></td>
                                <td><span>@if($user->city_id) {{ $user->city->translate()->title }} @endif</span></td>

                                <td><span class="h3">@if($user->children == 2) <i class="fa fa-check text-color__orange" aria-hidden="true"></i>@endif</span></td>
                                <td><span>@if($user->alcohol_id) {{ $user->alcohol->title }} @endif</span></td>
                                <td><span>@if($user->smoking_id) {{ $user->smoking->title }} @endif</span></td>
                                <td><span class="h1">
                                        @if($user->sex)
                                            <i class="fa fa-venus text-color__orange" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-mars x2 text-color__orange" aria-hidden="true"></i>
                                        @endif</span></td>
                                <td><span>{{ $user->created_at }}</span></td>
                                <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.user.edit', $user->id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.user.destroy', $user->id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">
                                    <h2>Данные отсутствуют</h2>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    @if($users->lastPage() > 1)
                        <div class="row col-12 justify-content-between py-4 pr-0">
                            <div class="align-self-center ml-4 font-size-14 mb-10">
                                <span>Страницы</span>
                            </div>
                            {{ $users->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
