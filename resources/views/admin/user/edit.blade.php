@extends('layouts.admin.base')

@section('title', 'User')

@section('content')
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Edit USER</strong></h4>
                    </div>
                    <div class="row justify-content-sm-center">
                        <div class="container mt-4">
                            <form action="{{ route('admin.user.update', $user->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        @if($user->photo != null)
                                            <img class="d-block  img-fluid" src="{{ asset($user->photo) }}" alt="{{ $user->name }}" id="userImageEdit">
                                        @endif
                                        <div id="cardUploadPhoto" class="card shadowP" role="button" onclick="document.getElementById('historyImages').click();">
                                            <div class="card-body pt-1 m-4">
                                                <div class="text-center row justify-content-center">
                                                    <div class="col-sm-7">
                                                        <i class="fa fa-download text-color__orange"></i>
                                                        <p class="mb-0">{{ __('main.stories.btn_upload') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="file" id="historyImages" class="d-none" name="images">
                                    </div>
                                    <input type="file" id="historyImages" class="d-none" name="image">
                                </div>
                                <div class="row justify-content-md-center">
                                    <div class="col-md-10">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Имя пользователя</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                                                <input name="name" type="text" class="form-control" value="{{ old('name') ?? $user->name }}" placeholder="{{ __('main.account.personal-info.your_name_placeholder') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Фамилия пользователя</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                                                <input name="second_name" type="text" class="form-control" value="{{ old('second_name') ?? $user->second_name }}" placeholder="{{ __('main.account.personal-info.your_last_placeholder') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.birthday') }}</label>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-3 form_gray_s">
                                                        <select class="form-control" name="date_birthday">
                                                            @for($i = 1; $i <= 31; $i++)
                                                                @if( old('date_birthday')==$i || date('d', strtotime($user->birthday)) == $i)
                                                                    <option value="{{ $i }}" selected>{{ $i }}</option>
                                                                @else
                                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                                @endif
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-5 form_gray_s">
                                                        <select class="form-control" name="month_birthday">
                                                            @for($i = 1; $i <= 12; $i++)
                                                                @if( old('month_birthday')==$i || date('m', strtotime($user->birthday)) == $i)
                                                                    <option value="{{ $i }}" selected>{{ $i }}</option>
                                                                @else
                                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                                @endif
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4 form_gray_s">
                                                        <select class="form-control" name="year_birthday">
                                                            @for($i = \Carbon\Carbon::now()->subYears(18)->format("Y"); $i >= 1960; $i--)
                                                                @if( old('year_birthday') == $i || date('Y', strtotime($user->birthday)) == $i)
                                                                    <option value="{{ $i }}" selected>{{ $i }}</option>
                                                                @else
                                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                                @endif
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row dropdown">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.country') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <input class="form-control dropdown-toggle " type="button" id="selectCountry" value="@if($user->country_id) {{ $user->country->translate()->title }} @endif" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">

                                                <input name="country_id" type="hidden" value="{{ $user->country_id }}">

                                                <ul class="dropdown-menu col-sm-11 category-list-two " aria-labelledby="selectCountry">
                                                    @foreach($countries as $item)
                                                        <li class="list-country mx-2 py-2" type="button">
                                                            <i class="mr-2 flag-icon flag-icon-{{ $item->flag }}"></i>
                                                            <span data-country-id="{{ $item->id }}">{{$item->translate()->title}}</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.town') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control" name="city_id" style="height: 45px;">
                                                    <option value=""></option>

                                                    @foreach($cities as $item)
                                                        @if( old('city_id') == $item->id || $user->city_id == $item->id)
                                                            <option value="{{ $item->id }}" selected>{{ $item->translate()->title }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ $item->translate()->title }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">isAdmin</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="isAdmin" style="height: 45px;">
                                                    <option value=""></option>
                                                    <option value="0" @if( !old('isAdmin') or !$user->isAdmin) selected @endif>Нет</option>
                                                    <option value="1" @if( old('isAdmin') or $user->isAdmin) selected @endif>Да</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">isReadyGame</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="isReadyGame" style="height: 45px;">
                                                    <option value=""></option>
                                                    <option value="0" @if( !old('isReadyGame') or !$user->isReadyGame) selected @endif>Нет</option>
                                                    <option value="1" @if( old('isReadyGame') or $user->isReadyGame) selected @endif>Да</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Пол пользователя</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="sex" style="height: 45px;">
                                                    <option value=""></option>
                                                    <option value="0" @if( !old('sex') or !$user->sex) selected @endif>Мужчина</option>
                                                    <option value="1" @if( old('sex') or $user->sex) selected @endif>Женщина</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Referral ID</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <input name="referral_id" type="text" class="form-control" value="{{ old('referral_id') ?? $user->referral_id }}" placeholder="{{ __('main.account.personal-info.your_last_placeholder') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <h5 class="card-title">Информация о пользователе</h5>
                                <div class="dropdown-divider"></div>
                                <div class="row justify-content-md-center">

                                    <div class="col-md-10">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.have_children') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="children" style="height: 45px;">
                                                    <option value=""></option>
                                                    <option value="1" @if( old('children') == 1 or $user->children == 1 or $user->children == null) selected @endif>{{ __('main.account.personal-info.have_children_not') }}</option>
                                                    <option value="2" @if( old('children') == 2 or $user->children == 2) selected @endif>{{ __('main.account.personal-info.have_children_yes') }}</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.smoking_attitude') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="smoking_id" style="height: 45px;">
                                                    <option value=""></option>

                                                    @foreach($smokings as $item)
                                                        @if( old('smoking_id') == $item->id || $user->smoking_id == $item->id)
                                                            <option value="{{ $item->id }}" selected>{{ __($item->key_translation) }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ __($item->key_translation) }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.alcohol_attitude') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="alcohol_id" style="height: 45px;">
                                                    <option value=""></option>

                                                    @foreach($alcohols as $item)
                                                        @if( old('alcohol_id') == $item->id || $user->alcohol_id == $item->id)
                                                            <option value="{{ $item->id }}" selected>{{ __($item->key_translation) }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ __($item->key_translation) }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.profession') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                                                <input type="text" name="profession" class="form-control " value="{{ old('profession') ?? $user->profession }}" placeholder="Профессия пользователя">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <h5 class="card-title">{{ __('main.account.personal-info.title_contact_info') }}</h5>
                                <div class="dropdown-divider"></div>
                                <div class="row justify-content-md-center">

                                    <div class="col-md-10">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.email') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                                                <input type="text" name="email" class="form-control" value="{{ old('email') ?? $user->email }}" placeholder="Электронный адрес">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.phone') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                                                <input type="text" name="phone" class="form-control" value="{{ old('phone') ?? $user->phone }}" placeholder="+38(___)___-__-__">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.youtube') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <i class="fa fa-youtube inst" aria-hidden="true"></i>
                                                <input type="text" class="form-control" name="youtube" value="{{ old('youtube') ?? $user->youtube }}" placeholder="https://www.youtube.com/">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.instagram') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <i class="fa fa-instagram inst" aria-hidden="true"></i>
                                                <input type="text" class="form-control" name="instagram" value="{{ old('instagram') ?? $user->instagram }}" placeholder="https://www.instagram.com/">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.vk') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <i class="fa fa-vk vk" title="vk"></i>
                                                <input type="text" class="form-control" name="vk" value="{{ old('vk') ?? $user->vk }}" placeholder="https://vk.com/">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.facebook') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <i class="fa fa-facebook-f faceb" title="facebook"></i>
                                                <input type="text" class="form-control" name="facebook" value="{{ old('vk') ?? $user->vk }}" placeholder="https://www.facebook.com/">
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.twitter') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <i class="fa fa-twitter twitt" title="twitter"></i>
                                                <input type="text" class="form-control " name="twitter" value="{{ old('twitter') ?? $user->twitter }}" placeholder="https://twitter.com/">
                                            </div>
                                        </div>

                                        <div class="form-group row justify-content-end">
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-lg bg-orange btn-block">{{ __('main.account.personal-info.btn_save') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
