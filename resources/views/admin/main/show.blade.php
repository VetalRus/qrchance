<h1>@lang('admin.hello')</h1>

@php
$url = explode("/", Request::path(), 2);


if( App::getLocale() == $url['0'] && isset($url['1']) ) {
	$iso = $url['1'] ;
} elseif( App::getLocale() != Request::path() ) {
	$iso = Request::path();
} else{
	$iso = "" ;
}
@endphp
<li><a class="text-light" href="{{ url( 'en/' . $iso ) }}">English</a></li>
