@extends('layouts.admin.auth')

@section('title', 'Login')

@section('content')

<div class="container h-p100">
    @if ($errors->any())
    <div class="col-12">
        <div class="alert alert-danger alert-dismissible alert-timeout col-12 col-md-12 col-lg-12 col-xl-12">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-close"></i> Ошибка!</h4>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </div>
    </div>
    @endif

    <div class="row align-items-center justify-content-md-center h-p100">

        <div class="col-lg-4 col-md-8 col-12">
            <div class="login-box">
                <div class="login-box-body">
                    <h3 class="text-center"><span class="text-color__orange">QR</span>Chance.com</h3>
                    <p class="login-box-msg">Login to admin panel</p>
                    <form method="POST" action="{{ route('admin.login') }}">
                        @csrf
                        <div class="form-group has-feedback">
                            <input type="email" name="email" class="form-control rounded {{ $errors->has('email') ? 'error' : '' }}" placeholder="Email" value="{{ old("email") }}">
                            <span class="ion ion-email form-control-feedback"></span>
                            @if($errors->has('email'))
                            <div class="help-block">
                                <ul role="alert">
                                    <li>{{$errors->first('email')}}</li>
                                </ul>
                            </div>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="password" class="form-control rounded {{ $errors->has('password') ? 'error' : '' }}" placeholder="Password">
                            <span class="ion ion-locked form-control-feedback"></span>
                            @if($errors->has('password'))
                            <div class="help-block">
                                <ul role="alert">
                                    <li>{{$errors->first('password')}}</li>
                                </ul>
                            </div>
                            @endif
                        </div>
                        <div class="row">

                            <!-- /.col -->
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-info btn-block margin-top-10">SIGN IN</button>
                            </div>
                            <!-- /.col -->
                        </div>
                        </form>

                </div>
                <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->

        </div>
    </div>
</div>
@endsection
