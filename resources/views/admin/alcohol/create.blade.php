@extends('layouts.admin.base')

@section('title', 'Create Alcohol')

@section('content')
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Create Alcohol Attitude Key</strong></h4>
                    </div>
                    <form action="{{route('admin.alcohol.store')}}" method="post">
                        @csrf
                        <div class="col-sm-12 mt-4 row">
                            <div class="form-group col-md-4">
                                <h4>Наименование</h4>
                                <input type="title" class="form-control" name="title">
                            </div>
                            <div class="form-group col-md-8">
                                <h4>Ключ</h4>
                                <input name="alcoholKey" class="form-control" value="{{ $key }}">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Создать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
