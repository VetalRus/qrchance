@extends('layouts.admin.base')

@section('title', 'Create Faq-Lang')

@section('content')
<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="box-header row mx-0 flex">
                    <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Create FAQ</strong></h4>
                </div>
                <div class="row justify-content-sm-center">
                    <div class="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                @foreach($languages as $key => $item)
                                <a class="nav-item nav-link @if($key == 0) show active @endif" data-toggle="tab" href="#id-{{ $item->id }}" role="tab">
                                    <span class="flag-icon flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                                </a>
                                @endforeach
                            </div>
                        </nav>
                    </div>
                    <div class="col-sm-10 mt-4">
                        <div class="tab-content" id="nav-tabContent">
                            @for($i = 1; $i <= $languages->count() ; $i++)

                                <div class="tab-pane fade @if($i == 1) show active @endif" id="id-{{ $i }}">
                                    <form action="{{route('admin.faq_lang.store')}}" method="post">
                                        @csrf
										<input type="hidden" name="lang_id" value="{{ $i }}">
                                        <div class="form-group row">
                                            <h4>Вопрос:</h4>
                                            <input type="text" class="form-control" name="title">
                                        </div>

                                        <div class="form-group row">
                                            <h4>Ответ:</h4>
                                            <textarea name="content" class="form-control w-100"></textarea>
                                        </div>

                                        <div class="form-group row justify-content-center">
                                            <div class="col-sm-5">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block">Создать</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
