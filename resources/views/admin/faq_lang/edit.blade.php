@extends('layouts.admin.base')

@section('title', 'Edit Faq-Lang')

@section('content')

<section class="content">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="box box-body">
                <div class="box-header row mx-0 flex">
                    <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Edit FAQ № - {{$faq->id}}</strong></h4>
                </div>
                <div class="row justify-content-sm-center">
                    <div class="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                @foreach($languages as $key => $item)
                                <a class="nav-item nav-link @if($key == 0) show active @endif" data-toggle="tab" href="#id-{{ $item->id }}" role="tab">
                                    <span class="flag-icon flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                                </a>
                                @endforeach
                            </div>
                        </nav>
                    </div>
                    <div class="col-sm-10 mt-4">
                        <div class="tab-content" id="nav-tabContent">
                            @php
                            $faqlangs = $faq->getLangAll();
                            @endphp
                            @foreach ($faqlangs as $item)
                            <div class="tab-pane fade @if($item->lang_id == 1) show active @endif" id="id-{{ $item->lang_id }}">
                                <form action="{{route('admin.faq_lang.update', $item->faq_id)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="lang_id" value="{{ $item->lang_id }}">
                                    <input type="hidden" name="_method" value="put">
                                    <div class="form-group row">
                                        <h4>Вопрос:</h4>
                                        <input type="text" class="form-control" name="title" value="{{ $item->title }}">
                                    </div>

                                    <div class="form-group row">
                                        <h4>Ответ:</h4>
                                        <textarea name="content" class="form-control w-100">{{ $item->content }}</textarea>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-lg btn-primary btn-block">Редактировать {{ $item->language->title }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
