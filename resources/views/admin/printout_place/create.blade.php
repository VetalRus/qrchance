@extends('layouts.admin.base')

@section('title', 'Printout Place')

@section('content')
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Create Printout Place</strong></h4>
                    </div>
                    <div class="row justify-content-sm-center">
                        <div class="container mt-4">
                            <form action="{{ route('admin.printout_place.store') }}" method="post">
                                @csrf

                                <div class="row justify-content-md-center">
                                    <div class="col-md-10">
                                        <div class="form-group row dropdown">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.country') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <input class="form-control dropdown-toggle " type="button" id="selectCountry"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">

                                                <input name="country_id" type="hidden" value="">

                                                <ul class="dropdown-menu col-sm-11 category-list-two " aria-labelledby="selectCountry">
                                                    @foreach($countries as $item)
                                                        <li class="list-country mx-2 py-2" type="button">
                                                            <i class="mr-2 flag-icon flag-icon-{{ $item->flag }}"></i>
                                                            <span data-country-id="{{ $item->id }}">{{$item->translate()->title}}</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.town') }}</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control" name="city_id" style="height: 45px;">
                                                    <option value=""></option>

                                                    @foreach($cities as $item)
                                                        <option value="{{ $item->id }}">{{ $item->translate()->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Адрес</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/home (registr).svg"></object>
                                                <input type="text" name="address" class="form-control " value="" placeholder="Адрес">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.email') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                                                <input type="text" name="email" class="form-control" value="" placeholder="Электронный адрес">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">{{ __('main.account.personal-info.phone') }}</label>
                                            <div class="col-sm-8 form_gray">
                                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                                                <input type="text" name="phone" class="form-control" value="" placeholder="+38(___)___-__-__">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Verified</label>
                                            <div class="col-sm-8 form_gray_s">
                                                <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
                                                <select class="form-control " name="verified" style="height: 45px;">
                                                    <option value=""></option>
                                                    <option value="0">Нет</option>
                                                    <option value="1">Да</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row justify-content-end">
                                    <div class="col-sm-8">
                                        <button type="submit" class="btn btn-lg bg-orange btn-block">{{ __('main.account.personal-info.btn_save') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
