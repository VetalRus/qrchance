@extends('layouts.admin.base')

@section('title', 'Printout Places')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row justify-content-md-between">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header col-md-10 row mx-0 justify-content-md-between">
                        <div class="">
                            <h4 class="box-title "><strong>Printout Places</strong></h4>
                        </div>
                        <div class="col-sm-8">
                            <div class="row flex justify-content-between">
                                <div class="col-sm-10">
                                    <form action="{{route('admin.printout_place.search')}}" method="post">
                                        @csrf
                                        <h3  class="box-title ">Поиск</h3>
                                        <select name="search_name" id="">
                                            <option value="id" @if( $search_name ?? old('search_name') == "id"  ) selected @endif >ID</option>
                                            <option value="country_id" @if( $search_name ?? old('search_name') == "country_id"  ) selected @endif >Страна</option>
                                            <option value="verified" @if(  $search_name ?? old('search_name')  == "verified") selected @endif >Verified</option>
                                        </select>
                                        <input type="text" name="search_text" value="{{  $search_text ?? old('search_text')  }}" placeholder="Введите текст поиска">
                                        <button type="submit" class="btn btn-primary btn-sm">Искать</button>
                                        @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </form>
                                </div>
                                <div>
                                    <a href="{{ route('admin.printout_place.create') }}" class="btn btn-primary pull-right">
                                        <i class="fa fa-plus-square-o"></i> Создать визитку
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <th>ID</th>
                        <th>Страна</th>
                        <th>Город</th>
                        <th>Адрес</th>
                        <th>Телефон</th>
                        <th>Электронная почта</th>
                        <th>Верифицирован</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        @forelse ($places as $item)
                            <tr>
                                <td><span>{{ $item->id }}</span></td>
                                <td><span>{{ $item->country->translate()->title }}</span></td>
                                <td><span>{{ $item->city->translate()->title }}</span></td>
                                <td><span>{{ $item->address }}</span></td>
                                <td><span>{{ $item->phone }}</span></td>
                                <td><span>{{ $item->email }}</span></td>
                                <td><span><span class="h3">@if($item->verified)<i class="fa fa-check text-color__orange" aria-hidden="true"></i>@endif</span></span></td>
                                <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.printout_place.edit', $item->id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.printout_place.destroy', $item->id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">
                                    <h2>Данные отсутствуют</h2>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    @if($places->lastPage() > 1)
                        <div class="row col-12 justify-content-between py-4 pr-0">
                            <div class="align-self-center ml-4 font-size-14 mb-10">
                                <span>Страницы</span>
                            </div>
                            {{ $places->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
