@extends('layouts.admin.base')

@section('title', 'History')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <div class="col-sm-3 col-sm flex align-items-center">
                            <h4 class="box-title "><strong>HISTORIES</strong></h4>
                        </div>
                        <div class="col-sm-9 justify-content-end">
                            <div class="row flex">
                                <div class="col-sm-10">
                                    <form action="{{route('admin.history.search')}}" method="post">
                                        @csrf
                                        <h3  class="box-title ">Поиск</h3>
                                        <select name="search_name" id="">
                                            <option value="user_id" @if( $search_name ?? old('search_name') == "user_id"  ) selected @endif >ID пользователя</option>
                                            <option value="name" @if( $search_name ?? old('search_name') == "name"  ) selected @endif >Имя пользователя</option>
                                            <option value="content" @if(  $search_name ?? old('search_name')  == "content") selected @endif >Содержание истории</option>
                                        </select>
                                        <input type="text" name="search_text" value="{{  $search_text ?? old('search_text')  }}" placeholder="Введите текст поиска">
                                        <button type="submit" class="btn btn-primary btn-sm">Искать</button>
                                        @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </form>
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{route('admin.history.create')}}" class="btn btn-primary pull-right">
                                        <i class="fa fa-plus-square-o"></i> Создать историю
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <th>ID</th>
                        <th>User</th>
                        <th>Содержание</th>
                        <th>Фото</th>
                        <th>Язык</th>
                        <th>Create_at</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        @forelse ($histories as $history)
                        @php
                            $images = json_decode($history->images, true);
                        @endphp
                            <tr>
                                <td><span>{{ $history->id }}</span></td>
                                <td><span>{{ $history->user->name }}</span></td>
                                <td><span>{{ $history->content }}</span></td>
                                <td>
                                    <div style="width:100px; height:100px;">
{{--                                        можно вместо class="img-fluid" поставить style="width: 100%; height:100%;"--}}
{{--                                        тогда картинки будут одинаковой высоты--}}
                                        <img class="img-fluid"  src="{{ asset($images[0]) }}" alt="{{ $history->title }}">
                                    </div>
                                </td>
                                <td><span>{{ $history->language->title }}</span></td>
                                <td><span>{{ $history->created_at }}</span></td>
                                <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.history.edit', $history->id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.history.destroy', $history->id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">
                                    <h2>Данные отсутствуют</h2>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    @if($histories->lastPage() > 1)
                        <div class="row col-12 justify-content-between py-4 pr-0">
                            <div class="align-self-center ml-4 font-size-14 mb-10">
                                <span>Страницы</span>
                            </div>
                            {{ $histories->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
