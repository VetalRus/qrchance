@extends('layouts.admin.base')

@section('title', 'History')

@section('content')
@php $user = $history->user; @endphp
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header row mx-0 flex">
                        <h4 class="box-title col-12 col-sm flex align-items-center"><strong>Edit HISTORY № - {{$history->id}}</strong></h4>
                    </div>
                    <div class="row justify-content-sm-center">
                        <div class="container mt-4">
                            <div class="row">
                                <div class="d-inline col-lg-1 col-sm-6">
                                    <img class="img-thumbnail" src="{{ asset($user->photo) }}">
                                </div>
                                <div class="d-inline col-lg-5 col-sm-6">
                                    <div class="row">
                                        <p class="col-12 mb-0">
                                            @if($user->city)
                                                {{ $user->city->translate()->title }}, {{ $user->country->translate()->title }}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="row">
                                        <h5 class="col-12 mb-0"><b>{{ $user->name }} {{ $user->second_name }}</b></h5>
                                    </div>
                                </div>

                                <div class="col-lg-6 text-right col-sm-12">
                                    <p>{{ date('d.m.Y', strtotime(new Date())) }}</p>
                                </div>

                                <div id="adminHistoryDropdown" class="dropdown col-md-8">
                                    <button class="btn btn-block btn-lg btn-outline-orange dropdown-toggle" type="button" data-toggle="dropdown" id="dropdownLanguage" aria-haspopup="true" aria-expanded="false">
                                        {{ $history->language->title }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                        <ul class="row">
                                            @foreach($languages as $key => $item)
                                                <li class="col-md-6 list-unstyled pt-2 pb-2 dropdown-item dropdown-events">
                                                    <a class="text-dark text-nowrap lang-click" data-id="{{ $item->id }}" data-title="{{ $item->title }}">
                                                        <span class="flag-icon flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <form action="{{ route('admin.history.update', $history->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="lang_id" id="inputValue">
                                <div class="row mt-3">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="content" rows="9">{{ $history->content }}</textarea>
                                        </div>

                                        <div class="form-group row justify-content-end">
                                            <div class="col-sm-5">
                                                <button type="submit" class="btn bg-orange  btn-block">{{ __('main.stories.btn_send') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="cardUploadPhoto" class="card shadowP" role="button" onclick="document.getElementById('historyImages').click();">
                                            <div class="card-body pt-1 m-4">
                                                <div class="text-center row justify-content-center">
                                                    <div class="col-sm-7">
                                                        <i class="fa fa-download text-color__orange"></i>
                                                        <p class="mb-0">{{ __('main.stories.btn_upload') }}</p>
                                                        <p class="text-color__orange mt-3">{{ __('main.stories.photo')}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="file" id="historyImages" class="d-none" name="images[]" multiple>
                                    </div>

                                </div>
                            </form>
                            @include('widgets.history_edit_slider', ['history' => $history])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
