@extends('layouts.admin.base')

@section('title', 'Smoking')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="box-header col-md-10 row mx-0 justify-content-md-between">
                        <div>
                            <h4 class="box-title "><strong>Smoking Attitude</strong></h4>
                        </div>
                        <div>
                            <a href="{{route('admin.smoking.create')}}" class="btn btn-primary pull-right">
                                <i class="fa fa-plus-square-o"></i> Создать ключ
                            </a>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Ключ</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        @forelse ($attitudes as $keyItem)
                            <tr>
                                <td><span>{{ $keyItem->id }}</span></td>
                                <td><span>{{ $keyItem->title }}</span></td>
                                <td><span>{{ $keyItem->key_translation }}</span></td>
                                <td><span>
                                    <div class="btn-group" role="group">
                                        <a href="{{route('admin.smoking.edit', $keyItem->id )}}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'admin.smoking.destroy', $keyItem->id) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">
                                    <h2>Данные отсутствуют</h2>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
