<section class="py-5 pb-lg-0 pt-lx-4" style="background-image: url({{ asset('/img/fon.jpg') }}); background-position-x: 80%; background-size: cover;">
    <div class="container">
        <div class="row-cols-1 fon">
            <div class="col-md-auto pt-5 text-light text-center">
                <h2 class="about-title text-light">{{ __('main.layouts.main.faq') }}</h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 col-md-10 offset-md-1 offset-lg-0 pb-5">

                <div class="accordion md-accordion" id="faqAccordion" role="tablist" aria-multiselectable="true">

                    @include('widgets.faq_loop')

                </div>

                <div class="col-auto px-0 col-sm-auto pt-3 pt-sm-4 px-lg-0 d-flex justify-content-center justify-content-lg-end pt-lg-4 pt-xl-5">
                    <form id="faqAjaxLoad" action="{{ route('ajax.widget.faq') }}" method="POST">
                        @csrf
                        <input type="hidden" name="offset" value="{{ $offset }}">
                        <button type="submit" class="btn btn-outline-light btn-block show-more py-1 mx-0 px-4 px-sm-4 d-flex align-items-center">
                             <object class="img-show-more img-black mr-1" type="image/svg+xml" data="/img/icon/refresh(about).svg" ></object>
                            {{ __('main.layouts.main.more') }}
                        </button>
                    </form>

                </div>
            </div>
            <div class="col-6 mt-5 d-none d-xl-block">
                <img src="{{ asset('/img/foto-fon-faq.png') }}">
            </div>
        </div>
    </div>
</section>




