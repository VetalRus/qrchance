@foreach($faqs as $index => $item)
	@php $faq_lang = $item->translate(); @endphp
	@if($faq_lang)
		<div class="card rounded mb-3">
			<div class="card-header border-0 bg-white" role="tab" id="faqHeading_{{ $item->id }}">
				<a class="collapsed text-dark text-decoration-none" data-toggle="collapse" data-parent="#faqAccordion" href="#faqCollapse_{{ $item->id }}" aria-expanded="false" aria-controls="faqCollapse_{{ $item->id }}">
					<h5 class="mb-0">
						{{ $faq_lang->title }}

						<i class="fa  pull-right text-color__orange @if( isset($is_show) && $is_show && $index == 0 ) fa-minus @else fa-plus @endif" aria-hidden="true"></i>
					</h5>
				</a>
			</div>

			<div id="faqCollapse_{{ $item->id }}" class="collapse @if( isset($is_show) && $is_show && $index == 0 ) show @endif" role="tabpanel" aria-labelledby="faqHeading_{{ $item->id }}" data-parent="#faqAccordion">
				<div class="card-body">
					{{ $faq_lang->content }}
				</div>
			</div>

		</div>
	@endif
@endforeach
