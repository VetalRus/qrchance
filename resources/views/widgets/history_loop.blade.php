@foreach($histories as $item)
    @php $user = $item->user; @endphp
    <div class="col-lg-{{$col_number}} col-sm-6 mb-4 w-100" style=" overflow-y: hidden; max-height: 430px ">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!-- !! Нужно прописать путь к папке с аватаром пользователя -->
                    <div class=" card-img-history text-center rounded-0  col-2 px-0 ml-3" >
                        <img class="card-img-top img-fluid h-100" src="{{ asset($user->photo) }} " >
                        
                    </div>
                    
                    <div class="col-9 pr-0" style="height: 3rem">
                        <div class="col-12 d-flex px-0">
                             <div class="col-7 px-0 m-0 d-flex flex-wrap " style=" word-wrap: break-word;">
                                <p class="card-text m-0">
                                    <small class="text-muted font-weight-bold" style="word-wrap: break-word;">
                                        @if($user->city)
                                        {{ $user->city->translate()->title }}, {{ $user->country->translate()->title }}&ensp;  
                                        @endif
                                    </small>
                                </p>
                             <!--    <p class="card-title m-0 font-weight-bold" style="word-wrap: break-word;">{{ $user->name }}</p> -->
                            </div>
                            @php
                            $days = date_diff(new DateTime(), $item->created_at)->days;
                            @endphp
                            <p class="card-text col-5 pl-0 d-flex justify-content-end flex-wrap">
                                <small class="text-muted" style="word-wrap: break-word;">
                                    <!-- @todo  ДОБАВИТЬ ПЕРЕВОДЫ ДЛЯ "СЕЙЧАС" И "ДНЕЙ"-->
                                    @if($days == 0)
                                    Сегодня
                                    @else

                                    {{ $days }} Дней
                                    @endif
                                </small>
                            </p>
                        </div>
                         <div class="col-12 px-0 m-0 d-flex flex-wrap ">
                           
                            <p class="card-title m-0 font-weight-bold" style="word-wrap: break-word;">{{ $user->name }}</p>
                        </div>
                    </div>
                </div>
                <p class="card-text mt-4 mb-3" style="height: 3rem; overflow: hidden;">{{ $item->content }}</p>
                <a href="{{ route('main.history.show', $item->id) }}" class="btn btn-sm float-right col-lg-5 bg-orange">{{ __('main.main.index.details') }}</a>
            </div>
            @php
            $image = json_decode($item->images, true)['0'];
            @endphp
            <div class="col-12 px-0">
			     <img class="card-img-bottom w-100 h-100"  src="{{ asset($image) }}">
            </div>
        </div>
    </div>
    @endforeach
