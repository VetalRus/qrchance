@php $qr_code = $user->qrCode() @endphp
@if($qr_code)
<h5 class="card-title pt-4 pb-sm-2 font-weight-bold"><b>{{ __('main.account.qr.title_qrcode') }}</b></h5>
<div class="row justify-content-md-between">

    <div class="col-12 col-md-6 mx-0 mb-4 mb-md-0 mx-auto justify-content-center">
        <div class="d-flex  align-items-center justify-content-center py-auto " style="background: #f5f5f5; height: 10em">
            <div class=" d-flex align-items-center bg-white m-auto " style="height: 8em; width: 12em">
                <div class="col-sm-8 bg-white m-auto p-auto">
                    <div class=" m-auto p-auto pl-3 p-sm-0">
                        <img src="{{ asset($qr_code->img)}}" class="img-fluid rounded m-auto" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6  mx-0 mt-4 mt-md-0 ">
        <div class="font-weight-normal">
            <h5><b>{{ __('main.account.qr.using') }}</b></h5>
            <div class="pr-0">
                <div>{{ __('main.account.qr.using_1') }}</div>
                <div>{{ __('main.account.qr.using_2') }}</div>
                <div>{{ __('main.account.qr.using_3') }}</div>
                <div>{{ __('main.account.qr.using_4') }}</div>
            </div>
        </div>

    </div>
    <div class="col-12 col-md-6 py-4 px-0 px-md-3 mx-auto" style="max-width: 359px;">
        <div class="card pr-0 pb-0">
            <div class="d-flex py-3 px-3">
                <div class="col-4 m-0 p-0">
                    <div class="col-12 px-0">
                        <div class="d-flex align-items-center justify-content-center">
                            <img src="{{ asset($qr_code->img)}}" class="rounded w-100 h-100" alt="">
                        </div>
                    </div>
                </div>

                <div class="col-8 m-0 p-0">
                    <div class="col-12 px-0">
                        <p class="col-12 bcard-side2-title1 px-0 pt-1 pt-sm-3 mb-1"> {{ __('main.account.qr.slogan5') }}&ensp;<span class="text-color__orange">QR</span>&ensp;{{ __('main.account.qr.slogan6') }}</p>
                        <div class="col-12 d-flex bcard-side2 px-0 py-1 ">
                            <div class="col-2 bcard-side2-img d-none d-sm-block px-2 py-2 mr-1 h-100" style="min-width: 32.55px">
                                <img class="" src="{{ asset('/img/bcard/email.png') }}" alt="emil">
                            </div>
                            <div class="col-12 col-sm-10 bcard-side2-text px-0 align-items-center h-100 py-1">
                                <p class="ml-2">{{ __('main.account.qr.slogan7') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 bg-orange text-center rounded-0 d-flex align-items-center justify-content-center py-2 py-xl-2  mb-0">
                <h5 class="bcard-side2-title "><span style="background: url({{ asset('/img/QRcode.png') }}); background-size: cover" class="py-1 px-1">QR</span><span class="text-white">Сhance.com</span></h5>
            </div>
        </div>
    </div>

    <div class=" col-12 col-md-6 py-4 px-0 px-md-3 mx-auto w-75 h-auto" style="max-width: 359px;">
        <div class="card pr-0 pb-0">
            <div class="d-flex justify-content-md-center align-items-center   text-center p-3">
                <div class="col-12  mx-auto px-0 pt-3 pb-2">
                    <h5 class="bcard-side1-title">{{ __('main.account.qr.slogan2') }}</br> {{ __('main.account.qr.slogan3') }}&ensp;<span style="background: url({{ asset('/img/QRcode.png') }}); background-size: cover" class=" py-1 px-1">QR</span><span class="text-color__orange">Сhance.com</span></h5>
                    <div class=" bcard-side1__sep my-1 mx-5"></div>
                </div>

            </div>
            <div class=" bg-orange text-center rounded-0 d-flex align-items-center py-2 pt-xl-2 pb-3">
                <p class="mb-0 text-center mx-auto bcard-text-footer"><span class="text-white">{{ __('main.account.qr.slogan4') }}</span>&ensp;{{ __('main.account.qr.company') }}</p>
            </div>

        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12 pb-4 pt-4 pt-md-0">
        <form action="{{ route('pdf.business-card', ['download'=>'pdf']) }}" method="post">
            @csrf
            <button type="submit" class="btn btn-lg bg-orange btn-block text-white mb-1">{{ __('main.account.qr.btn_download') }}
                <object class="ml-0  ml-md-2" type="image/svg+xml" data="../img/icon/download(card).svg" style="width: 1.2rem; height: 1.2rem"></object>
            </button>
        </form>
    </div>
</div>

<h5 class="col-12 text-center">{{ __('main.account.qr.info') }}:</h5>
<div class="dropdown-divider my-4"></div>
<h5 class="mb-4"><b>{{ __('main.account.qr.print_info') }}</b></h5>
<ul>
    @foreach($printoutPlaces as $item)
    <li>
        {{ $item->city->translate()->title }}, {{ $item->address }}, {{ $item->phone }}, {{ $item->email }}
    </li>
    @endforeach
</ul>
<div class="dropdown-divider my-4"></div>
<h5><b>{{ __('main.account.qr.title_info1') }}</b></h5>
<h5> <b>{{ __('main.account.qr.title_info2') }}</b></h5>
<form id="formAddPrintoutPlace" method="post" action="{{ route('main.account.add-printout-place') }}">
    @csrf
    <div class="row justify-content-md-center py-3 pt-md-5">
        <div class="col-md-10">
            <div class="form-group row form-group-text">
                <label for="name" class="col-sm-4 col-form-label">{{ __('main.account.qr.town') }}</label>
                <div class="col-sm-8  form_gray_s">
                    <div class="gray-select">
                        <i class="fa fa-angle-right circle_btn " aria-hidden="true"></i>
                        <input class="form-control dropdown-toggle " type="button" id="selectCity" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <input class="input_id" name="city_id" type="hidden" value="{{ old('city_id') ?? $user->city_id }}">
                        <ul class="dropdown-menu col-12 text-center border-0 mt-0" aria-labelledby="selectCity">
                            @foreach($cities as $item)
                            <li class="list-country ml-2 py-2">
                                <input type="hidden" value="{{ $item->id }}">
                                <span>{{ $item->translate()->title }}</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="alert alert-danger d-none" role="alert">
                        {{ __('main.account.qr.error.city') }}
                    </div>
                </div>
            </div>

            <div class="form-group row form-group-text">
                <label for="email" class="col-sm-4 col-form-label">{{ __('main.account.qr.street/town') }}</label>

                <div class="col-sm-8 form_gray">
                    <object class="inp_avatar" type="image/svg+xml" data="/img/icon/placeholder(registr).svg"></object>
                    <input type="text" class="form-control" name="address" value="{{ old('email') }}" placeholder="{{ __('main.account.qr.address.placeholder') }}">

                </div>
                <div class="alert alert-danger d-none" role="alert">
                    {{ __('main.account.qr.error.address') }}
                </div>
            </div>

            <div class="form-group row form-group-text">
                <label for="password" class="col-sm-4 col-form-label">{{ __('main.account.qr.email') }}</label>

                <div class="col-sm-8  form_gray">
                    <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                    <input type="email" class="form-control" name="email" placeholder="{{ __('main.account.qr.email_placeholder') }}">
                </div>
                <div class="alert alert-danger d-none" role="alert">
                    {{ __('main.account.qr.error.email') }}
                </div>
            </div>
            <div class="form-group row  form-group-text">
                <label for="password" class="col-sm-4 col-form-label">{{ __('main.account.qr.phone') }}</label>

                <div class="col-sm-8 form_gray">

                    <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                    @widget('InputPhone', ['phone' => old('phone') ])
                </div>
                <div class="alert alert-danger d-none" role="alert">
                    {{ __('main.account.printout.error_phone') }}
                </div>

            </div>
            <div class="form-group row justify-content-md-center">
                <div class="col-md-6">
                    <div>
                        <button class="btn btn-lg bg-orange btn-block ml-md-5 mt-2">
                            {{ __('main.account.qr.save') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endif
