<div class="modal fade" id="modalWarningChanceStatus" tabindex="-1" role="dialog" aria-labelledby="Warning" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class='modal-header'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                   <!--  <i class="fa fa-exclamation-triangle text-color__orange " aria-hidden="true"></i> -->
                     <object class="icon_triangle mt-3" type="image/svg+xml" data="/img/icon/warning(orange).svg"></object>
                </h3>
            </div>
            <div class="modal-body pb-5">
                <h1 class="text-center d-flex justify-content-center align-items-center flex-column personal-title">
                    {{ __('main.error.title') }}
                </h1>

                <h5 class="my-4 text-center" style="line-height: 1.8em">
                    {{ __('main.account.profile.warning_change_status')  }}
                    <span class="text-color__orange">
                        {{ __('main.account.profile.warning_change_status1')  }}
                    </span>
                     {{ __('main.account.profile.warning_change_status2')  }}
                </h5>

                <form id="formChangeUserStatus" method="POST" action="{{ route('main.account.update.status') }}">
                    @csrf

                    <input type="hidden" name="status_id">
                    <div class="form-group row justify-content-md-center">
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-lg bg-orange btn-block">
                                {{ __('main.account.profile.btn_change') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
