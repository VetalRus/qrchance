<div class="modal fade {{ session('success_modal') ? 'show' : '' }}" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="Error" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class='modal-header'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
					<i class="fa fa-check-circle text-color__orange"></i>

                </h3>
            </div>
            <div class="modal-body">
                <h1 class="text-center d-flex align-items-center flex-column">
                    {{ __('main.success.title') }}
                </h1>

				<h5 class="my-4 text-center modal-message">
					{{ session('success_modal') ? __(session('success_modal')) : ''   }}
				</h5>

                <div class="form-group row justify-content-md-center">
                    <div class="col-md-8">
                        <button data-toggle="modal" data-dismiss='modal' aria-label='Close' class="btn btn-lg bg-orange btn-block">
                            {{ __('main.success.btn_go') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
