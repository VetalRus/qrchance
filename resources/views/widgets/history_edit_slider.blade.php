@if($history->images != null)
<div id="historyEditSlider">
    <div class="row justify-content-md-center mb-3">
        <h4 class="mt-5">{{ __('main.stories.your_photos') }}</h4>
    </div>
    @php
    $images = json_decode($history->images, true);
    $imagesCount = count($images);
    @endphp
    @if($imagesCount>3)
    <div style="position: relative">
        <div class="swiper-container mt-4">
            <div class="swiper-wrapper">
                @foreach($images as $key => $item)
                <div class="swiper-slide h-100%">
                    <div class="img-item">
                        <form class="deletePhotoHistoryAjax" action="{{ route('ajax.main.history.deletePhotoHistory', $history->id ) }}" method="POST">
                            @csrf
                            <input type="hidden" name="delete_image_id" value="{{ $key }}">
                            <button type='submit' class='close mr-1'>
                                <i class="fa fa-times " aria-hidden="true"></i>
                            </button>
                        </form>

                        <img class="d-block  img-fluid" src="{{ asset($item) }}" alt="{{ $history->title }}">
                    </div>
                </div>
                @endforeach

            </div>

            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>

    @else
    <div class="row d-flex mt-3">
        @foreach($images as $key => $item)
        <div class="col photoHistory-{{ $key }}">
            <div class="img-item">

                <form @if($imagesCount> 1) class="deletePhotoHistoryAjax" action="{{ route('ajax.main.history.deletePhotoHistory', $history->id ) }}"@else onsubmit="return false" @endif method="POST">
                    @csrf
                    <input type="hidden" name="delete_image_id" value="{{ $key }}">
                    <button class='btn close' @if($imagesCount < 2) id="historyCloseButton" @else type='submit' @endif>
                        <i class="fa fa-times @if($imagesCount < 3) display-4 @endif" aria-hidden="true"></i>
                    </button>
                </form>

                <img class="w-100 mx-2" src="{{ asset($item) }}" alt="{{ $history->title }}">
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>
@endif
