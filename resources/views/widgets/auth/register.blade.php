<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="Register" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <form method="POST" action="{{ route('register') }}" id="formRegister">
            @csrf

			<input type="hidden" name="referral_token" value="{{ auth()->guest() ? request()->get('referral') : "" }}">

            <div class="modal-content">
                <div class='modal-header'>
                    <h3 class='col-12 modal-title text-center'>
                        <button type='button' class='close pl-0' data-dismiss='modal' aria-label='Close'>
                            <h1><span aria-hidden='true'>&times</span></h1>
                        </button>
                        <div class="pt-4">
                        <b>{{ __('main.auth.register.welcome') }}</b><br/>
                        <span class="text-color__orange">QR</span>Сhance.com
                        </div>
                    </h3>
                </div>
                @include('widgets.auth.register.first-step')

                @include('widgets.auth.register.second-step')

                @include('widgets.auth.register.third-step')
            </div>
        </form>
    </div>
</div>
