<div class="modal-body d-none" id="modalRegisterThirdStep">
    <div class="container">
        <h4 class="text-center">
            {{ __('main.auth.register.verify_text') }}
        </h4>
    </div>
    <div class="form-group row justify-content-md-center">
        <div class="col-md-6">
            <div class="mt-5">
                <a href="{{route('main.home.index')}}" class="btn btn-lg bg-orange btn-block">
                    {{ __('main.auth.register.btn_next') }}
                </a>
            </div>
        </div>
    </div>
</div>
