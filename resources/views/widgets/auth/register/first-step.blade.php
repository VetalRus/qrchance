<div class="modal-body" id="modalRegisterFirstStep">
    <h2 class="text-center text-color__orange d-flex align-items-center flex-column">
        <object class="avatar_reg mb-2" type="image/svg+xml" data="../img/icon/user(1).svg" style="width: 1.7rem; height: 1.7rem"></object>
        {{ __('main.auth.register.title') }}
    </h2>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <label for="name" class="col-form-label text-md-right">{{ __('main.auth.register.name') }}</label>

            <div class="input-group mb-2 form_gray">
                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/user (registr).svg"></object>
                <input type="text" class="form-control" required name="name" value="{{ old('name') }}" autocomplete="name" autofocus placeholder="{{ __('main.auth.register.name_placeholder') }}">
            </div>
            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_name') }}
            </div>
        </div>
    </div>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <label for="email" class="col-form-label text-md-right">{{ __('main.auth.register.email') }}</label>

            <div class="input-group mb-2 form_gray">
                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/email (registr).svg"></object>
                <input type="email" class="form-control" required name="email" value="{{ old('email') }}" autocomplete="email" placeholder="{{ __('main.auth.register.email_placeholder') }}">

            </div>
            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_email') }}
            </div>
            <div class="alert alert-danger d-none duplicate-email" role="alert">
                {{ __('main.auth.register.error_duplicate-email') }}
            </div>

        </div>
    </div>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <label for="password" class="col-form-label text-md-right">{{ __('main.auth.register.password') }}</label>

            <div class="input-group mb-2 form_gray">
                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/unlock (registr).svg"></object>
                <input type="password" class="form-control" name="password" required autocomplete="new-password" placeholder="{{ __('main.auth.register.password_placeholder') }}">
                <i class="fa fa-eye-slash changeTypeInput" aria-hidden="true"></i>
            </div>
            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_password') }}
            </div>
        </div>
    </div>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <label for="password-confirm" class="col-form-label text-md-right">{{ __('main.auth.register.password_confirmation') }}</label>


            <div class="input-group mb-2 form_gray">
                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/unlock (registr).svg"></object>
                <input type="password" class="form-control" required name="password_confirmation" autocomplete="new-password" placeholder="{{ __('main.auth.register.password_confirmation_placeholder') }}">
                <i class="fa fa-eye-slash changeTypeInput" aria-hidden="true"></i>
            </div>
            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_password_confirmation') }}
            </div>
        </div>
    </div>


    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <div class="input-group mb-2">
                {!! NoCaptcha::display() !!}
            </div>

            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_recaptcha_response') }}
            </div>
        </div>
    </div>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-10">
            <div class="input-group form-check">
                <input class="form-check-input" type="checkbox" name="18_years">

                <label class="form-check-label" for="18_years">
                    {{ __('main.auth.register.18_years') }}
                </label>
            </div>
            <div class="alert alert-danger d-none" role="alert">
                {{ __('main.auth.register.error_18_years') }}
            </div>
        </div>
    </div>

    <div class="form-group row justify-content-md-center">
        <div class="col-md-6">
            <div>
                <button type="button" class="btn btn-lg bg-orange btn-block" id="registerNextStep2">
                    {{ __('main.auth.register.btn_next') }}
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>

</div>
