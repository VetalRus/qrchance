<div class="modal-body d-none" id="modalRegisterSecondStep">
    <div class="container">
    <h4 class="text-center">
        {{ __('main.auth.register.goals') }}
    </h4>


    <div class="form-group row justify-content-md-center my-5">
        <div class="col-md-8">
            <div class="form-check form-check-inline mr-5">
                <input class="form-check-input" type="radio" name="sex" id="sexMan" value="1" checked >
                <label class="form-check-label" for="sexMan">{{ __('main.auth.register.sex_man') }}</label>
            </div>
            <div class="form-check form-check-inline ml-2">
                <input class="form-check-input" type="radio" name="sex" id="sexGirl" value="2" >
                <label class="form-check-label" for="sexGirl">{{ __('main.auth.register.sex_woman') }}</label>
            </div>
        </div>
    </div>
    </div>
    <div class="form-group row justify-content-md-center">
        <div class="col-md-6">
            <div class="mb-5">
                <button type="submit" class="btn btn-lg bg-orange btn-block">
                    {{ __('main.auth.register.btn_submit') }}
                </button>
            </div>
        </div>
    </div>
</div>

