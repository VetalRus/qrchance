<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class='modal-header'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    {{ __('main.auth.login.welcome') }}<br />
                    <span class="text-color__orange">QR</span>Сhance.com
                </h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center text-color__orange d-flex align-items-center flex-column">
                    <object class="avatar_reg mb-2" type="image/svg+xml" data="{{ asset("/img/icon/login(1).svg") }}" style="width: 1.7rem; height: 1.7rem"></object>
                    {{ __('main.auth.login.title') }}
                </h2>

                <form method="POST" action="{{ route('login') }}" id="formLogin">
                    @csrf

                    <div class="alert alert-danger d-none" role="alert">
                        {{ __('main.auth.login.error_login_or_password') }}
                    </div>
                    <div class="alert alert-danger d-none error-validate" role="alert">
                        {{ __('main.auth.login.error_validate') }}
                    </div>
                    <div class="form-group row justify-content-md-center">
                        <div class="col-md-10">
                            <label for="name" class="col-form-label text-md-right">{{ __('main.auth.login.email') }}</label>

                            <div class="input-group mb-2 form_gray">

                                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/email (registr).svg"></object>

                                <input type="text" class="form-control" required name="email" autocomplete="email" autofocus placeholder="{{ __('main.auth.login.email_placeholder') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row justify-content-md-center">
                        <div class="col-md-10">
                            <label for="name" class="col-form-label text-md-right">{{ __('main.auth.login.password') }}</label>

                            <div class="input-group mb-2 form_gray">

                                <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/unlock (registr).svg"></object>
                                <input type="password" class="form-control" required name="password" placeholder="{{ __('main.auth.login.password_placeholder') }}">
                           		<i class="fa fa-eye-slash changeTypeInput" aria-hidden="true"></i>
						    </div>
                        </div>
                    </div>

                    <div class="form-group row justify-content-md-center">
                        <div class="col-md-10">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember">

                                <label class="form-check-label" for="remember">
                                    {{ __('main.auth.login.remember') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row justify-content-md-center">
                        <div class="col-md-8">
                            <button type="submit" class="btn btn-lg bg-orange btn-block">
                                {{ __('main.auth.login.btn_submit') }}
                            </button>
                        </div>
                    </div>
                    @if (Route::has('register'))
                    <div class="form-group row">
                        <div class="col-md-10 offset-1">
                            {{ __('main.auth.login.account') }}
                            <a data-toggle="modal" data-target="#modalRegister" data-dismiss='modal' aria-label='Close' href="#"><b>{{ __('main.auth.login.register') }}</b></a>
                        </div>
                    </div>
                    @endif

                    @if (Route::has('password.request'))
                    <div class="form-group row">
                        <div class="col-md-10 offset-1">
                            <a data-toggle="modal" class="text-dark" data-target="#modalPasswordRecovery" data-dismiss='modal' aria-label='Close' href="#">
                                {{ __('main.auth.login.forgot_password') }}
                            </a>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
