<div class="modal-body pt-0 d-none" id="modalPasswordRecoverySecondStep">
    <h2 class="text-center text-color__orange">
        <i class="fa fa-unlock-alt" aria-hidden="true"></i><br>
    </h2>
    <h4 class="text-center">
        <b>{{ __('main.auth.forgot.recovery') }}</b><br>
    </h4>
    <h5 class="my-4 text-center">
        {{ __('main.auth.forgot.send') }}
    </h5>
    <div class="row justify-content-md-center">
        <div class="col-md-9">
            <div>
                <button type="button" class="btn btn-lg bg-orange btn-block" id="loginNextStep2" data-dismiss="modal">
                    {{ __('main.auth.forgot.go') }}
                </button>
            </div>
        </div>
    </div>
</div>

