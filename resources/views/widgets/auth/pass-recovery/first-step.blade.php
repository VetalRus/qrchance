<div class="modal-body pt-0" id="modalPasswordRecoveryFirstStep">
    <h2 class="text-center text-color__orange mb-0">
        <i class="fa fa-unlock-alt" aria-hidden="true"></i><br>
    </h2>
    <h4 class="text-center mb-2">
        <b>{{ __('main.auth.forgot.password') }}</b><br>
    </h4>
    <form method="POST" action="{{ route('password.email') }}" id="formPasswordRecovery">
        @csrf
        <div class="form-group row justify-content-md-center">
            <div class="col-md-10">
                <label for="email" class="col-form-label text-md-right">{{ __('main.auth.forgot.email') }}</label>

                <div class="input-group mb-2 form_gray">
                    <object class="inp_avatar_reg" type="image/svg+xml" data="../img/icon/email (registr).svg"></object>
                    <input type="email" class="form-control" required name="email" value="{{ old('email') }}" autocomplete="email" placeholder="{{ __('main.auth.register.email_placeholder') }}">

                </div>
                <div class="alert alert-danger d-none" role="alert">
                    {{ __('main.auth.forgot.error_email') }}
                </div>

            </div>
        </div>
        <div class="form-group row justify-content-md-center">
            <div class="col-md-10">
                <div>
                    <button type="submit" class="btn btn-lg bg-orange btn-block " id="loginNextStp2" data-toggle="collapse" data-target=".second">
                        {{ __('main.auth.forgot.send_btn') }}
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
