<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="PasswordRecovery" aria-hidden="true" id="modalPasswordRecovery">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class='modal-header pb-0'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    {{ __('main.auth.login.welcome') }}<br />
                    <span class="text-color__orange">QR</span>Сhance.com
                </h3>
            </div>
            @include('widgets.auth.pass-recovery.first-step')

            @include('widgets.auth.pass-recovery.second-step')
        </div>
    </div>
</div>
