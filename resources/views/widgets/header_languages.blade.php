<li class="nav-item dropdown position-relative">
    <a class="nav-link nav dropdown-toggle d-flex align-items-center" data-toggle="dropdown" id="dropdownLanguage" aria-haspopup="true" aria-expanded="false">
        <span class="flag-icon mb-1 flag-icon-{{ App\Models\Language::flag() }}"> </span> 
        <span class="form-group-text mx-md-3  d-none d-md-block ">{{ __('main.language') }}</span>

        <span class="form-group-text ml-1 initialism d-block d-md-none ">{{ App::getLocale() }}</span>
    </a>
    <div class="dropdown-menu border-top-0 position-absolute pr-1" aria-labelledby="dropdownLanguage">
        @php
            $url = explode("/", Request::path(), 2);
            if( App::getLocale() == $url['0'] && isset($url['1']) ) {
                $path = $url['1'] ;
            } elseif( App::getLocale() != Request::path() ) {
                $path = Request::path();
            } else{
                $path = "" ;
            }
        @endphp
        <ul class="col-12 d-flex flex-wrap align-items-start  w-100 h-auto pr-5 pl-0">
            @foreach($languages as $item)
                <li class="col-md-6 list-unstyled py-2 pr-5">
                    <a class="text-dark text-nowrap mx-2 pr-3" href="{{ url( $item->iso . '/' . $path ) }}">
                        <span class="flag-icon ml-2 flag-icon-{{ $item->flag }}"> </span> {{ $item->title }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</li>