<div class="modal fade {{ $errors->has('error_modal') ? 'show' : '' }}" id="modalError" tabindex="-1" role="dialog" aria-labelledby="Error" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class='modal-header pb-0'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                   <object class="icon_triangle mt-3" type="image/svg+xml" data="/img/icon/warning(orange).svg"></object>

                </h3>
            </div>
            <div class="modal-body pb-5">
                <h1 class="text-center d-flex justify-content-center align-items-center personal-title">
                    {{ __('main.error.title') }}
                </h1>

				<h5 class="my-4 text-center" style="line-height: 1.8em">
					{{ $errors->has('error_modal') ? __($errors->first('error_modal')) : ''   }}
				</h5>

                <div class="form-group row justify-content-md-center">
                    <div class="col-md-8">
                        <button data-toggle="modal" data-dismiss='modal' aria-label='Close' class="btn btn-lg bg-orange btn-block">
                            {{ __('main.error.btn_go') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
