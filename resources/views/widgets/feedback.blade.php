<div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="Feedback" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class='modal-header'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                 </h3>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-center mt-0">
                   <object class="about_avatar" type="image/svg+xml" data="/img/icon/support(about).svg"></object>
                </div>
                <div class="card-title text-center mt-2 mb-0 ">
                    <h3 class="font-weight-bold">{{ __('main.feedback.title') }}</h3>
                </div>
                @if(Session::has('message'))
                <p class="text-center color-text font-mb15 m-0 p-0">
                    {{Session::get('message')}}
                </p>
                @endif
                <div class="card-body ">
                    <form method="POST" action="{{ route('mails.feedback') }}">
                        @csrf

                        <div class="form-group row  mb-4">
                            <label for="name" class="col-12  col-form-label ml-1 ml-md-5  font-mr15 py-0">{{ __('main.about_project.name') }}:</label>
                            <div class="input-group col-md-10 m-auto form_gray">

                                 <object class="inp_avatar" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>
                                <input id="name" type="text" class="form-control input-group-text text-left" name="name" required autocomplete="name" autofocus placeholder="{{ __('main.about_project.name_placeholder') }}">

                                @error('name')
                                <span class="invalid-about_project" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="email" class="col-12  col-form-label ml-1 ml-md-5  font-mr15 py-0">{{ __('main.about_project.email') }}:</label>

                            <div class="input-group col-md-10 m-auto form_gray">

                                 <object class="inp_avatar" type="image/svg+xml" data="/img/icon/email (registr).svg"></object>
                                <input id="email" type="email" class="input-group-text text-left form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" placeholder="{{ __('main.about_project.email_placeholder') }}">

                                @error('email')
                                <span class="invalid-about_project" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="tel" class="col-12  col-form-label ml-1 ml-md-5  font-mr15 py-0">{{ __('main.about_project.phone') }}:</label>

                            <div class="input-group col-md-10 m-auto form_gray">

                                <object class="inp_avatar" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>
                                <input id="tel" type="text" class="input-group-text text-left form-control" name="phone" required placeholder="{{ __('main.about_project.phone_placeholder') }}">

                                @error('phone')
                                <span class="invalid-about_project" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="massage" class="col-12  col-form-label ml-1 ml-md-5  font-mr15 py-0">{{ __('main.about_project.write_message') }}:</label>

                            <div class="col-md-10 m-auto form_gray" style="height: 100px">
                                <textarea id="text" type="text" class="input-group-text text-left form-control" name="text" rows="4" style="resize: none; resize: none; height: 100%"> </textarea>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-lg bg-orange btn-block">{{ __('main.about_project.send_message') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
