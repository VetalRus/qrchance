<div id="historyRow" class="row">

    @include('widgets.history_loop')

    @if(Auth::user() && Auth::user()->isRelationship())
    <div class="col-lg-{{$col_number}} col-sm-6 mb-2 w-100" style=" overflow: hidden; max-height: 430px">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="card-img-top bg-orange history-question card-img-history text-center rounded-0  col-2 ml-3">
                        <h2 class="m-0 font-weight-bold">
                            ?
                        </h2>
                    </div>
                    <div class="col-7 m-0 d-flex flex-wrap">
                        <p class="card-title m-0  font-weight-bold " style="height: 3rem; word-wrap: break-word;">{{ __('main.main.index.your_story') }}</p>
                    </div>
                </div>
                <p class="card-text mt-4" style="height: 3rem; overflow: hidden;">{{ __('main.main.index.text_history') }}</p>
                <a href="{{ route('main.history.create') }}" class="btn btn-sm float-right col-lg-7 bg-orange">{{ __('main.main.index.create_history') }}</a>
            </div>
            <div class="col-12 px-0">
                <img class="card-img-bottom h-100  w-100" style="" src="{{ asset('img/account-history.jpg') }}">
            </div>
        </div>
    </div>
    @endif

</div>

<div class="col-md-12 mt-4 pr-md-0 d-flex justify-content-end">
    <form id="historyAjaxLoad" class="mx-auto mx-sm-0" action="{{ route('ajax.widget.history') }}" method="POST">
        @csrf
        <input type="hidden" name="offset" value="{{ $offset }}">
        <input type="hidden" name="take" value="{{ $take }}">
        <input type="hidden" name="col_number" value="{{ $col_number }}">
        <button type="submit" class="btn btn-outline-orange  show-more py-1  px-3 px-sm-4 d-flex align-items-center mt-3 mb-5">
            <object class="img-show-more mr-1" type="image/svg+xml" data="/img/icon/refresh(about)orange.svg"></object>
            {{ __('main.main.index.more') }}
        </button>
    </form>
</div>
