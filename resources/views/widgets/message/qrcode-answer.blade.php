<div class="modal fade" id="modalQRCodeAnswer" tabindex="-1" role="dialog" aria-labelledby="Answer" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class='modal-header pb-0'>
                <div class="col-12 d-flex justify-content-center ">
                    <object class="img-message-orange-max-big mx-auto mt-2" type="image/svg+xml" data="../img/icon/question(orange).svg"></object>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>   
                </div>
            </div>
            <div class="modal-body px-2 px-sm-5 pt-2">
                 <h3 class='col-12 modal-title text-center grcode-answer-title px-1 px-sm-2 pb-4'>
                    
                    {{ __('main.qrcode.modal_title_1') }}&#8194;<span class="grcode-answer-title-big">{{ $qr_code->user->name }}</span> {{ __('main.qrcode.modal_title_2') }}
                </h3>

                <form method="POST" action="{{ route('main.qrcode.send-message', $qr_code->user->id) }}">
                    @csrf

                    <div class="form-group row justify-content-md-center">
                        <div class="col-12 px-4">
                            <label for="name"
                                   class="col-form-label text-md-right form-group-text">{{ __('main.qrcode.send-message.name') }}</label>

                            <div class="input-group mb-2 form_gray">
                                <object class="inp_avatar_reg" type="image/svg+xml" data="/img/icon/user (registr).svg"></object>       

                                <input type="text" class="form-control" required
                                       name="name" autofocus
                                       placeholder="{{ __('main.qrcode.send-message.name_placeholder') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row justify-content-md-center">
                        <div class="col-12 px-4">
                            <label for="name"
                                   class="col-form-label text-md-right form-group-text">{{ __('main.qrcode.send-message.phone') }}</label>

                            <div class="input-group mb-2 form_gray">
                                <object class="inp_avatar_reg" type="image/svg+xml" data="/img/icon/phone-call (registr).svg"></object>       
                                <input type="text" class="form-control" required
                                       name="phone"
                                       placeholder="{{ __('main.qrcode.send-message.phone_placeholder') }}">
                            </div>
                        </div>
                    </div>


                    <div class="form-group row justify-content-md-center">
                        <div class="col-12 px-4 py-4">
                            <button type="submit" class="btn btn-lg bg-orange btn-block">
                                {{ __('main.qrcode.send-message.btn_go') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        
    </div>
</div>
