<div class="modal fade" id="modalQRCodeAnswerThanks" tabindex="-1" role="dialog" aria-labelledby="Answer" aria-hidden="true">
    <div class="modal-dialog" role="document">

       <div class="modal-content pb-3 justify-content-center">
            <div class='modal-header pb-4'>
                <div class="col-12 d-flex justify-content-center ">
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>   
                </div>
            </div>
            <div class="modal-body px-2 px-sm-5 pt-2 text-center">
                <h2 class="col-12  modal-title text-center main-mini-title px-1  pb-2">
                   {{ __('main.qr.thanks.title') }}
                </h2>

                <h3 class='col-12 modal-title text-center grcode-answer-title px-1 px-sm-2 '>
                   {{ __('main.qr.thanks.welcome') }}
                </h3>
                <h3 class="col-12 logo-header mb-0">
                   <span class="text-color__orange">QR</span>Chance.com  
                </h3>
                <div class="col-12 ">
                    <img src="/img/children.png" style="width: 102%">
                </div>
               
            </div>
        </div>

    </div>
</div>
