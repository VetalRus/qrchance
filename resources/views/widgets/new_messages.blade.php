<a class="nav-link message-header-link mr-md-3" href="{{ route('main.account.messages') }}">
	<img class="img-header-bell mt-1 " src="/img/icon/bell(header).svg">
    <span id="count-2" class="badge bg-orange-no-gradient number-messages">{{ $messages }}</span>
</a>
