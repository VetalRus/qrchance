<li class="nav-item dropdown">
    <div class="col-12 d-flex justify-content-between px-0">
        <a id="navbarDropdown" class=" nav-link d-flex w-100 disabled d-md-none" href="#" role="button" 
             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
          v-pre >
            <span class="position-relative avatar" >
                <img src="{{ asset(Auth::user()->photo) }}"  alt="" class="card-img" style="width: 45px; height: 45px">
                <span class="badge bg-orange-no-gradient position-absolute d-none d-md-block " style="top: 30px">
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </span>
            </span>
            <div class="flex-colump d-sm-flex my-auto">
                <span class="badge  text-color__orange text-right d-block d-sm-none">ID {{ Auth::user()->id }}</span>
                <div>
                  {{ Auth::user()->name }} {{ Auth::user()->second_name }}  
                </div>
                <span class="badge btn btn-lg bg-orange-no-gradient text-white d-none d-sm-block px-3 ml-3 pt-2">ID {{ Auth::user()->id }}</span>
            </div>
        </a>

        <a id="navbarDropdown" class="  nav-link d-md-flex w-100 d-none" href="#" role="button" 
             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
          v-pre >
            <span class="position-relative avatar " >
                <img src="{{ asset(Auth::user()->photo) }}"  alt="" class="card-img" style="width: 45px; height: 45px">
                <span class="badge bg-orange-no-gradient position-absolute d-none d-md-block " style="top: 30px">
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </span>
            </span>
            <div class="flex-colump d-sm-flex my-auto">
                <span class="badge  text-color__orange text-right d-block d-sm-none">ID {{ Auth::user()->id }}</span>
                <div>
                  {{ Auth::user()->name }} {{ Auth::user()->second_name }}  
                </div>
                <span class="badge btn btn-lg bg-orange-no-gradient text-white d-none d-sm-block px-3 ml-3 pt-2">ID {{ Auth::user()->id }}</span>
            </div>
        </a>

         <div id="navsMenuBtn" class="navbar-toggler d-block d-md-none btn-circle  bg-white  shadow p-2 my-auto navsMenuBtn">
            <span class="navbar-toggler-icon "></span>
        </div>  
        
     
        <div class="dropdown-menu dropdown-menu-right shadow  p-0  mt-2" aria-labelledby="dropdown09">
            <a class="nav-link bg-orange-nav personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.index') active @endif " href="{{ route('main.account.index') }}"
               aria-selected="false">
                <object class="menu_avatar mx-3 " type="image/svg+xml" data="/img/icon/user (menu).svg"></object> 
                <div class="personal_link_text ml-0">{{ __('main.dropdown_header_user_menu.my_profile') }}</div>   
            </a>
            
            <a class="nav-link personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.business-card') active @endif  " href="{{ route('main.account.business-card') }}"
               aria-selected="false">
                <object class="menu_avatar  mx-3" type="image/svg+xml" data="/img/icon/give-card (menu).svg"></object>
                <div class="personal_link_text ">{{ __('main.dropdown_header_user_menu.creating_visitcard') }}</div>
            </a>
            <a class="nav-link personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.messages') active @endif  " href="{{ route('main.account.messages') }}" aria-selected="false">
                <object class="menu_avatar  mx-3" type="image/svg+xml" data="/img/icon/chat(menu).svg"></object>
                <div class="personal_link_text">{{ __('main.dropdown_header_user_menu.message') }}</div>
            </a>
            <a class="nav-link personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.histories') active @endif  " href="{{ route('main.account.histories') }}" aria-selected="false">
                 <object class="menu_avatar  mx-3" type="image/svg+xml" data="/img/icon/contract(menu).svg"></object> 
                <div class="personal_link_text">{{ __('main.dropdown_header_user_menu.other_stories') }}</div>
            </a>
            <a class="nav-link personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.payment') active @endif  " href="#v-pills-profile" aria-selected="false">
                 <object class="menu_avatar mx-3" type="image/svg+xml" data="/img/icon/credit-cards-payment(menu).svg"></object> 
                <div class="personal_link_text ">{{ __('main.dropdown_header_user_menu.payment') }}</div>
            </a>
            <span class="menu_line ml-3"></span>
            <a class="nav-link personal_link pl-3 pr-5  @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'main.account.security') active @endif " href="{{ route('main.account.security') }}" aria-selected="false">
                 <object class="menu_avatar mx-3" type="image/svg+xml" data="/img/icon/settings(menu).svg"></object> 
                <div class="personal_link_text">{{ __('main.dropdown_header_user_menu.settings') }}</div>
            </a>
            <span class="menu_line ml-3"></span>
             <a class="nav-link personal_link pl-3 pr-5 " href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out mx-3 " ></i> 
                <div class="personal_link_text">{{ __('Выйти') }}</div>
            </a>
            
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</li>
