<div class="modal fade" id="modalVideoPresentation" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class='modal-header'>
                <h3 class='col-12 modal-title text-center'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    {{ __('main.auth.login.welcome') }}<br/>
                    <span class="text-color__orange">QR</span>Сhance.com
                </h3>
            </div>
            <div class="modal-body">
                <iframe class="video" src="https://www.youtube.com/embed/U-g9fwJSA6Y?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
