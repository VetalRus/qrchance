<div class="gray-select">
    <i class="fa fa-angle-right circle_btn" aria-hidden="true"></i>
    <input class="form-control dropdown-toggle " type="button" id="selectCity" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <input class="input_id" name="city_id" type="hidden" value="">
    <ul class="dropdown-menu col-sm-12 text-center border-0 mt-0" aria-labelledby="selectCity">
        @foreach($cities as $item)
        <li class="list-country ml-2 py-2">
            <input type="hidden" value="{{ $item->id }}">
            <span>{{$item->translate()->title}}</span>
        </li>
        @endforeach
    </ul>
</div>
