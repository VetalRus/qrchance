<?php

return  [
	'mode'                 => 'UTF-8',
	'format' 			   => [50, 90],
	'orientation'          => 'L',
	'default_font_size'    => '12',
	'default_font'         => 'gothampro',
	'margin_left'          => 0,
	'margin_right'         => 0,
	'margin_top'           => 0,
	'margin_bottom'        => 0,
	'margin_header'        => 0,
	'margin_footer'        => 0,
	'title'                => 'QRchance',
	'author'               => '',
	'watermark'            => '',
	'show_watermark'       => false,
	'watermark_font'       => 'gothampro',
	'display_mode'         => 'fullpage',
	'watermark_text_alpha' => 0.1,
	// 'custom_font_dir'      => base_path('public\fonts\business-card'),
	// 'custom_font_data' 	   => [
	// 	'gothampro' => [
	// 		'R' => 'GothamPro.ttf',
	// 	],
	// 	'gothampromedium' => [
	// 		'R' => 'GothamPro-Medium.ttf',
	// 	],
		
	// ],
	'auto_language_detection'  => false,
	'temp_dir'               => '',
	'pdfa' 			=> false,
        'pdfaauto' 		=> false,
];