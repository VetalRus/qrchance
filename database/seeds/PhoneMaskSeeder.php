<?php

use Illuminate\Database\Seeder;

class PhoneMaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phone_mask')->insert([
            [
				'country_id' => '1',
				'phone' => '+7(__)___-__-__',
			],
            [
				'country_id' => '2',
				'phone' => '+44(__)___-__-__',
			],
        ]);
    }
}
