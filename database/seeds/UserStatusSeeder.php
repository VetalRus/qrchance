<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_status')->insert([
            [
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);

        DB::table('user_status_languages')->insert([
            [
                'status_id' => User::STATUS_IN_SEATCH_OF,
                'title' => 'В поиске',
                'lang_id' => 5,
            ],
            [
                'status_id' => User::STATUS_IN_RELATIONSHIP,
                'title' => 'В Отношениях',
                'lang_id' => 5,
            ],
            [
                'status_id' => User::STATUS_IN_NOT_ACTIVE,
                'title' => 'Не активен',
                'lang_id' => 5,
            ],
        ]);

        for ($i = 1; $i < 19; $i++) {
            if ($i != 5) {
                DB::table('user_status_languages')->insert([
                    [
						'status_id' => User::STATUS_IN_SEATCH_OF,
                        'lang_id' => $i,
                    ],
                    [
						'status_id' => User::STATUS_IN_RELATIONSHIP,
                        'lang_id' => $i,
                    ],
				    [
						'status_id' => User::STATUS_IN_NOT_ACTIVE,
                        'lang_id' => $i,
                    ],
                ]);
            }

        }
    }
}
