<?php

use Illuminate\Database\Seeder;
use App\Models\History;

class HistoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 6; $i++) {
            History::create([
                'user_id' => 2,
                'content' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                'lang_id' => "5",
                "images" => '["\/img\/photo-history.jpg"]',
            ]);
        }

    }
}
