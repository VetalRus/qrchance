<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
		DB::table('users')->insert([
			'name' => 'Admin',
			'verify_status' => User::STATUS_ACTIVE,
            'isAdmin' => '1',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
		]);

		DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'second_name' => Str::random(5),
            'birthday' => Carbon::make('01.01.1999'),
            'country_id' => '2',
            'city_id' => '2',
            'children' => '2',
            'smoking_id' => '3',
            'alcohol_id' => '2',
            'profession' => 'Manager',
            'phone' => '+44 255 3485',
            'instagram' => 'instagram',
            'vk' => 'vk',
            'facebook'=> 'facebook',
            'twitter' => 'twitter',
            'youtube' => 'youtube',
            'verify_token' => Str::random(30),
            'email_verified_at' => Carbon::now(),
            'sex' => '2',
            'referral_id' => '1',
            'referral_token' => Str::random(30),
            'remember_token' => Str::random(30),
            'created_at' => Carbon::now(),
        ]);
        
    }
}
