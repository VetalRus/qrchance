<?php

use Illuminate\Database\Seeder;
use App\Models\PrintoutPlace;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PrintoutPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('printout_places')->insert([
            'country_id' => '1',
            'city_id' => '1',
            'address' => 'Банкова 1',
            'phone' => '38 098 7985',
            'email' => 'admin@gmail.com',
            'verified' => '1',
        ]);

        DB::table('printout_places')->insert([
            'country_id' => '2',
            'city_id' => '2',
            'address' => '10 Downing Street',
            'phone' => '44 987 9854',
            'email' => 'bjohnson@gmail.com',
            'verified' => '1',
        ]);

    }
}
