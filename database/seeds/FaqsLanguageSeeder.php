<?php

use Illuminate\Database\Seeder;

class FaqsLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 7; $i++) {
            DB::table('faqs')->insert([
                'created_at' => now(),
                'updated_at' => now()
			]);
			
			for ($a = 1; $a < 19; $a++) {
				DB::table('faqs_languages')->insert([
					'faq_id' => $i,
					'title' => "Лучший сайт знакомств Украина",
					'content' => "Что такое любовь? Взаимная, честная, верная… Та невидимая эссенция, парящая в воздухе, то небольшое напряжение между едва знакомыми людьми, то словно магнитное притяжение к человеку – это и есть любовь в её чистом виде? Сложно заводить знакомства, а уж тем более встретить «того самого» человека, и находясь ежедневно среди людей, можно продолжать чувствовать себя одиноким.",
					'lang_id' => $a,
				]);
			}
        }

    }
}
