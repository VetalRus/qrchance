<?php

use Illuminate\Database\Seeder;

class AlcoholSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alcohols')->insert([
            [
                'title' => 'Положительное',
                'key_translation' => 'main.account.personal-info.alcohol_positive',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Нейтральное',
                'key_translation' => 'main.account.personal-info.alcohol_neutral',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Компромиссное',
                'key_translation' => 'main.account.personal-info.alcohol_compromise',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Негативное',
                'key_translation' => 'main.account.personal-info.alcohol_negative',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Резко негативное',
                'key_translation' => 'main.account.personal-info.alcohol_very_negative',
                'created_at' => now(),
                'updated_at' => now()
			],
		]);
    }
}
