<?php

use Illuminate\Database\Seeder;

class SmokeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('smokings')->insert([
            [
                'title' => 'Положительное',
                'key_translation' => 'main.account.personal-info.smoking_positive',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Нейтральное',
                'key_translation' => 'main.account.personal-info.smoking_neutral',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Компромиссное',
                'key_translation' => 'main.account.personal-info.smoking_compromise',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Негативное',
                'key_translation' => 'main.account.personal-info.smoking_negative',
                'created_at' => now(),
                'updated_at' => now()
			],
            [
                'title' => 'Резко негативное',
                'key_translation' => 'main.account.personal-info.smoking_very_negative',
                'created_at' => now(),
                'updated_at' => now()
			],
		]);
    }
}
