<?php

use Illuminate\Database\Seeder;

class SocialNetworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_networks')->insert([
		     [
                'url' => 'https://www.facebook.com',
                'icon' => 'facebook',
                'title' => 'facebook',
                'order' => '1',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'url' => 'https://vk.com',
                'icon' => 'vk',
                'title' => 'vk',
                'order' => '2',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'url' => 'https://twitter.com',
                'icon' => 'twitter',
                'title' => 'Twitter',
                'order' => '3',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'url' => 'https://www.instagram.com',
                'icon' => 'instagram',
                'title' => 'Instagram',
                'order' => '4',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'url' => 'https://www.youtube.com',
                'icon' => 'youtube',
                'title' => 'Youtube',
                'order' => '5',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
