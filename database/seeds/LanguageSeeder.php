<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            [
                'title' => 'English',
                'iso' => 'en',
                'flag' => 'us',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Deutsch',
                'iso' => 'de',
                'flag' => 'de',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Français',
                'iso' => 'fr',
                'flag' => 'fr',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Español',
                'iso' => 'es',
                'flag' => 'es',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Русский',
                'iso' => 'ru',
                'flag' => 'ru',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Italiano',
                'iso' => 'it',
                'flag' => 'it',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Português',
                'iso' => 'pt',
                'flag' => 'pt',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Polski',
                'iso' => 'pl',
                'flag' => 'pl',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Nederlands',
                'iso' => 'nl',
                'flag' => 'nl',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => '日本人',
                'iso' => 'ja',
                'flag' => 'jp',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Česky',
                'iso' => 'cz',
                'flag' => 'cz',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Svenska',
                'iso' => 'sv',
                'flag' => 'sv',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => '中文',
                'iso' => 'zh',
                'flag' => 'cn',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Türkçe',
                'iso' => 'tr',
                'flag' => 'tr',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Español (Mexico)',
                'iso' => 'mx',
                'flag' => 'mx',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Ελληνικά',
                'iso' => 'gr',
                'flag' => 'gr',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Português (Brasil)',
                'iso' => 'br',
                'flag' => 'br',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Magyar',
                'iso' => 'hu',
                'flag' => 'hu',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Українська',
                'iso' => 'ua',
                'flag' => 'ua',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'title' => 'Беларускі',
                'iso' => 'by',
                'flag' => 'by',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'हिन्दी',
                'iso' => 'hin',
                'flag' => 'in',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'বাংলা',
                'iso' => 'ben',
                'flag' => 'in',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'Indonesia',
                'iso' => 'ind',
                'flag' => 'id',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'ਪੰਜਾਬੀ',
                'iso' => 'pan',
                'flag' => 'pk',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'Pilipinas',
                'iso' => 'tgl',
                'flag' => 'ph',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'Tiếng Việt',
                'iso' => 'vie',
                'flag' => 'vn',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'ไทย',
                'iso' => 'tha',
                'flag' => 'th',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'ဗမာ',
                'iso' => 'bur',
                'flag' => 'mm',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => '한국어',
                'iso' => 'kor',
                'flag' => 'kr',
                'created_at' => now(),
                'updated_at' => now()
			],

            [
                'title' => 'Bahasa Melayu',
                'iso' => 'msa',
                'flag' => 'my',
                'created_at' => now(),
                'updated_at' => now()
			],
        ]);
    }
}