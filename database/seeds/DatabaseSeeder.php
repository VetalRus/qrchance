<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountrySeeder::class,
            UserSeeder::class,
            LanguageSeeder::class,
            SocialNetworkSeeder::class,
            HistoriesSeeder::class,
            FaqsLanguageSeeder::class,
            AlcoholSeeder::class,
			SmokeSeeder::class,
			UserStatusSeeder::class,
			PrintoutPlaceSeeder::class,
			FreePeriodSeeder::class,
			PhoneMaskSeeder::class,
        ]);
    }
}
