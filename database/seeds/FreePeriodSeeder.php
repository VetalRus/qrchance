<?php

use Illuminate\Database\Seeder;

class FreePeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('free_period')->insert([
            [
                'title' => 'Бесплатный период',
                'end_date' => now()->modify('+10 day'), 
			],
		]);
    }
}
