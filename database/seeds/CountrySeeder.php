<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'iso' => 'ua',
                'flag' => 'ua',
            ], [
                'iso' => 'gb',
                'flag' => 'gb',
            ]
        ]);

        for ($i = 1; $i < 19; $i++) {
            DB::table('countries_languages')->insert([
                [
                    'country_id' => 1,
                    'title' => 'Украина',
                    'lang_id' => $i,
                ],
            ]);
        }

        for ($i = 1; $i < 19; $i++) {
            DB::table('countries_languages')->insert([
                [
                    'country_id' => 2,
                    'title' => 'Великобритания',
                    'lang_id' => $i,
                ]
            ]);
        }


        DB::table('cities')->insert([
            [
                'country_id' => 1
            ], [
                'country_id' => 2
            ]
        ]);
        for ($i = 1; $i < 19; $i++) {
            DB::table('cities_languages')->insert([
                [
                    'city_id' => 1,
                    'title' => 'Киев',
                    'lang_id' => $i,
                ],
            ]);
        }

         for ($i = 1; $i < 19; $i++) {
             DB::table('cities_languages')->insert([
                [
                    'city_id' => 2,
                    'title' => 'Лондон',
                    'lang_id' => $i,
                ]
            ]);
        }
    }
}
