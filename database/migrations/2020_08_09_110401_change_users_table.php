<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class ChangeUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->smallInteger('verify_status')->default(User::STATUS_INACTIVE)->after('remember_token');
            $table->string('verify_token')->after('verify_status')->nullable()->unique();
        });

        DB::table('users')->update([
            'verify_status' => User::STATUS_ACTIVE,
        ]);
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verify_status');
            $table->dropColumn('verify_token');
        });
    }
}
