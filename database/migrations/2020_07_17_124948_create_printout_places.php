<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintoutPlaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printout_places', function (Blueprint $table) {
            $table->id();
            $table->integer('country_id');
            $table->integer('city_id');
            $table->string('address');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->boolean('verified')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printout_places');
    }
}
