<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
			$table->boolean('isAdmin')->default(0);
			$table->boolean('isReadyGame')->default(0);
            $table->string('email')->unique();
            $table->string('photo')->default("/img/avatars/user.svg");
            $table->string('second_name')->nullable();
            $table->date('birthday')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('children')->nullable();
            $table->integer('smoking_id')->nullable();
            $table->integer('alcohol_id')->nullable();
            $table->string('profession')->nullable();
            $table->string('phone')->nullable();
            $table->string('instagram')->nullable();
            $table->string('vk')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();

            /*
             * statuses:
             * женат
             * не женат
             * свободен
             * в активном поиске
             * */
            $table->integer('status_id')->default(User::STATUS_IN_SEATCH_OF);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			$table->integer('sex')->nullable();
			
			$table->integer('referral_id')->nullable();
            $table->string('referral_token')->nullable()->unique();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
