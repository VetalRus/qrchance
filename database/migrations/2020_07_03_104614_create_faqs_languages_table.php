<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs_languages', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('faq_id')->unsigned();
            $table->foreign('faq_id')->references('id')->on('faqs');

            $table->string('title', 255)->nullable();
            $table->text('content')->nullable();

            $table->bigInteger('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('languages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs_languages');
    }
}
